export const locale = {
  lang: 'en',
  data: {
    'NAV': {
      'APPLICATIONS': 'Applications',
      'MENU': 'Menu',
      'INFO': 'Info',
      'META': 'Meta',
      'GALLERY': 'Gallery',
      'ROOMS_INFO': 'Rooms info',
      'ROOMS_LIST': 'Room List',
      'ROOMS_PARAMETERS': 'Room Parameters',
      'ARTICLES': 'Articles',
      'ROOM_BADGE': 'Room Badges',
      'BOOKING_FEATURES': 'Booking Features',
      'ROOM_TYPES': 'Room Types',
      'WELLNESS': 'Wellness',
      'TEXTS': 'Texts',
      'ARTICLE_CATEGORIES': 'Article categories',
    }
  }
};
