import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'applications',
    title: 'Menu',
    translate: 'NAV.APPLICATIONS',
    type: 'group',
    children: [
      // {
      //     id       : 'sample',
      //     title    : 'Sample',
      //     translate: 'NAV.SAMPLE.TITLE',
      //     type     : 'item',
      //     icon     : 'email',
      //     url      : '/sample',
      //     badge    : {
      //         title    : '25',
      //         translate: 'NAV.SAMPLE.BADGE',
      //         bg       : '#F44336',
      //         fg       : '#FFFFFF'
      //     }
      // }
      {
        id: 'info',
        title: 'Info',
        translate: 'NAV.INFO',
        type: 'item',
        icon: 'info',
        url: '/admin/info',
      },
      {
        id: 'texts',
        title: 'Texts',
        translate: 'NAV.TEXTS',
        type: 'item',
        icon: 'line_style',
        url: '/admin/texts',
      },
      {
        id: 'meta',
        title: 'Meta',
        translate: 'NAV.META',
        type: 'item',
        icon: 'all_out',
        url: '/admin/meta',
      },
      {
        id: 'gallery',
        title: 'Gallery',
        translate: 'NAV.GALLERY',
        type: 'item',
        icon: 'photo_library',
        url: '/admin/gallery',
      },
      {
        id: 'rooms-info',
        title: 'Rooms info',
        translate: 'NAV.ROOMS_INFO',
        type: 'collapsable',
        icon: 'dashboard',
        children: [
          {
            id: 'rooms',
            title: 'Room List',
            translate: 'NAV.ROOMS_LIST',
            type: 'item',
            url: '/admin/rooms',
          },
          {
            id: 'room-parameters',
            title: 'Room Parameters',
            translate: 'NAV.ROOMS_PARAMETERS',
            type: 'item',
            url: '/admin/room-parameters',
          },
          {
            id: 'room-types',
            title: 'Room Types',
            translate: 'NAV.ROOM_TYPES',
            type: 'item',
            url: '/admin/room-types',
          },
        ]
      },
      {
        id: 'articles',
        title: 'Articles',
        translate: 'NAV.ARTICLES',
        type: 'item',
        icon: 'subtitles',
        url: '/admin/articles',
      },
      {
        id: 'article categories',
        title: 'Article Categories',
        translate: 'NAV.ARTICLE_CATEGORIES',
        type: 'item',
        icon: 'subtitles',
        url: '/admin/article-categories',
      },
      {
        id: 'wellness',
        title: 'Wellness',
        translate: 'NAV.WELLNESS',
        type: 'item',
        icon: 'spa',
        url: '/admin/wellness',
      },
    ]
  }
];
