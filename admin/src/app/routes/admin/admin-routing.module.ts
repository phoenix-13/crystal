import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminGuardService } from './admin-guard.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'info',
    pathMatch: 'full',
    canActivate: [AdminGuardService],
  },
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminGuardService],
    children: [
      {
        path: 'info',
        loadChildren: './routes/info/info.module#InfoModule'
      },
      {
        path: 'texts',
        loadChildren: './routes/texts/texts.module#TextsModule'
      },
      {
        path: 'meta',
        loadChildren: './routes/meta/meta.module#MetaModule'
      },
      {
        path: 'gallery',
        loadChildren: './routes/gallery/gallery.module#GalleryModule'
      },
      {
        path: 'rooms',
        loadChildren: './routes/rooms/rooms.module#RoomsModule'
      },
      {
        path: 'room-parameters',
        loadChildren: './routes/room-parameters/room-parameters.module#RoomParametersModule'
      },
      {
        path: 'articles',
        loadChildren: './routes/articles/articles.module#ArticlesModule'
      },
      {
        path: 'article-categories',
        loadChildren: './routes/article-categories/article-categories.module#ArticleCategoriesModule'
      },      
      {
        path: 'room-types',
        loadChildren: './routes/room-types/room-types.module#RoomTypesModule'
      },
      {
        path: 'wellness',
        loadChildren: './routes/wellness/wellness.module#WellnessModule'
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
