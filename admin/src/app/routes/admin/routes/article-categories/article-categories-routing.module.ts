import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleCategoriesComponent } from './article-categories.component';

const routes: Routes = [{
  path: '',
  component: ArticleCategoriesComponent,
  children: []
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleCategoriesRoutingModule { }
