import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Query } from '../../../../shared/models/query';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { ArticleCategory } from '../../../../shared/models/articleCategory';
import { Router, ActivatedRoute } from '@angular/router';
import { ArticleCategoryApiService } from '../../../../shared/http/article-category-api.service';
import { ConfirmDeleteModalComponent } from '../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { filter, tap, switchMap, share, map } from 'rxjs/operators';
import { ArticleCategoryModalComponent } from './shared/modals/article-category/article-category-modal.component';


@Component({
  selector: 'app-article-categories',
  templateUrl: './article-categories.component.html',
  styleUrls: ['./article-categories.component.scss']
})
export class ArticleCategoriesComponent {

  query: Query;
  items$: Observable<ArticleCategory[]>;
  numTotal$: Observable<number>;
  loadData$: Subject<Query>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ArticleCategoryApiService,
    private dialog: MatDialog,
  ) {
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/article-categories'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      switchMap(q => this.api.getByQuery(q)),
      share(),
    );

    this.items$ = data$.pipe(map(d => d.items));
    this.numTotal$ = data$.pipe(map(d => d.numTotal));
  }

  add() {
    const data: ArticleCategory = {
      title: {},
    };
    this.dialog
      .open(ArticleCategoryModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(d => {
          return this.api.create(d);
        }),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  update(data: any) {
    this.api.update(data)
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  delete(data: ArticleCategory) {
    this.dialog
      .open(ConfirmDeleteModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(() => this.api.delete(data._id)),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  reloadParams(query: Query) {
    this.query = { ...this.query, ...query };
    this.loadData$.next(this.query);
  }
}

function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}