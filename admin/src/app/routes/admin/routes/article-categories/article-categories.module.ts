import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleCategoriesRoutingModule } from './article-categories-routing.module';
import { ArticleCategoriesComponent } from './article-categories.component';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [ArticleCategoriesComponent],
  imports: [
    CommonModule,
    ArticleCategoriesRoutingModule,
    ComponentsModule,
    SharedModule,
  ],
  exports: [],
})
export class ArticleCategoriesModule { }
