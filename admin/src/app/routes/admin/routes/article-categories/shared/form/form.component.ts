import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ArticleCategory } from 'app/shared/models/articleCategory';
import { FormComponent as _FormComponent } from '../../../../../../shared/components/form.component';
import * as moment from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LLL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMM YYYY',
  },
};

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class FormComponent extends _FormComponent implements OnInit {

  @Input() formData: ArticleCategory;
  @Input() canSubmit: boolean = true;
  @Output() submitForm = new EventEmitter<ArticleCategory>();

  form: FormGroup;

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.formData.title = this.formData.title || {};

    this.form = this.fb.group({
      title: this.fb.group({
        ge: [this.formData.title.ge || ''],
        en: [this.formData.title.en || ''],
        ru: [this.formData.title.ru || ''],
      }),
    });
  }

  submit() {
    this.submitForm.emit(this.form.value);
  }
}
