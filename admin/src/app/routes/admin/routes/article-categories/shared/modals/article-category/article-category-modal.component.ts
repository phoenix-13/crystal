import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Article } from 'app/shared/models/article';
import { FormComponent } from 'app/shared/components/form.component';
import { FormComponent as _FormComponent } from '../../form/form.component';
import * as _ from 'lodash';
import { MetaFormComponent } from 'app/routes/admin/routes/shared/components/meta-form/meta-form.component';

@Component({
  selector: 'app-article-category-modal',
  templateUrl: './article-category-modal.component.html',
  styleUrls: ['./article-category-modal.component.scss']
})
export class ArticleCategoryModalComponent implements OnInit, AfterViewInit {

  metas: any; // metas -> meta
  filesToCreate: any[] = []; // remove
  filesToDestroy: any[] = []; // remove 
  showFormWarning: boolean = false;
  submitted: boolean = false;

  canSubmit = false; // canSubmit -> showSubmit

  @ViewChild('articleCategoryForm', { static: false }) articleCategoryFormComponent: _FormComponent;

  articleType: Article;

  constructor(private dialogRef: MatDialogRef<ArticleCategoryModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Article) { }

  formComponents: FormComponent[];

  ngOnInit() {
    // empty meta data object for making new meta object
    this.metas = {};
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.articleCategoryFormComponent,
    ];
  }

  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }

  onFinish() {
    this.showFormWarning = false;
    this.submitted = true;
    if (this.formsAreValid()) {
      this.dialogRef.close(this.getArticleData());
    } else {
      this.showFormWarning = true;
    }
  }

  getArticleData(): any {
    let data = _.cloneDeep(_.merge(
      this.articleType,
      this.articleCategoryFormComponent.getFormValue(),
    ));
    return data;
  }

} 
