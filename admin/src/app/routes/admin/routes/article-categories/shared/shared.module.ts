import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleCategoryModalComponent } from './modals/article-category/article-category-modal.component';
import { FormComponent } from './form/form.component';
import { ConfirmDeleteModalComponent } from '../../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { SharedModule as _SharedModule } from '../../shared/shared.module';


@NgModule({
   imports: [
      CommonModule,
      _SharedModule,
   ],
   exports: [_SharedModule, FormComponent],
   declarations: [FormComponent, ArticleCategoryModalComponent],
   entryComponents: [ArticleCategoryModalComponent, ConfirmDeleteModalComponent],
})
export class SharedModule { }