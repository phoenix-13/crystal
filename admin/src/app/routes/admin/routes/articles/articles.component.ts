import { Component } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ArticleModalComponent } from './shared/modals/article/article-modal.component';
import { Query } from '../../../../shared/models/query';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { Article } from '../../../../shared/models/article';
import { Router, ActivatedRoute } from '@angular/router';
import { ArticleApiService } from '../../../../shared/http/article-api.service';
import { LoadingService } from '../../../../shared/services/loading.service';
import { ConfirmDeleteModalComponent } from '../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { filter, tap, switchMap, share, map } from 'rxjs/operators';
import { FileApiService } from 'app/shared/http/files-api.service';


@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent {

  query: Query;
  items$: Observable<Article[]>;
  numTotal$: Observable<number>;
  loadData$: Subject<Query>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private api: ArticleApiService,
    private dialog: MatDialog,
    public fileApiService: FileApiService,
  ) {
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/articles'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      switchMap(q => this.api.getByQuery(q)),
      share(),
    );

    this.items$ = data$.pipe(map(d => d.items));
    this.numTotal$ = data$.pipe(map(d => d.numTotal));
  }

  add() {
    // creating data type with empty values for new element
    const data: Article = {
      title: {},
      description: {},
      thumbnail: {},
      content: {},
      isFeatured: true,
      meta: {},
    };
    // opens modal with empty data type for creating new element
    this.dialog
      .open(ArticleModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(d => {
          return this.api.create(d);
        }),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  // updates element in DataBase with new values
  update(data: any) {
    this.api.update(data)
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  // deletes element in DataBase
  delete(data: Article) {
    this.dialog
      .open(ConfirmDeleteModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(() => this.api.delete(data._id)),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  // reload query params
  reloadParams(query: Query) {
    this.query = { ...this.query, ...query };
    this.loadData$.next(this.query);
  }
}

function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}