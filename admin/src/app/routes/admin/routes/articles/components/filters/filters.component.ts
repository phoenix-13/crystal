import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Query } from 'app/shared/models/query';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  @Input() query: Query;
  @Output() queryChange = new EventEmitter<Query>();

  constructor() { }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    // converts entered value in lower case without spaces 
    this.query.keyword = filterValue.trim().toLowerCase();

    let val = this.query.keyword;
    this.queryChange.emit({ keyword: val });
  }

}
