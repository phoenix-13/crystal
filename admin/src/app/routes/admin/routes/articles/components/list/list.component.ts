import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatPaginator, PageEvent } from '@angular/material';
import { Article } from 'app/shared/models/article';
import { Query } from 'app/shared/models/query';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class ListComponent implements OnInit {
  @Input() items: any;
  @Input() numTotal: any;
  @Input() query: Query;

  @Output() queryChange = new EventEmitter<Query>();
  @Output() updateForm = new EventEmitter<Object>();
  @Output() deleteForm = new EventEmitter<Article>();
  @Output() updateMeta = new EventEmitter<Object>();

  // data source For data-table
  dataSource: Article[];
  pageEvent: PageEvent;
  pageLength: number;
  expandedElement: any;

  constructor() { }

  ngOnInit() {
    // subscribe for getting inputed data
    this.items.subscribe((data) => {
      this.dataSource = data;
    });

    this.numTotal.subscribe((data) => {
      this.pageLength = data;
    });
  }

  pagenatorEvent(pageData: any): any {
    this.queryChange.emit({
      page: pageData.pageIndex + 1,
      limit: pageData.pageSize,
    });
  }

  submitMeta(data: any, id: any) { // metaData -> data
    this.updateMeta.emit({ _id: id, ...data });
  }

  submitFormData(data: any, id: any) {
    this.updateForm.emit({ _id: id, ...data });
  }

  confirmDelete(event, element) {
    // prevent table element header from other click actions
    event.stopPropagation();
    this.deleteForm.emit(element);
  }
  columnsToDisplay = ['name', 'options'];
}

