import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Article } from 'app/shared/models/article';
import { FormComponent as _FormComponent } from '../../../../../../shared/components/form.component';
import * as moment from 'moment';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ArticleCategoryApiService } from '../../../../../../shared/http/article-category-api.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LLL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMM YYYY',
  },
};

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class FormComponent extends _FormComponent implements OnInit, AfterViewInit {

  @Input() formData: Article; // formData -> article
  @Input() canSubmit: boolean = true; // canSubmit -> showSubmit
  @Output() submitForm = new EventEmitter<Article>();


  form: FormGroup;
  selectedImage: any;
  filesToCreate: any[] = [];
  filesToDestroy: any[] = [];
  articleCategories: any[] = [];
  dataLoaded: boolean = false;

  constructor(
    private fb: FormBuilder,
    private categoriesApi: ArticleCategoryApiService,
  ) {
    super();
  }

  ngOnInit() {

    this.categoriesApi.getByQuery({all: true}).subscribe((data) => {
      this.articleCategories = data.items;

      this.formData.title = this.formData.title || {};
      this.formData.content = this.formData.content || {};
      this.formData.description = this.formData.description || {};
      this.formData.thumbnail = this.formData.thumbnail || {};
      this.formData.articleCategory = this.formData.articleCategory || {};

      setTimeout(() => {
        this.form = this.fb.group({
          title: this.fb.group({
            ge: [this.formData.title.ge || ''],
            en: [this.formData.title.en || ''],
            ru: [this.formData.title.ru || ''],
          }),
          description: this.fb.group({
            ge: [this.formData.description.ge || ''],
            en: [this.formData.description.en || ''],
            ru: [this.formData.description.ru || ''],
          }),
          content: this.fb.group({
            ge: [this.formData.content.ge || ''],
            en: [this.formData.content.en || ''],
            ru: [this.formData.content.ru || ''],
          }),
          articleCategory: [this.formData.articleCategory._id],
          createdAt: [moment(this.formData.createdAt)],
          thumbnail: this.fb.group({
            url: [this.formData.thumbnail.url || '']
          }),
        });
        this.dataLoaded = true;
      }, );

    })
  }

  ngAfterViewInit(): void {
    
  }

  onUploadComplete(data) {
    this.form.get('thumbnail').get('url').markAsTouched();
    this.form.get('thumbnail').get('url').setValue(data.url);
  }

  submit() {
    this.submitForm.emit(this.form.value);
  }
}
