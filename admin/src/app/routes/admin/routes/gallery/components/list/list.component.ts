import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatDialog, PageEvent } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Query } from 'app/shared/models/query';
import { Gallery } from 'app/shared/models/gallery';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListComponent implements OnInit {
  @Input() items: any;
  @Input() numTotal: any;
  @Input() query: Query;

  @Output() queryChange = new EventEmitter<Query>();
  @Output() updateGalleryForm = new EventEmitter<Object>();
  @Output() updateGalleryPhotosForm = new EventEmitter<Object>();
  @Output() deleteForm = new EventEmitter<Gallery>();
  @Output() updateMeta = new EventEmitter<Object>();


  galleryPhotosData: any;
  dataSource: Gallery[];
  pageEvent: PageEvent;
  pageLength: number;
  expandedElement: any;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
    this.items.subscribe((data) => {
      this.dataSource = data;
    });

    this.numTotal.subscribe((data) => {
      this.pageLength = data;
    });
  }

  pagenatorEvent(pageData: any): any {
    this.queryChange.emit({
      page: pageData.pageIndex + 1,
      limit: pageData.pageSize,
    });
  }

  submitMeta(metaData: any, id: number) {
    this.updateMeta.emit({ _id: id, ...metaData });
  }


  confirmDelete(event, element) {
    // prevent table element header from other click actions
    event.stopPropagation();
    this.deleteForm.emit(element);
  }

  submitGalleryForm(data: any, id: any) {
    this.updateGalleryForm.emit({ _id: id, ...data });
  }

  submitGalleryPhotos(data: any, id: any) {
    this.updateGalleryPhotosForm.emit({ _id: id, ...data });
  }

  columnsToDisplay = ['name', 'options'];

}
