import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { GalleryModalComponent } from './shared/modals/gallery/gallery-modal.component';
import { Query } from 'app/shared/models/query';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Gallery } from 'app/shared/models/gallery';
import { GalleryApiService } from 'app/shared/http/gallery-api.service';
import { FileApiService } from 'app/shared/http/files-api.service';
import { LoadingService } from 'app/shared/services/loading.service';
import { Router, ActivatedRoute } from '@angular/router';
import { map, tap, switchMap, share, filter } from 'rxjs/operators';
import { ConfirmDeleteModalComponent } from '../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import * as moment from 'moment';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent {
  query: Query;
  items$: Observable<Gallery[]>;
  numTotal$: Observable<number>;
  loadData$: Subject<Query>;

  constructor(
    private dialog: MatDialog,
    private api: GalleryApiService,
    private router: Router,
    private route: ActivatedRoute,
    public spinner: LoadingService,
    public fileApiService: FileApiService,
  ) {
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/gallery'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      tap(() => this.spinner.start()),
      switchMap(q => this.api.getByQuery(q)),
      tap(this.spinner.getStopHandler()),
      share(),
    );

    this.items$ = data$.pipe(map(d => d.items));
    this.numTotal$ = data$.pipe(map(d => d.numTotal));

    // this.items$.subscribe((data) => console.log('data', data));

  }

  add() {
    const data: Gallery = {
      title: {},
      description: {},
      images: [],
      date: moment(),
      thumbnail: {},
    };

    this.dialog
      .open(GalleryModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        tap(() => this.spinner.start()),
        switchMap(d => {
          d.images = d.images.map(({ url, file }) => ({ url: url || file.name }));
          d.filesToCreate = d.filesToCreate.filter(data => data.file).map(({ file }) => file);

          return this.fileApiService.createFiles(d.filesToCreate, d.filesToDestroy)
            .pipe(switchMap(() =>
              this.api.create(d)));
        }),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  update(data: any) {
    this.api.update(data)
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  updateGallery(data: any) {
    data.images = data.images.map(({ url, file }) => ({ url: url || file.name }));
    data.filesToCreate = data.filesToCreate.filter(data => data.file).map(({ file }) => file);
    this.fileApiService.createFiles(data.filesToCreate, data.filesToDestroy)
      .pipe(switchMap(() => {
        return this.api.update(data);
      })).subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  delete(data: Gallery) {
    this.dialog
      .open(ConfirmDeleteModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        tap(() => this.spinner.start()),
        switchMap(() => this.api.delete(data._id)),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  reloadParams(query: Query) {
    this.query = { ...this.query, ...query };
    this.loadData$.next(this.query);
  }

}


function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}