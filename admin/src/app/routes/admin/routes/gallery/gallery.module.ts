import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [GalleryComponent],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    ComponentsModule,
    SharedModule
  ],
  exports: [GalleryComponent]
})
export class GalleryModule { }
