import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Gallery } from 'app/shared/models/gallery';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { FormComponent } from 'app/shared/components/form.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-gallery-form',
  templateUrl: './gallery-form.component.html',
  styleUrls: ['./gallery-form.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})

export class GalleryFormComponent extends FormComponent implements OnInit {
  @Input() galleryFormData: Gallery;
  @Input() canSubmit: boolean = true;
  @Output() submitGalleryForm = new EventEmitter<Gallery>();

  form: FormGroup;
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      title: this.fb.group({
        ge: [this.galleryFormData.title.ge || ''],
        en: [this.galleryFormData.title.en || ''],
        ru: [this.galleryFormData.title.ru || ''],
      }),
      description: this.fb.group({
        ge: [this.galleryFormData.description.ge || ''],
        en: [this.galleryFormData.description.en || ''],
        ru: [this.galleryFormData.description.ru || ''],
      }),
      thumbnail: this.fb.group({
        url: [this.galleryFormData.thumbnail.url || '']
      }),
      date: [this.galleryFormData.date || moment()],
    });

  }

  onImageSelected(selectedImage) {
    this.form.get('thumbnail').get('url').markAsTouched();
    this.form.get('thumbnail').get('url').setValue(selectedImage);
  }

  onUploadComplete(data) {
    this.form.get('thumbnail').get('url').markAsTouched();
    this.form.get('thumbnail').get('url').setValue(data.url);
  }

  submit() {
    this.submitGalleryForm.emit(this.form.value);
  }

}
