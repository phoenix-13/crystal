import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormComponent } from 'app/shared/components/form.component';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent extends FormComponent implements OnInit {
  @Input() images: any[];
  @Output() submitGalleryPhotos = new EventEmitter<any>();
  @Input() canSubmit: boolean = true;

  filesToDestroy = [];
  filesToCreate = [];

  form: FormGroup;
  isPhoto = false;
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      images: [this.images || []]
    });

    if (this.images.length !== 0) {
      this.isPhoto = true;
    }
  }

  onImageSelected(image) {
    this.isPhoto = true;
    this.form.get('images').markAsTouched();
    let images = this.form.get('images').value;
    images.push(image);
    this.form.get('images').setValue(images);
    this.filesToCreate.push(image);
  }

  onImageRemoved(index) {
    this.form.get('images').markAsTouched();
    let images = this.form.get('images').value;
    let [image] = images.splice(index, 1);
    if (!image.file) {
      this.filesToDestroy.push(image.url);
    }
    this.form.get('images').setValue(images);

    if (this.form.get('images').value <= 0) {
      this.isPhoto = false;
    }
  }

  getFilesData() {
    return {
      filesToCreate: this.filesToCreate,
      filesToDestroy: this.filesToDestroy,
    };
  }

  submit() {
    let data = {
      filesToCreate: this.filesToCreate,
      filesToDestroy: this.filesToDestroy,
    };
    this.submitGalleryPhotos.emit({ ...data, ...this.form.value });
  }

}
