import { Component, OnInit, Inject, AfterViewInit, ViewChild, ViewChildren } from '@angular/core';
import { FormComponent } from 'app/shared/components/form.component';
import * as _ from 'lodash';
import { Gallery } from 'app/shared/models/gallery';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MetaFormComponent } from 'app/routes/admin/routes/shared/components/meta-form/meta-form.component';
import { GalleryFormComponent } from '../../form/gallery-form/gallery-form.component';
import { ImagesComponent } from '../../form/images/images.component';

@Component({
  selector: 'app-gallery-modal',
  templateUrl: './gallery-modal.component.html',
  styleUrls: ['./gallery-modal.component.scss']
})
export class GalleryModalComponent implements OnInit, AfterViewInit {
  metas: any;
  formComponents: FormComponent[];
  galleryType: Gallery;
  submitted: boolean = false;

  @ViewChild('metaForm', { static: false }) galleryMetaComponent: MetaFormComponent;
  @ViewChild('galleryForm', { static: false }) galleryFormComponent: GalleryFormComponent;
  @ViewChild('galleryPhotoForm', { static: false }) galleryPhotoFormComponent: ImagesComponent;

  constructor(private dialogRef: MatDialogRef<GalleryModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Gallery) { }

  ngOnInit() {
    this.metas = {};
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.galleryMetaComponent,
      this.galleryFormComponent,
      this.galleryPhotoFormComponent,
    ];
  }


  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }

  onFinish() {
    this.submitted = true;
    if (this.formsAreValid()) {
      this.dialogRef.close(this.getGalleryData());
    } else {
      console.log('modal error');
    }
  }

  getGalleryData(): any {
    let data = _.cloneDeep(_.merge(
      this.galleryType,
      this.galleryMetaComponent.getFormValue(),
      this.galleryFormComponent.getFormValue(),
      this.galleryPhotoFormComponent.getFormValue(),
      this.galleryPhotoFormComponent.getFilesData(),
    ));

    return data;
  }

}
