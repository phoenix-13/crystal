import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeleteModalComponent } from '../../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { SharedModule as _SharedModule } from '../../shared/shared.module';
import { GalleryFormComponent } from './form/gallery-form/gallery-form.component';
import { ImagesComponent } from './form/images/images.component';
import { GalleryModalComponent } from './modals/gallery/gallery-modal.component';

@NgModule({
  declarations: [GalleryFormComponent, ImagesComponent, GalleryModalComponent],
  imports: [
    CommonModule,
    _SharedModule,
  ],
  exports: [
    _SharedModule,
    GalleryFormComponent,
    ImagesComponent,
    GalleryModalComponent,
  ],
  entryComponents: [ConfirmDeleteModalComponent, GalleryModalComponent]
})
export class SharedModule { }
