import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { CommonApiService } from 'app/shared/http/common-api.service';
import { Common } from 'app/shared/models/common';
import { FormComponent } from 'app/shared/components/form.component';
import { BannerComponent } from './shared/banner/banner.component';
import * as _ from 'lodash';

declare const google;

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit, AfterViewInit {


  ContactForm: FormGroup;
  PromoForm: FormGroup;
  aboutUsForm: FormGroup;
  homeSections: FormGroup;
  wellnessForm: FormGroup;
  filterLimitsForm: FormGroup;
  defaultMealPriceRangesForm: FormGroup;
  location: any;
  formComponents: FormComponent[];
  formdata: any;

  // map variables
  map: any;
  address: any = {};
  invalidAddress: boolean = false;
  locationOutOfBounds: boolean = true;

  @ViewChild('aboutBanner', { static: false }) AboutBannerForm: BannerComponent;
  @ViewChild('wellnessBanner', { static: false }) WellnessBannerForm: BannerComponent;
  @ViewChild('roomsBanner', { static: false }) RoomsBannerForm: BannerComponent;
  @ViewChild('roomBanner', { static: false }) RoomBannerForm: BannerComponent;
  @ViewChild('newsBanner', { static: false }) NewsBannerForm: BannerComponent;
  @ViewChild('articleBanner', { static: false }) ArticleBannerForm: BannerComponent;
  @ViewChild('galleryBanner', { static: false }) GalleryBannerForm: BannerComponent;
  @ViewChild('contactBanner', { static: false }) ContactBannerForm: BannerComponent;
  @ViewChild('mapElement', { static: false }) mapElement;


  constructor(
    private fb: FormBuilder,
    private api: CommonApiService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  createMap(location, mapZoom) {
    let center = new google.maps.LatLng(location.lat, location.lng);
    let mapOptions = {
      zoom: mapZoom || 16,
      zoomControl: true,
      mapTypeControl: true,
      streetViewControl: false,
      fullscreenControl: true,
      center: center,
      draggable: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_TOP
      }
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let marker = new google.maps.Marker({
      position: center,
      map: this.map,
    });

    google.maps.event.addListener(this.map, 'dragend', () => this.updateLocation());
    google.maps.event.addListener(this.map, 'zoom', () => this.updateZoom());
    google.maps.event.addListener(this.map, 'drag', () => marker.setPosition(this.map.getCenter().toJSON()));
  }


  updateLocation() {
    return this.map.getCenter().toJSON();
  }

  updateZoom() {
    return this.map.getZoom();
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.AboutBannerForm,
      this.WellnessBannerForm,
      this.RoomsBannerForm,
      this.RoomBannerForm,
      this.NewsBannerForm,
      this.ArticleBannerForm,
      this.GalleryBannerForm,
      this.ContactBannerForm,
    ];

    setTimeout(() => {
      this.createMap(this.formdata.contact.location, this.formdata.contact.mapZoom);
    }, 1500);

  }

  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }

  initForm(data: any) {
    this.formdata = data || {};
    
    this.formdata.promo.headerLogo = data.promo.headerLogo || {
      ge: {},
      en: {},
      ru: {},
    };
    this.formdata.promo.footerLogo = data.promo.footerLogo || {
      ge: {},
      en: {},
      ru: {},
    };
    // this.formdata.contact.phone = this.formdata.contact.phone || {};

    this.ContactForm = this.fb.group({
      contact: this.fb.group({
        phone: [data.contact.phone || '', Validators.required],
        email: [data.contact.email || '', [Validators.required, Validators.email]],
        address: this.fb.group({
          ge: [data.contact.address.ge || ''],
          en: [data.contact.address.en || ''],
          ru: [data.contact.address.ru || ''],
        }),
        socialLinks: this.fb.array((data.contact.socialLinks || []).map(socialLink => this.createSocialLink(socialLink))),
        // facebookUrl: [data.contact.facebookUrl || ''],
        // instagramUrl: [data.contact.instagramUrl || ''],
        // linkedinUrl: [data.contact.linkedinUrl || ''],
        // twitterUrl: [data.contact.twitterUrl || ''],
      }),
    });

    this.PromoForm = this.fb.group({
      promo: this.fb.group({
        promoImage: this.fb.group({
          ge: this.fb.group({
            url: [data.promo.promoImage.ge.url || ''],
          }),
          en: this.fb.group({
            url: [data.promo.promoImage.en.url || ''],
          }),
          ru: this.fb.group({
            url: [data.promo.promoImage.ru.url || ''],
          }),
        }),
        headerLogo: this.fb.group({
          ge: this.fb.group({
            url: [data.promo.headerLogo.ge.url || ''],
          }),
          en: this.fb.group({
            url: [data.promo.headerLogo.en.url || ''],
          }),
          ru: this.fb.group({
            url: [data.promo.headerLogo.ru.url || ''],
          }),
        }),

        footerLogo: this.fb.group({
          ge: this.fb.group({
            url: [data.promo.footerLogo.ge.url || ''],
          }),
          en: this.fb.group({
            url: [data.promo.footerLogo.en.url || ''],
          }),
          ru: this.fb.group({
            url: [data.promo.footerLogo.ru.url || ''],
          }),
        }),

        headerColor: [data.promo.headerColor || ''],
        langColor: [data.promo.langColor || ''],
      }),
    });

    this.aboutUsForm = this.fb.group({
      aboutUs: this.fb.group({
        image: this.fb.group({
          url: [data.aboutUs.image.url || ''],
        }),
        title: this.fb.group({
          ge: [data.aboutUs.title.ge || ''],
          en: [data.aboutUs.title.en || ''],
          ru: [data.aboutUs.title.ru || ''],
        }),
        subtitle: this.fb.group({
          ge: [data.aboutUs.subtitle.ge || ''],
          en: [data.aboutUs.subtitle.en || ''],
          ru: [data.aboutUs.subtitle.ru || ''],
        }),
        content: this.fb.group({
          ge: [data.aboutUs.content.ge || ''],
          en: [data.aboutUs.content.en || ''],
          ru: [data.aboutUs.content.ru || ''],
        }),
        contactPhone: [data.aboutUs.contactPhone || ''],
        contactText: this.fb.group({
          ge: [data.aboutUs.contactText.ge || ''],
          en: [data.aboutUs.contactText.en || ''],
          ru: [data.aboutUs.contactText.ru || ''],
        }),
      }),
    });

    this.homeSections = this.fb.group({
      homeSections: this.fb.group({
        rooms: this.fb.group({
          title: this.fb.group({
            ge: [data.homeSections.rooms.title.ge || ''],
            en: [data.homeSections.rooms.title.en || ''],
            ru: [data.homeSections.rooms.title.ru || ''],
          }),
          description: this.fb.group({
            ge: [data.homeSections.rooms.description.ge || ''],
            en: [data.homeSections.rooms.description.en || ''],
            ru: [data.homeSections.rooms.description.ru || ''],
          }),
          button: this.fb.group({
            ge: [data.homeSections.rooms.button.ge || ''],
            en: [data.homeSections.rooms.button.en || ''],
            ru: [data.homeSections.rooms.button.ru || ''],
          }),

        }),
        gallery: this.fb.group({
          title: this.fb.group({
            ge: [data.homeSections.gallery.title.ge || ''],
            en: [data.homeSections.gallery.title.en || ''],
            ru: [data.homeSections.gallery.title.ru || ''],
          }),
          description: this.fb.group({
            ge: [data.homeSections.gallery.description.ge || ''],
            en: [data.homeSections.gallery.description.en || ''],
            ru: [data.homeSections.gallery.description.ru || ''],
          }),
        }),
        contact: this.fb.group({
          title: this.fb.group({
            ge: [data.homeSections.contact.title.ge || ''],
            en: [data.homeSections.contact.title.en || ''],
            ru: [data.homeSections.contact.title.ru || ''],
          }),
          description: this.fb.group({
            ge: [data.homeSections.contact.description.ge || ''],
            en: [data.homeSections.contact.description.en || ''],
            ru: [data.homeSections.contact.description.ru || ''],
          }),
        })
      })
    });

    this.wellnessForm = this.fb.group({
      wellness: this.fb.group({
        contactPhone: [data.wellness.contactPhone || ''],
        contactText: this.fb.group({
          ge: [data.wellness.contactText.ge || ''],
          en: [data.wellness.contactText.en || ''],
          ru: [data.wellness.contactText.ru || ''],
        }),
        image1: this.fb.group({
          url: [data.wellness.image1.url || '']
        }),
        image2: this.fb.group({
          url: [data.wellness.image2.url || '']
        }),
        image3: this.fb.group({
          url: [data.wellness.image3.url || '']
        }),
        image4: this.fb.group({
          url: [data.wellness.image4.url || '']
        }),
        image5: this.fb.group({
          url: [data.wellness.image5.url || '']
        }),
      }),
    });

    data.filterLimits = data.filterLimits || {};

    this.filterLimitsForm = this.fb.group({
      filterLimits: this.fb.group({
        from: [data.filterLimits.from || 0],
        to: [data.filterLimits.to || 1000],
      }),
    });

    data.defaultMealPriceRanges = data.defaultMealPriceRanges || {};

    this.defaultMealPriceRangesForm = this.fb.group({
      defaultMealPriceRanges: this.fb.group({
        singleMealPrice: [data.defaultMealPriceRanges.singleMealPrice || 10],
        doubleMealPrice: [data.defaultMealPriceRanges.doubleMealPrice || 10],
        tripleMealPrice: [data.defaultMealPriceRanges.tripleMealPrice || 10],
      }),
    })

  }

  get socialLinksForms() {
    // console.log("this.ContactForm.get('contact').", this.ContactForm.get('contact'));
    return this.ContactForm.get('contact').get('socialLinks') as FormArray;
  }
  
  addSocialLink() {
  
    const socialLink = this.fb.group({ 
      icon: [],
      url: [],
    })
  
    this.socialLinksForms.push(socialLink);
  }
  
  deleteSocialLink(i) {
    this.socialLinksForms.removeAt(i)
  }

  createSocialLink(socialLink): FormGroup {
    return this.fb.group({
      icon: [socialLink.icon],
      url: [socialLink.url]
    });
  }
  

  loadData() {
    // setTimeout(() => this.spinner.start());
    this.api.getOne()
      .subscribe((data) => {
        this.initForm(data);
      });
  }

  update(data: any) {
    // this.spinner.start()
    this.api.update(data)
      .subscribe(() => {
        this.api.getOne();
      }, (data) => {
      });
  }

  submitContact() {
    // for map
    let contactData = {
      contact: {
        ...this.ContactForm.value.contact,
        ...{ 
          location: this.updateLocation(), 
          mapZoom: this.updateZoom(),
        }
      }
    };
    console.log('confirmed address: ', contactData);

    this.update(contactData);
  }

  submitPromo() {
    this.update(this.PromoForm.value);
  }

  submitAboutUs() {
    this.update(this.aboutUsForm.value);
  }

  submitMealPrices(data: any) {
    this.update(data);
  }

  submitHomeSections() {
    this.update(this.homeSections.value);
  }

  submitWellnes() {
    this.update(this.wellnessForm.value);
  }

  submitFilterLimitsForm() {
    this.update(this.filterLimitsForm.value);
  }

  submitDefaultMealPriceRanges() {
    this.update(this.defaultMealPriceRangesForm.value);
  }

  onSubmitBanners() {
    if (this.formsAreValid()) {
      this.update({ banners: this.getFormData() });
    } else {
      console.log('create error');
    }
  }

  onUploadCompleteGe(data) {
    this.PromoForm.get('promo').get('promoImage').get('ge').get('url').markAsTouched();
    this.PromoForm.get('promo').get('promoImage').get('ge').get('url').setValue(data.url);
  }
  onUploadCompleteEn(data) {
    this.PromoForm.get('promo').get('promoImage').get('en').get('url').markAsTouched();
    this.PromoForm.get('promo').get('promoImage').get('en').get('url').setValue(data.url);
  }
  onUploadCompleteRu(data) {
    this.PromoForm.get('promo').get('promoImage').get('ru').get('url').markAsTouched();
    this.PromoForm.get('promo').get('promoImage').get('ru').get('url').setValue(data.url);
  }

  onUploadCompleteHeaderLogoGE(data) {
    this.PromoForm.get('promo').get('headerLogo').get('ge').get('url').markAsTouched();
    this.PromoForm.get('promo').get('headerLogo').get('ge').get('url').setValue(data.url);
  }

  onUploadCompleteHeaderLogoEN(data) {
    this.PromoForm.get('promo').get('headerLogo').get('en').get('url').markAsTouched();
    this.PromoForm.get('promo').get('headerLogo').get('en').get('url').setValue(data.url);
  }

  onUploadCompleteHeaderLogoRU(data) {
    this.PromoForm.get('promo').get('headerLogo').get('ru').get('url').markAsTouched();
    this.PromoForm.get('promo').get('headerLogo').get('ru').get('url').setValue(data.url);
  }

  onUploadCompleteFooterLogoGE(data) {
    this.PromoForm.get('promo').get('footerLogo').get('ge').get('url').markAsTouched();
    this.PromoForm.get('promo').get('footerLogo').get('ge').get('url').setValue(data.url);
  }

  onUploadCompleteFooterLogoEN(data) {
    this.PromoForm.get('promo').get('footerLogo').get('en').get('url').markAsTouched();
    this.PromoForm.get('promo').get('footerLogo').get('en').get('url').setValue(data.url);
  }

  onUploadCompleteFooterLogoRU(data) {
    this.PromoForm.get('promo').get('footerLogo').get('ru').get('url').markAsTouched();
    this.PromoForm.get('promo').get('footerLogo').get('ru').get('url').setValue(data.url);
  }

  aboutUsImageUpload(data) {
    this.aboutUsForm.get('aboutUs').get('image').get('url').markAsTouched();
    this.aboutUsForm.get('aboutUs').get('image').get('url').setValue(data.url);
  }

  onWellnesImage1upload(data) {
    this.wellnessForm.get('wellness').get('image1').get('url').markAllAsTouched();
    this.wellnessForm.get('wellness').get('image1').get('url').setValue(data.url);
  }
  onWellnesImage2upload(data) {
    this.wellnessForm.get('wellness').get('image2').get('url').markAllAsTouched();
    this.wellnessForm.get('wellness').get('image2').get('url').setValue(data.url);
  }
  onWellnesImage3upload(data) {
    this.wellnessForm.get('wellness').get('image3').get('url').markAllAsTouched();
    this.wellnessForm.get('wellness').get('image3').get('url').setValue(data.url);
  }
  onWellnesImage4upload(data) {
    this.wellnessForm.get('wellness').get('image4').get('url').markAllAsTouched();
    this.wellnessForm.get('wellness').get('image4').get('url').setValue(data.url);
  }
  onWellnesImage5upload(data) {
    this.wellnessForm.get('wellness').get('image5').get('url').markAllAsTouched();
    this.wellnessForm.get('wellness').get('image5').get('url').setValue(data.url);
  }

  getFormData(): any {
    let data = {
      about: this.AboutBannerForm.getFormValue(),
      wellness: this.WellnessBannerForm.getFormValue(),
      rooms: this.RoomsBannerForm.getFormValue(),
      room: this.RoomBannerForm.getFormValue(),
      news: this.NewsBannerForm.getFormValue(),
      article: this.ArticleBannerForm.getFormValue(),
      gallery: this.GalleryBannerForm.getFormValue(),
      contact: this.ContactBannerForm.getFormValue(),
    };
    return data;
  }
}
