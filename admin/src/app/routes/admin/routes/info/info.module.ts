import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfoRoutingModule } from './info-routing.module';
import { InfoComponent } from './info.component';
import { SharedModule } from '../shared/shared.module';
import { BannerComponent } from './shared/banner/banner.component';
import { MealPricesComponent } from './shared/meal-prices/meal-prices.component';

@NgModule({
  declarations: [InfoComponent, BannerComponent, MealPricesComponent],
  imports: [
    CommonModule,
    InfoRoutingModule,
    SharedModule,
  ],
  exports: [InfoComponent, BannerComponent, MealPricesComponent]
})
export class InfoModule { }
