import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormComponent } from 'app/shared/components/form.component';

@Component({
  selector: 'app-banner-form',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent extends FormComponent implements OnInit {
  
  @Input() bannersData: any;
  @Output() submitBanners = new EventEmitter<any>();

  form: FormGroup;
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      title: this.fb.group({
        ge: [this.bannersData.title.ge || ''],
        en: [this.bannersData.title.en || ''],
        ru: [this.bannersData.title.ru || ''],
      }),
      image: this.fb.group({
        url: [this.bannersData.image.url || ''],
      }),
    });
  }

  onUploadComplete(data) {
    this.form.get('image').get('url').markAsTouched();
    this.form.get('image').get('url').setValue(data.url);
  }

  submit() {
    this.submitBanners.emit(this.form.value);
  }

}
