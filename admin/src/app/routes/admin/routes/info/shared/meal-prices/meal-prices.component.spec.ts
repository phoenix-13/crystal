import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MealPricesComponent } from './meal-prices.component';

describe('MealPricesComponent', () => {
  let component: MealPricesComponent;
  let fixture: ComponentFixture<MealPricesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MealPricesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealPricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
