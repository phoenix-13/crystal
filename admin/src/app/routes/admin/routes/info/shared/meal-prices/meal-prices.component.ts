import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import * as moment from 'moment';
import { MealPrices } from 'app/shared/models/meal-prices';


export const MY_FORMATS = {
  parse: {
    dateInput: 'LLL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMM YYYY',
  },
};

@Component({
  selector: 'app-meal-prices',
  templateUrl: './meal-prices.component.html',
  styleUrls: ['./meal-prices.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class MealPricesComponent implements OnInit {
  @Input() mealPricesData: any;
  @Output() submitMealPrices = new EventEmitter<MealPrices>();
  @Input() canSubmit: boolean = true;

  form: FormGroup;
  submitted: boolean;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      priceRangeDates: this.fb.array(this.mealPricesData.map(item => this.createItem(item)))
    });
  }

  get dateForms() {
    return this.form.get('priceRangeDates') as FormArray;
  }

  createItem(item): FormGroup {
    return this.fb.group({
      startDate: [moment(item.startDate), [Validators.required]],
      endDate: [moment(item.endDate), [Validators.required]],
      singleMealPrice: [item.singleMealPrice, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
      doubleMealPrice: [item.doubleMealPrice, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
      tripleMealPrice: [item.tripleMealPrice, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
    }, { validator: this.dateDiffValidator.bind(this) });
  }

  addForm() {
    const array = this.fb.group({
      startDate: [moment(), [Validators.required]],
      endDate: [moment().add(1, 'day'), [Validators.required]],
      singleMealPrice: [null, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
      doubleMealPrice: [null, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
      tripleMealPrice: [null, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
    }, { validator: this.dateDiffValidator.bind(this) });

    this.dateForms.push(array);
  }

  checkValueChange() {
    var arrayControl = this.form.get('priceRangeDates') as FormArray;
    let jStart;
    let jEnd;
    let iStart;
    let iEnd;

    for (let i = 0; i < this.form.value.priceRangeDates.length; i++) {
      iStart = moment(arrayControl.at(i).get('startDate').value).startOf('day');
      iEnd = moment(arrayControl.at(i).get('endDate').value).startOf('day');

      for (let j = 0; j < this.form.value.priceRangeDates.length; j++) {
        if (i !== j) {
          jStart = moment(arrayControl.at(j).get('startDate').value).startOf('day');
          jEnd = moment(arrayControl.at(j).get('endDate').value).startOf('day');

          if (moment(iEnd).isBetween(jStart, jEnd) || moment(iStart).isBetween(jStart, jEnd)
            || moment(iEnd).isSame(jEnd) || moment(iStart).isSame(jStart)) {
            return false;
          }
        }
      }
    }
    return true;
  }

  deleteForm(i) {
    this.dateForms.removeAt(i);
  }

  dateDiffValidator(form: FormGroup): { [key: string]: boolean } {
    let startDateVal = moment(form.get('startDate').value).format("MM-DD-YYYY");
    let endDateVal = moment(form.get('endDate').value).format("MM-DD-YYYY");

    if (moment(endDateVal).isAfter(startDateVal)) {
      return null;
    } else {
      return { 'dateDiffValidator': true };
    }
  }

  submit() {
    console.log('form submitted data', this.form.value);

    this.submitted = true;
    this.submitMealPrices.emit(this.form.value);
  }

}
