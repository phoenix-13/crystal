import { Component, OnInit } from '@angular/core';
import { FormComponent } from 'app/shared/components/form.component';
import { MetaApiService } from 'app/shared/http/meta-api.service';

@Component({
  selector: 'app-meta',
  templateUrl: './meta.component.html',
  styleUrls: ['./meta.component.scss']
})
export class MetaComponent implements OnInit {
  metas: any;

  constructor(private api: MetaApiService) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.api.getOne()
      .subscribe((data) => {
        this.metas = data;
      });
  }

  update(data: any, pageTitle: any) {

    // this.spinner.start()
    this.api.update({ [pageTitle]: data.meta })
      .subscribe(() => {
        this.api.getOne();
      }, (data) => {
        console.log('after update', data);
      });
  }

}
