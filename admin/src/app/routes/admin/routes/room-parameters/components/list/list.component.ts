import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { MatDialog, PageEvent } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { RoomParameters } from 'app/shared/models/room-parameters';
import { Query } from 'app/shared/models/query';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListComponent implements OnInit {
  @Input() items: any;
  @Input() numTotal: any;
  @Input() query: Query;

  @Output() queryChange = new EventEmitter<Query>();
  @Output() updateForm = new EventEmitter<any>();
  @Output() deleteForm = new EventEmitter<RoomParameters>();

  dataSource: RoomParameters[];
  pageEvent: PageEvent;
  pageLength: number;
  expandedElement: any;

  columnsToDisplay = ['name', 'options'];

  constructor() { }

  ngOnInit(): any {
    this.items.subscribe((data) => {
      this.dataSource = data;
    });

    this.numTotal.subscribe((data) => {
      this.pageLength = data;
    });
  }

  pagenatorEvent(pageData: any): any {
    this.queryChange.emit({
      page: pageData.pageIndex + 1,
      limit: pageData.pageSize,
    });
  }

  confirmDelete(event, element): any {
    // prevent table element header from other click actions
    event.stopPropagation();
    this.deleteForm.emit(element);
  }

  submitRoomParameters(data: any, id: any): any {
    this.updateForm.emit({ _id: id, ...data });
  }






}





