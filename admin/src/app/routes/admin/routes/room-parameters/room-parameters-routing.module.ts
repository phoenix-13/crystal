import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoomParametersComponent } from './room-parameters.component';

const routes: Routes = [{
  path: '',
  component: RoomParametersComponent,
  children: []
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomParametersRoutingModule { }
