import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomParametersComponent } from './room-parameters.component';

describe('RoomParametersComponent', () => {
  let component: RoomParametersComponent;
  let fixture: ComponentFixture<RoomParametersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomParametersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomParametersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
