import { Component } from '@angular/core';
import { RoomParameterModalComponent } from './shared/modals/room-parameter/room-parameter-modal.component';
import { MatDialog } from '@angular/material';
import { Query } from 'app/shared/models/query';
import { RoomParameters } from 'app/shared/models/room-parameters';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { RoomParameterApiService } from 'app/shared/http/room-parameter-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, share, map, filter } from 'rxjs/operators';
import { ConfirmDeleteModalComponent } from '../shared/modals/confirm-delete-modal/confirm-delete-modal.component';

@Component({
  selector: 'app-room-parameters',
  templateUrl: './room-parameters.component.html',
  styleUrls: ['./room-parameters.component.scss']
})
export class RoomParametersComponent {
  query: Query;
  items$: Observable<RoomParameters[]>;
  numTotal$: Observable<number>;
  loadData$: Subject<Query>;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private api: RoomParameterApiService,
  ) {
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/room-parameters'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      switchMap(q => this.api.getByQuery(q)),
      share(),
    );

    this.items$ = data$.pipe(map(d => d.items));
    this.numTotal$ = data$.pipe(map(d => d.numTotal));
  }

  add() {
    const data: RoomParameters = {
      name: {},
      icon: {},
    };
    this.dialog
      .open(RoomParameterModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(d => {
          return this.api.create(d);
        }),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  update(data: any) {
    this.api.update(data)
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  delete(data: RoomParameters) {
    this.dialog
      .open(ConfirmDeleteModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(() => this.api.delete(data._id)),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  reloadParams(query: Query) {
    this.query = { ...this.query, ...query };
    this.loadData$.next(this.query);
  }

}

function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}