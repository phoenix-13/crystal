import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomParametersRoutingModule } from './room-parameters-routing.module';
import { RoomParametersComponent } from './room-parameters.component';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [RoomParametersComponent],
  imports: [
    CommonModule,
    RoomParametersRoutingModule,
    ComponentsModule,
    SharedModule
  ],
  exports: [RoomParametersComponent]
})
export class RoomParametersModule { }
