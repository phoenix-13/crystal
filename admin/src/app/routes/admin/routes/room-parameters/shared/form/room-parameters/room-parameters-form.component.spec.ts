import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomParametersFormComponent } from './room-parameters-form.component';

describe('RoomParametersFormComponent', () => {
  let component: RoomParametersFormComponent;
  let fixture: ComponentFixture<RoomParametersFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomParametersFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomParametersFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
