import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RoomParameters } from 'app/shared/models/room-parameters';
import { FormComponent } from 'app/shared/components/form.component';


@Component({
  selector: 'app-room-parameters-form',
  templateUrl: './room-parameters-form.component.html',
  styleUrls: ['./room-parameters-form.component.scss']
})
export class RoomParametersFormComponent extends FormComponent implements OnInit {
  @Input() roomParametersFormData: RoomParameters;
  @Output() submitRoomParameters = new EventEmitter<RoomParameters>();
  @Input() canSubmit: boolean = true;

  form: FormGroup;
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: this.fb.group({
        ge: [this.roomParametersFormData.name.ge || ''],
        en: [this.roomParametersFormData.name.en || ''],
        ru: [this.roomParametersFormData.name.ru || ''],
      }),
      icon: this.fb.group({
        url: [this.roomParametersFormData.icon.url || ''],
      }),
    });
  }

  onUploadComplete(data) {
    this.form.get('icon').get('url').markAsTouched();
    this.form.get('icon').get('url').setValue(data.url);
  }

  submit() {
    this.submitRoomParameters.emit(this.form.value);
  }
}
