import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomParameterModalComponent } from './room-parameter-modal.component';

describe('RoomParameterModalComponent', () => {
  let component: RoomParameterModalComponent;
  let fixture: ComponentFixture<RoomParameterModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RoomParameterModalComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomParameterModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
