import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { RoomParameters } from 'app/shared/models/room-parameters';
import { FormComponent } from 'app/shared/components/form.component';
import { RoomParametersFormComponent } from '../../form/room-parameters/room-parameters-form.component';
import * as _ from 'lodash';


@Component({
  selector: 'app-room-parameter-modal',
  templateUrl: './room-parameter-modal.component.html',
  styleUrls: ['./room-parameter-modal.component.scss']
})
export class RoomParameterModalComponent implements OnInit, AfterViewInit {
  formComponents: FormComponent[];
  roomParameterType: RoomParameters;

  @ViewChild('roomParametersForm', { static: false }) roomParametersForm: RoomParametersFormComponent;

  constructor(
    private dialogRef: MatDialogRef<RoomParameterModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoomParameters
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.roomParametersForm
    ];
  }

  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }


  onFinish() {
    if (this.formsAreValid()) {
      this.dialogRef.close(this.getFormData());
    } else {
      console.log('create error');
    }
  }

  getFormData(): any {
    let data = _.cloneDeep(_.merge(
      this.roomParameterType,
      this.roomParametersForm.getFormValue(),
    ));
    return data;
  }

}
