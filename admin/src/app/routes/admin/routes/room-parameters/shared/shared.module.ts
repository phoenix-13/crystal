import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule as _SharedModule } from '../../shared/shared.module';
import { ConfirmDeleteModalComponent } from '../../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { RoomParametersFormComponent } from './form/room-parameters/room-parameters-form.component';
import { RoomParameterModalComponent } from './modals/room-parameter/room-parameter-modal.component';

@NgModule({
  declarations: [RoomParametersFormComponent, RoomParameterModalComponent],
  imports: [
    CommonModule,
    _SharedModule,
  ],
  exports: [
    _SharedModule,
    RoomParametersFormComponent,
    RoomParameterModalComponent,
  ],
  entryComponents: [ConfirmDeleteModalComponent, RoomParameterModalComponent],
})
export class SharedModule { }
