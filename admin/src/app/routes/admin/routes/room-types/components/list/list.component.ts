import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Query } from 'app/shared/models/query';
import { RoomTypes } from 'app/shared/models/room-types';
import { PageEvent, MatTable } from '@angular/material';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListComponent implements OnInit {
  @Input() items: any;
  @Input() numTotal: any;
  @Input() query: Query;

  @Output() queryChange = new EventEmitter<Query>();
  @Output() updateForm = new EventEmitter<any>();
  @Output() deleteForm = new EventEmitter<RoomTypes>();
  @Output() updatePositions = new EventEmitter<any>();

  @ViewChild('table', { static: false }) table: MatTable<ListComponent>;

  dataSource: RoomTypes[];
  pageEvent: PageEvent;
  pageLength: number;
  expandedElement: any;

  columnsToDisplay = ['name', 'options'];

  constructor() { }


  ngOnInit(): any {
    this.items.subscribe((data) => {
      this.dataSource = data;
    });

    this.numTotal.subscribe((data) => {
      this.pageLength = data;
    });
  }

  pagenatorEvent(pageData: any): any {
    this.queryChange.emit({
      page: pageData.pageIndex + 1,
      limit: pageData.pageSize,
    });
  }

  confirmDelete(event, element): any {
    // prevent table element header from other click actions
    event.stopPropagation();
    this.deleteForm.emit(element);
  }

  submitRoomTypes(data: any, id: any): any {
    this.updateForm.emit({ _id: id, ...data });
  }


  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.dataSource, event.previousIndex, event.currentIndex);
    this.table.renderRows();
    let page = this.query.page - 1;
    let limit = this.query.limit;
    const data = this.dataSource.map((item, index) => {
      return {
        position: index + (page * limit),
        _id: item._id,
      };
    });

    this.updatePositions.emit({ items: data });
  }
}
