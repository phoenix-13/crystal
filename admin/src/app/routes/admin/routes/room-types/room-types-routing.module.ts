import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoomTypesComponent } from './room-types.component';

const routes: Routes = [{
  path: '',
  component: RoomTypesComponent,
  children: []
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoomTypesRoutingModule { }
