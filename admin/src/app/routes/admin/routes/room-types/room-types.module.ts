import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomTypesRoutingModule } from './room-types-routing.module';
import { RoomTypesComponent } from './room-types.component';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [RoomTypesComponent],
  imports: [
    CommonModule,
    RoomTypesRoutingModule,
    ComponentsModule,
    SharedModule
  ],
  exports: [RoomTypesComponent]
})
export class RoomTypesModule { }
