import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FormComponent } from 'app/shared/components/form.component';
import { RoomTypes } from 'app/shared/models/room-types';

@Component({
  selector: 'app-room-types-form',
  templateUrl: './room-types.component.html',
  styleUrls: ['./room-types.component.scss']
})
export class RoomTypesComponent extends FormComponent implements OnInit {
  @Input() roomTypesData: RoomTypes;
  @Output() submitRoomTypes = new EventEmitter<RoomTypes>();
  @Input() canSubmit: boolean = true;

  form: FormGroup;
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: this.fb.group({
        ge: [this.roomTypesData.name.ge || ''],
        en: [this.roomTypesData.name.en || ''],
        ru: [this.roomTypesData.name.ru || ''],
      }),
    });
  }

  submit() {
    this.submitRoomTypes.emit(this.form.value);
  }
}
