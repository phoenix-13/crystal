import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomTypesModalComponent } from './room-types-modal.component';

describe('RoomTypesModalComponent', () => {
  let component: RoomTypesModalComponent;
  let fixture: ComponentFixture<RoomTypesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomTypesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomTypesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
