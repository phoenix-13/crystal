import { Component, OnInit, ViewChild, Inject, AfterViewInit } from '@angular/core';
import { RoomTypes } from 'app/shared/models/room-types';
import { FormComponent } from 'app/shared/components/form.component';
import { RoomTypesComponent } from '../../form/room-types/room-types.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as _ from 'lodash';


@Component({
  selector: 'app-room-types-modal',
  templateUrl: './room-types-modal.component.html',
  styleUrls: ['./room-types-modal.component.scss']
})
export class RoomTypesModalComponent implements OnInit, AfterViewInit {
  formComponents: FormComponent[];
  roomTypeType: RoomTypes;

  @ViewChild('roomTypeForm', { static: false }) roomTypeFormComponent: RoomTypesComponent;

  constructor(
    private dialogRef: MatDialogRef<RoomTypesModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoomTypes
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.roomTypeFormComponent
    ];
  }


  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }

  onFinish() {
    if (this.formsAreValid()) {
      this.dialogRef.close(this.getFormData());
    } else {
      console.log('create error');
    }
  }

  getFormData(): any {
    let data = _.cloneDeep(_.merge(
      this.roomTypeType,
      this.roomTypeFormComponent.getFormValue(),
    ));
    return data;
  }
}
