import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoomTypesComponent } from './form/room-types/room-types.component';
import { RoomTypesModalComponent } from './modals/room-types-modal/room-types-modal.component';
import { SharedModule as _SharedModule } from '../../shared/shared.module';
import { ConfirmDeleteModalComponent } from '../../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
@NgModule({
  declarations: [RoomTypesComponent, RoomTypesModalComponent],
  imports: [
    CommonModule,
    _SharedModule,
  ],
  exports: [_SharedModule, RoomTypesComponent, RoomTypesModalComponent],
  entryComponents: [RoomTypesModalComponent, ConfirmDeleteModalComponent]
})
export class SharedModule { }
