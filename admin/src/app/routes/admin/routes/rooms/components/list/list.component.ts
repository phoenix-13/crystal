import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { PageEvent } from '@angular/material';
import { Query } from 'app/shared/models/query';
import { Rooms } from 'app/shared/models/rooms';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListComponent implements OnInit {
  @Input() items: any;
  @Input() numTotal: any;
  @Input() query: Query;

  @Output() queryChange = new EventEmitter<Query>();
  @Output() updateForm = new EventEmitter<Object>();
  @Output() updatePhotosForm = new EventEmitter<Object>();
  @Output() updatePriceRangeForm = new EventEmitter<Object>();
  @Output() deleteForm = new EventEmitter<Rooms>();
  @Output() updateMeta = new EventEmitter<Object>();


  dataSource: Rooms[];
  pageEvent: PageEvent;
  pageLength: number;
  expandedElement: any;

  columnsToDisplay = ['name', 'options'];

  constructor() { }

  ngOnInit() {
    this.items.subscribe((data) => {
      this.dataSource = data;
    });

    this.numTotal.subscribe((data) => {
      this.pageLength = data;
    });
  }

  pagenatorEvent(pageData: any): any {
    this.queryChange.emit({
      page: pageData.pageIndex + 1,
      limit: pageData.pageSize,
    });
  }

  submitMeta(data: any, id: any) {
    this.updateMeta.emit({ _id: id, ...data });
  }

  submitRoomsForm(data: any, id: any) {
    this.updateForm.emit({ _id: id, ...data });
  }

  submitPriceRangeFormData(data: any, id: any) {
    this.updatePriceRangeForm.emit({ _id: id, ...data });
  }

  submitRoomPhotos(data: any, id: any) {
    this.updatePhotosForm.emit({ _id: id, ...data });
  }


  confirmDelete(event, element) {
    // prevent table element header from other click actions
    event.stopPropagation();
    this.deleteForm.emit(element);
  }

}

