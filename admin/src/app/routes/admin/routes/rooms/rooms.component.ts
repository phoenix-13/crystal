import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { RoomModalComponent } from './shared/modals/room/room-modal.component';
import { Query } from 'app/shared/models/query';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { RoomApiService } from 'app/shared/http/room-api.service';
import { switchMap, share, map, filter } from 'rxjs/operators';
import { Rooms } from 'app/shared/models/rooms';
import { ConfirmDeleteModalComponent } from '../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { FileApiService } from 'app/shared/http/files-api.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent {

  query: Query;
  items$: Observable<Rooms[]>;
  numTotal$: Observable<number>;
  loadData$: Subject<Query>;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private api: RoomApiService,
    private fileApiService: FileApiService,
  ) {
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/rooms'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      switchMap(q => this.api.getByQuery(q)),
      share(),
    );

    this.items$ = data$.pipe(map(d => d.items));
    this.numTotal$ = data$.pipe(map(d => d.numTotal));
  }

  add() {
    const data: Rooms = {
      name: {},
      stars: 4,
      description: {},
      thumbnail: {},
      images: [],
      type: {},
      defaultPrice: 0,
      priceForAdditionalPerson: 0,
      priceRangeDates: [],
      meta: {},
      features: [],
      numberOfGuests: 0,
      numberOfAdditionalGuests: 0,
      infantFriendly: false,
    };
    this.dialog.open(RoomModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(d => {
          d.images = d.images.map(({ url, file }) => ({ url: url || file.name }));
          d.filesToCreate = d.filesToCreate.filter(data => data.file).map(({ file }) => file);

          return this.fileApiService.createFiles(d.filesToCreate, d.filesToDestroy)
            .pipe(switchMap(() => this.api.create(d)));
        }),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  update(data: any) {
    this.api.update(data)
      .subscribe(() => {

        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  updateGallery(data: any) {
    data.images = data.images.map(({ url, file }) => ({ url: url || file.name }));
    data.filesToCreate = data.filesToCreate.filter(data => data.file).map(({ file }) => file);
    this.fileApiService.createFiles(data.filesToCreate, data.filesToDestroy)
      .pipe(switchMap(() => {
        return this.api.update(data);
      })).subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  delete(data: Rooms) {
    this.dialog
      .open(ConfirmDeleteModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(() => this.api.delete(data._id)),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  reloadParams(query: Query) {
    this.query = { ...this.query, ...query };
    this.loadData$.next(this.query);
  }


}

function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}