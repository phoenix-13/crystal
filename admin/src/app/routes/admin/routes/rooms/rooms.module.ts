import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomsRoutingModule } from './rooms-routing.module';
import { RoomsComponent } from './rooms.component';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [RoomsComponent],
  imports: [
    CommonModule,
    RoomsRoutingModule,
    ComponentsModule,
    SharedModule,
  ],
  exports: [RoomsComponent]
})
export class RoomsModule { }
