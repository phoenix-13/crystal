import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormComponent } from 'app/shared/components/form.component';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-images-form',
  templateUrl: './images-form.component.html',
  styleUrls: ['./images-form.component.scss']
})
export class ImagesFormComponent extends FormComponent implements OnInit {
  @Input() images: any[];
  @Output() submitRoomPhotos = new EventEmitter<any>();
  @Input() canSubmit: boolean = true;

  filesToDestroy = [];
  filesToCreate = [];

  form: FormGroup;
  isPhoto = false;
  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      images: [this.images || []]
    });

    if (this.images.length !== 0) {
      this.isPhoto = true;
    }
  }

  onImageSelected(image) {
    this.isPhoto = true;
    this.form.get('images').markAsTouched();
    let images = this.form.get('images').value;
    images.push(image);
    this.form.get('images').setValue(images);

    this.filesToCreate.push(image);
  }

  onImageRemoved(index) {
    this.form.get('images').markAsTouched();
    let images = this.form.get('images').value;
    let [image] = images.splice(index, 1);
    if (!image.file) {
      this.filesToDestroy.push(image.url);
    }
    this.form.get('images').setValue(images);

    if (this.form.get('images').value <= 0) {
      this.isPhoto = false;
    }
  }

  getFilesData() {
    return {
      filesToCreate: this.filesToCreate,
      filesToDestroy: this.filesToDestroy,
      images: this.form.get('images').value,
    };
  }

  submit() {
    let data = {
      filesToCreate: this.filesToCreate,
      filesToDestroy: this.filesToDestroy,
    };
    this.submitRoomPhotos.emit({ ...data, ...this.form.value });
  }

}
