import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { Rooms } from 'app/shared/models/rooms';
import * as _moment from 'moment';
import { FormComponent } from 'app/shared/components/form.component';


const moment = _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LLL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMM YYYY',
  },
};
@Component({
  selector: 'app-price-range-form',
  templateUrl: './price-range-form.component.html',
  styleUrls: ['./price-range-form.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ]
})
export class PriceRangeFormComponent extends FormComponent implements OnInit {
  @Input() priceRangeFormData: any;
  @Output() submitPriceRangeFormData = new EventEmitter<Rooms>();
  @Input() canSubmit: boolean = true;

  form: FormGroup;
  submitted: boolean;

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      priceRangeDates: this.fb.array(this.priceRangeFormData.map(item => this.createItem(item)))
    });
  }

  createItem(item): FormGroup {
    return this.fb.group({
      startDate: [moment(item.startDate), [Validators.required]],
      endDate: [moment(item.endDate), [Validators.required]],
      defaultPrice: [item.defaultPrice || '', [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
      priceForAdditionalPerson: [item.priceForAdditionalPerson || '', [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
    }, { validator: this.dateDiffValidator.bind(this) });
  }

  get dateForms() {
    return this.form.get('priceRangeDates') as FormArray;
  }

  addForm() {
    const array = this.fb.group({
      startDate: [moment(), [Validators.required]],
      endDate: [moment().add(1, 'day'), [Validators.required]],
      defaultPrice: [null, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
      priceForAdditionalPerson: [null, [Validators.required, Validators.pattern('^(0|[1-9][0-9]*)$')]],
    }, { validator: this.dateDiffValidator.bind(this) });

    this.dateForms.push(array);
  }

  checkValueChange() {
    var arrayControl = this.form.get('priceRangeDates') as FormArray;
    let jStart;
    let jEnd;
    let iStart;
    let iEnd;

    for (let i = 0; i < this.form.value.priceRangeDates.length; i++) {
      iStart = moment(arrayControl.at(i).get('startDate').value).startOf('day');
      iEnd = moment(arrayControl.at(i).get('endDate').value).startOf('day');

      for (let j = 0; j < this.form.value.priceRangeDates.length; j++) {
        if (i !== j) {
          jStart = moment(arrayControl.at(j).get('startDate').value).startOf('day');
          jEnd = moment(arrayControl.at(j).get('endDate').value).startOf('day');

          if (moment(iEnd).isBetween(jStart, jEnd) || moment(iStart).isBetween(jStart, jEnd)
            || moment(iEnd).isSame(jEnd) || moment(iStart).isSame(jStart)) {
            return false;
          }
        }
      }
    }
    return true;
  }

  deleteForm(i) {
    this.dateForms.removeAt(i);
  }

  submit() {

    if (!this.checkValueChange()) {
      console.log('error');
    }
    else {
      this.submitPriceRangeFormData.emit(this.form.value);
      this.submitted = true;
    }
  }

  dateDiffValidator(form: FormGroup): { [key: string]: boolean } {
    let startDateVal = moment(form.get('startDate').value).format("MM-DD-YYYY");
    let endDateVal = moment(form.get('endDate').value).format("MM-DD-YYYY");

    console.log('startDateVal', startDateVal);
    console.log('endDateVal', endDateVal);

    console.log(` --- isAfter --- `, moment(endDateVal).isAfter(startDateVal));

    if (moment(endDateVal).isAfter(startDateVal)) {
      return null;
    } else {
      return { 'dateDiffValidator': true };
    }
  }
}




