import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Rooms } from 'app/shared/models/rooms';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { MAT_CHECKBOX_CLICK_ACTION } from '@angular/material';
import { RoomParameterApiService } from 'app/shared/http/room-parameter-api.service';
import { Query } from 'app/shared/models/query';
import { Router, ActivatedRoute } from '@angular/router';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { switchMap, share, map } from 'rxjs/operators';
import { RoomParameters } from 'app/shared/models/room-parameters';
import { FormComponent } from 'app/shared/components/form.component';
import { RoomTypesApiService } from 'app/shared/http/room-types-api.service';
import { RoomTypes } from 'app/shared/models/room-types';

@Component({
  selector: 'app-rooms-form',
  templateUrl: './rooms-form.component.html',
  styleUrls: ['./rooms-form.component.scss'],
  providers: [
    { provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check' }
  ]
})
export class RoomsFormComponent extends FormComponent implements OnInit {
  @Input() roomsFormData: Rooms;
  @Output() submitRoomsForm = new EventEmitter<Rooms>();
  @Input() canSubmit: boolean = true;

  @ViewChild('checkbox', { static: false }) checkboxEvent: any;


  query: Query;
  form: FormGroup;
  loadData$: Subject<Query>;
  types = [1,2,3,4,5];
  features: any[];
  roomTypesArray: any[];
  newFeatures$: Observable<RoomParameters[]>;
  roomTypes$: Observable<RoomTypes[]>;
  infantFriendly: boolean;

  constructor(
    private fb: FormBuilder,
    private featuresApi: RoomParameterApiService,
    private roomTypesApi: RoomTypesApiService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    super();
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/rooms'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      switchMap(q => this.featuresApi.getByQuery(q)),
      share(),
    );

    const roomTypeData$ = this.loadData$.pipe(
      switchMap(q => this.roomTypesApi.getByQuery(q)),
      share(),
    );

    this.roomTypes$ = roomTypeData$.pipe(map(d => d.items));
    this.newFeatures$ = data$.pipe(map(d => d.items));
  }

  ngOnInit() {
    this.newFeatures$.subscribe((data) => {
      this.features = data;
    });

    this.roomTypes$.subscribe((data) => {
      this.roomTypesArray = data;
    });

    this.form = this.fb.group({
      name: this.fb.group({
        ge: [this.roomsFormData.name.ge || ''],
        en: [this.roomsFormData.name.en || ''],
        ru: [this.roomsFormData.name.ru || ''],
      }),
      stars: [this.roomsFormData.stars || ''],
      defaultPrice: [this.roomsFormData.defaultPrice || '', Validators.pattern('^(0|[1-9][0-9]*)$')],
      priceForAdditionalPerson: [this.roomsFormData.priceForAdditionalPerson || '', Validators.pattern('^(0|[1-9][0-9]*)$')],
      features: [this.roomsFormData.features || []],
      description: this.fb.group({
        ge: [this.roomsFormData.description.ge || ''],
        en: [this.roomsFormData.description.en || ''],
        ru: [this.roomsFormData.description.ru || ''],
      }),
      type: [this.roomsFormData.type, Validators.required],
      thumbnail: this.fb.group({
        url: [this.roomsFormData.thumbnail.url || '']
      }),
      numberOfGuests: [this.roomsFormData.numberOfGuests || ''],
      numberOfAdditionalGuests: [this.roomsFormData.numberOfAdditionalGuests || ''],
      infantFriendly: [this.roomsFormData.infantFriendly || false],
    });


    if (this.roomsFormData.infantFriendly === true) {
      this.infantFriendly = true;
    } else {
      this.infantFriendly = false;
    }
  }

  onUploadComplete(data) {
    this.form.get('thumbnail').get('url').markAsTouched();
    this.form.get('thumbnail').get('url').setValue(data.url);
  }

  toggleInfants(e: any) {
    if (e === 1) {
      this.form.get('infantFriendly').setValue(true);
    } else {
      this.form.get('infantFriendly').setValue(false);
    }
  }

  // toggleFeature(_id: number) {
  //   var elementItem = this.form.get('features').value.find(element => element._id === _id);
  //   var featureToAdd = this.features.find(element => element._id === _id);

  //   var mapedParameters = this.form.get('features').value.map(element => element._id).indexOf(_id);

  //   if (elementItem) {
  //     this.form.get('features').value.splice(mapedParameters, 1);
  //   } else {
  //     this.form.get('features').value.push(featureToAdd);
  //   }
  // }

  // isFeatureActive(feature) {
  //   let selectedFeatures = this.form.get('features').value;

  //   if (selectedFeatures.find(data => data._id === feature._id)) {
  //     return selectedFeatures.find(data => data._id === feature._id);
  //   }
  // }

  toggleFeature(feature) {

    if (this.form.get('features').value.indexOf(feature._id) > -1) {
      let currentFeatures = this.form.get('features').value;
      currentFeatures = currentFeatures.filter((value) => value !== feature._id);
      this.form.get('features').setValue(currentFeatures);
    } else {
      let currentFeatures = this.form.get('features').value;
      currentFeatures = [...currentFeatures, feature._id];
      this.form.get('features').setValue(currentFeatures);
    }
  }

  // isFeatureActive(feature) {
  //   let selectedFeatures = this.form.get('features').value;
  //   return selectedFeatures.indexOf(feature._id) > -1;
  // }

  submit() {
    this.submitRoomsForm.emit(this.form.value);
  };

}

function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}