import { Component, OnInit, Inject, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Rooms } from 'app/shared/models/rooms';
import { FormComponent } from 'app/shared/components/form.component';
import { RoomsFormComponent } from '../../form/rooms-form/rooms-form.component';
import { MetaFormComponent } from 'app/routes/admin/routes/shared/components/meta-form/meta-form.component';
import { PriceRangeFormComponent } from '../../form/price-range/price-range-form.component';
import { ImagesFormComponent } from '../../form/images/images-form.component';
import * as _ from 'lodash';

@Component({
  selector: 'app-room-modal',
  templateUrl: './room-modal.component.html',
  styleUrls: ['./room-modal.component.scss']
})
export class RoomModalComponent implements OnInit, AfterViewInit {
  metas: any;
  formComponents: FormComponent[];
  roomType: Rooms;

  @ViewChild('roomForm', { static: false }) roomFormComponent: RoomsFormComponent;
  @ViewChild('metaForm', { static: false }) metaFormComponent: MetaFormComponent;
  @ViewChild('priceRangeForm', { static: false }) priceRangeFormComponent: PriceRangeFormComponent;
  @ViewChild('roomPhotosForm', { static: false }) roomPhotosFormComponent: ImagesFormComponent;

  constructor(private dialogRef: MatDialogRef<RoomModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Rooms) { }

  ngOnInit() {
    this.metas = {};
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.roomFormComponent,
      this.metaFormComponent,
      this.priceRangeFormComponent,
      this.roomPhotosFormComponent,
    ];
  }

  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }

  onFinish() {
    if (this.formsAreValid()) {
      this.dialogRef.close(this.getFormsData());
      console.log('data to create', this.getFormsData());

    } else {
      console.log('modal error');
    }
  }

  getFormsData(): any {
    let data = _.cloneDeep(_.merge(
      this.roomType,
      this.roomFormComponent.getFormValue(),
      this.metaFormComponent.getFormValue(),
      this.priceRangeFormComponent.getFormValue(),
      this.roomPhotosFormComponent.getFilesData(),
    ));

    return data;
  }

} 
