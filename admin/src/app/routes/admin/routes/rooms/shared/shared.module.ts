import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule as _SharedModule } from '../../shared/shared.module';
import { ConfirmDeleteModalComponent } from '../../shared/modals/confirm-delete-modal/confirm-delete-modal.component';
import { RoomsFormComponent } from './form/rooms-form/rooms-form.component';
import { PriceRangeFormComponent } from './form/price-range/price-range-form.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { RoomModalComponent } from './modals/room/room-modal.component';
import { ImagesFormComponent } from './form/images/images-form.component';

@NgModule({
  declarations: [RoomsFormComponent, PriceRangeFormComponent, RoomModalComponent, ImagesFormComponent],
  imports: [
    CommonModule,
    _SharedModule,
    MatMomentDateModule,
  ],
  exports: [
    _SharedModule,
    RoomsFormComponent,
    PriceRangeFormComponent,
    MatMomentDateModule,
    RoomModalComponent,
    ImagesFormComponent,
  ],
  entryComponents: [ConfirmDeleteModalComponent, RoomModalComponent]
})
export class SharedModule { }
