import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeleteModalComponent } from './modals/confirm-delete-modal/confirm-delete-modal.component';
import { MatTabsModule, MatInputModule, MatButtonModule, MatFormFieldModule, MatChipsModule, MatIconModule, MatSortModule, MatPaginatorModule, MatDialogModule, MatTableModule, MatTooltipModule, MatSelectModule, MatCheckboxModule, MatDatepickerModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { MetaFormComponent } from './components/meta-form/meta-form.component';
import { SharedModule as GlobalSharedModule } from '../../../../shared/shared.module';


@NgModule({
  declarations: [ConfirmDeleteModalComponent, MetaFormComponent],
  imports: [
    CommonModule,
    MatTabsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    QuillModule,
    MatChipsModule,
    MatIconModule,
    GlobalSharedModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTooltipModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,

  ],

  exports: [
    ConfirmDeleteModalComponent,
    MatTabsModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatChipsModule,
    MatIconModule,
    QuillModule,
    MetaFormComponent,
    MatSortModule,
    GlobalSharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTooltipModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
  ]
})
export class SharedModule { }
