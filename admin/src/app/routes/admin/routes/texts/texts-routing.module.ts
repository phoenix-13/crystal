import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TextsComponent } from './texts.component';


const routes: Routes = [{
  path: '',
  component: TextsComponent,
  children: []
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TextsRoutingModule { }
