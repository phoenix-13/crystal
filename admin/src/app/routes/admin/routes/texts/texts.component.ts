import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../../../shared/http/common-api.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-texts',
  templateUrl: './texts.component.html',
  styleUrls: ['./texts.component.scss']
})
export class TextsComponent implements OnInit {

  data: any;
  textsForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private api: CommonApiService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.api.getOne().subscribe((data) => {
      this.initForm(data);
    });
  }

  initForm(data: any) {
    this.data = data || {};

    this.data.staticTexts = this.data.staticTexts || {};
    
    this.data.staticTexts.header = this.data.staticTexts.header || {};
    this.data.staticTexts.header.about = this.data.staticTexts.header.about || {};
    this.data.staticTexts.header.wellness = this.data.staticTexts.header.wellness || {};
    this.data.staticTexts.header.rooms = this.data.staticTexts.header.rooms || {};
    this.data.staticTexts.header.news = this.data.staticTexts.header.news || {};
    this.data.staticTexts.header.gallery = this.data.staticTexts.header.gallery || {};
    this.data.staticTexts.header.contact = this.data.staticTexts.header.contact || {};

    this.data.staticTexts.room = this.data.staticTexts.room || {};
    this.data.staticTexts.room.startsFrom_prefix = this.data.staticTexts.room.startsFrom_prefix || {};
    this.data.staticTexts.room.startsFrom_suffix = this.data.staticTexts.room.startsFrom_suffix || {};

    this.data.staticTexts.room.night = this.data.staticTexts.room.night || {};
    this.data.staticTexts.room.description = this.data.staticTexts.room.description || {};
    this.data.staticTexts.room.bookNow = this.data.staticTexts.room.bookNow || {};
    this.data.staticTexts.room.from = this.data.staticTexts.room.from || {};

    this.data.staticTexts.room.emailField = this.data.staticTexts.room.emailField || {};
    this.data.staticTexts.room.phoneField = this.data.staticTexts.room.phoneField || {};
    this.data.staticTexts.room.bookingMessage = this.data.staticTexts.room.bookingMessage || {};
    this.data.staticTexts.room.breakfast = this.data.staticTexts.room.breakfast || {};
    this.data.staticTexts.room.doubleMeal = this.data.staticTexts.room.doubleMeal || {};
    this.data.staticTexts.room.tripleMeal = this.data.staticTexts.room.tripleMeal || {};
    this.data.staticTexts.room.bookingButton = this.data.staticTexts.room.bookingButton || {};


    this.data.staticTexts.filters = this.data.staticTexts.filters || {};
    this.data.staticTexts.filters.moreFilters = this.data.staticTexts.filters.moreFilters || {};
    this.data.staticTexts.filters.priceRange = this.data.staticTexts.filters.priceRange || {};
    this.data.staticTexts.filters.stars = this.data.staticTexts.filters.stars || {};
    this.data.staticTexts.filters.roomTypes = this.data.staticTexts.filters.roomTypes || {};
    this.data.staticTexts.filters.search = this.data.staticTexts.filters.search || {};
    this.data.staticTexts.filters.clear = this.data.staticTexts.filters.clear || {};
    this.data.staticTexts.filters.resultsNotFound = this.data.staticTexts.filters.resultsNotFound || {};

    this.data.staticTexts.global = this.data.staticTexts.global || {};
    this.data.staticTexts.global.startDate = this.data.staticTexts.global.startDate || {};
    this.data.staticTexts.global.endDate = this.data.staticTexts.global.endDate || {};
    this.data.staticTexts.global.adults = this.data.staticTexts.global.adults || {};
    this.data.staticTexts.global.children = this.data.staticTexts.global.children || {};
    this.data.staticTexts.global.infants = this.data.staticTexts.global.infants || {};
    this.data.staticTexts.global.search = this.data.staticTexts.global.search || {};
    this.data.staticTexts.global.photos = this.data.staticTexts.global.photos || {};
    this.data.staticTexts.global.mail = this.data.staticTexts.global.mail || {};
    this.data.staticTexts.global.phone = this.data.staticTexts.global.phone || {};
    this.data.staticTexts.global.address = this.data.staticTexts.global.address || {};
    this.data.staticTexts.global.social = this.data.staticTexts.global.social || {};
    this.data.staticTexts.global.name = this.data.staticTexts.global.name || {};
    this.data.staticTexts.global.message = this.data.staticTexts.global.message || {};
    this.data.staticTexts.global.send = this.data.staticTexts.global.send || {};
    this.data.staticTexts.global.category = this.data.staticTexts.global.category || {};
    this.data.staticTexts.global.startBooking = this.data.staticTexts.global.startBooking || {};
    this.data.staticTexts.global.bookVisit = this.data.staticTexts.global.bookVisit || {};
    this.data.staticTexts.global.callNow = this.data.staticTexts.global.callNow || {};

    this.data.staticTexts.successMessages = this.data.staticTexts.successMessages || {};
    this.data.staticTexts.successMessages.contactMessage = this.data.staticTexts.successMessages.contactMessage || {};
    this.data.staticTexts.successMessages.bookingMessage = this.data.staticTexts.successMessages.bookingMessage || {};
    this.data.staticTexts.successMessages.button = this.data.staticTexts.successMessages.button || {};
    

    this.textsForm = this.fb.group({

      header: this.fb.group({
        about: this.fb.group({
          ge: [this.data.staticTexts.header.about.ge || ''],
          en: [this.data.staticTexts.header.about.en || ''],
          ru: [this.data.staticTexts.header.about.ru || ''],
        }),
        wellness: this.fb.group({
          ge: [this.data.staticTexts.header.wellness.ge || ''],
          en: [this.data.staticTexts.header.wellness.en || ''],
          ru: [this.data.staticTexts.header.wellness.ru || ''],
        }),
        rooms: this.fb.group({
          ge: [this.data.staticTexts.header.rooms.ge || ''],
          en: [this.data.staticTexts.header.rooms.en || ''],
          ru: [this.data.staticTexts.header.rooms.ru || ''],
        }),
        news: this.fb.group({
          ge: [this.data.staticTexts.header.news.ge || ''],
          en: [this.data.staticTexts.header.news.en || ''],
          ru: [this.data.staticTexts.header.news.ru || ''],
        }),
        gallery: this.fb.group({
          ge: [this.data.staticTexts.header.gallery.ge || ''],
          en: [this.data.staticTexts.header.gallery.en || ''],
          ru: [this.data.staticTexts.header.gallery.ru || ''],
        }),
        contact: this.fb.group({
          ge: [this.data.staticTexts.header.contact.ge || ''],
          en: [this.data.staticTexts.header.contact.en || ''],
          ru: [this.data.staticTexts.header.contact.ru || ''],
        }),
      }),
    
      room: this.fb.group({
        startsFrom_prefix: this.fb.group({
          ge: [this.data.staticTexts.room.startsFrom_prefix.ge || ''],
          en: [this.data.staticTexts.room.startsFrom_prefix.en || ''],
          ru: [this.data.staticTexts.room.startsFrom_prefix.ru || ''],
        }),
        startsFrom_suffix: this.fb.group({
          ge: [this.data.staticTexts.room.startsFrom_suffix.ge || ''],
          en: [this.data.staticTexts.room.startsFrom_suffix.en || ''],
          ru: [this.data.staticTexts.room.startsFrom_suffix.ru || ''],
        }),
        night: this.fb.group({
          ge: [this.data.staticTexts.room.night.ge || ''],
          en: [this.data.staticTexts.room.night.en || ''],
          ru: [this.data.staticTexts.room.night.ru || ''],
        }),
        description: this.fb.group({
          ge: [this.data.staticTexts.room.description.ge || ''],
          en: [this.data.staticTexts.room.description.en || ''],
          ru: [this.data.staticTexts.room.description.ru || ''],
        }),
        bookNow: this.fb.group({
          ge: [this.data.staticTexts.room.bookNow.ge || ''],
          en: [this.data.staticTexts.room.bookNow.en || ''],
          ru: [this.data.staticTexts.room.bookNow.ru || ''],
        }),
        from: this.fb.group({
          ge: [this.data.staticTexts.room.from.ge || ''],
          en: [this.data.staticTexts.room.from.en || ''],
          ru: [this.data.staticTexts.room.from.ru || ''],
        }),
        bookingMessage: this.fb.group({
          ge: [this.data.staticTexts.room.bookingMessage.ge || ''],
          en: [this.data.staticTexts.room.bookingMessage.en || ''],
          ru: [this.data.staticTexts.room.bookingMessage.ru || ''],
        }),
        breakfast: this.fb.group({
          ge: [this.data.staticTexts.room.breakfast.ge || ''],
          en: [this.data.staticTexts.room.breakfast.en || ''],
          ru: [this.data.staticTexts.room.breakfast.ru || ''],
        }),
        doubleMeal: this.fb.group({
          ge: [this.data.staticTexts.room.doubleMeal.ge || ''],
          en: [this.data.staticTexts.room.doubleMeal.en || ''],
          ru: [this.data.staticTexts.room.doubleMeal.ru || ''],
        }),
        tripleMeal: this.fb.group({
          ge: [this.data.staticTexts.room.tripleMeal.ge || ''],
          en: [this.data.staticTexts.room.tripleMeal.en || ''],
          ru: [this.data.staticTexts.room.tripleMeal.ru || ''],
        }),
        bookingButton: this.fb.group({
          ge: [this.data.staticTexts.room.bookingButton.ge || ''],
          en: [this.data.staticTexts.room.bookingButton.en || ''],
          ru: [this.data.staticTexts.room.bookingButton.ru || ''],
        }),
        emailField: this.fb.group({
          ge: [this.data.staticTexts.room.emailField.ge || ''],
          en: [this.data.staticTexts.room.emailField.en || ''],
          ru: [this.data.staticTexts.room.emailField.ru || ''],
        }),
        phoneField: this.fb.group({
          ge: [this.data.staticTexts.room.phoneField.ge || ''],
          en: [this.data.staticTexts.room.phoneField.en || ''],
          ru: [this.data.staticTexts.room.phoneField.ru || ''],
        }),
      }),
    
      filters: this.fb.group({
        moreFilters: this.fb.group({
          ge: [this.data.staticTexts.filters.moreFilters.ge || ''],
          en: [this.data.staticTexts.filters.moreFilters.en || ''],
          ru: [this.data.staticTexts.filters.moreFilters.ru || ''],
        }),
        priceRange: this.fb.group({
          ge: [this.data.staticTexts.filters.priceRange.ge || ''],
          en: [this.data.staticTexts.filters.priceRange.en || ''],
          ru: [this.data.staticTexts.filters.priceRange.ru || ''],
        }),
        stars: this.fb.group({
          ge: [this.data.staticTexts.filters.stars.ge || ''],
          en: [this.data.staticTexts.filters.stars.en || ''],
          ru: [this.data.staticTexts.filters.stars.ru || ''],
        }),
        roomTypes: this.fb.group({
          ge: [this.data.staticTexts.filters.roomTypes.ge || ''],
          en: [this.data.staticTexts.filters.roomTypes.en || ''],
          ru: [this.data.staticTexts.filters.roomTypes.ru || ''],
        }),
        search: this.fb.group({
          ge: [this.data.staticTexts.filters.search.ge || ''],
          en: [this.data.staticTexts.filters.search.en || ''],
          ru: [this.data.staticTexts.filters.search.ru || ''],
        }),
        clear: this.fb.group({
          ge: [this.data.staticTexts.filters.clear.ge || ''],
          en: [this.data.staticTexts.filters.clear.en || ''],
          ru: [this.data.staticTexts.filters.clear.ru || ''],
        }),
        resultsNotFound: this.fb.group({
          ge: [this.data.staticTexts.filters.resultsNotFound.ge || ''],
          en: [this.data.staticTexts.filters.resultsNotFound.en || ''],
          ru: [this.data.staticTexts.filters.resultsNotFound.ru || ''],
        }),

      }),
    
      global: this.fb.group({
        startDate: this.fb.group({
          ge: [this.data.staticTexts.global.startDate.ge || ''],
          en: [this.data.staticTexts.global.startDate.en || ''],
          ru: [this.data.staticTexts.global.startDate.ru || ''],
        }),
        endDate: this.fb.group({
          ge: [this.data.staticTexts.global.endDate.ge || ''],
          en: [this.data.staticTexts.global.endDate.en || ''],
          ru: [this.data.staticTexts.global.endDate.ru || ''],
        }),
        adults: this.fb.group({
          ge: [this.data.staticTexts.global.adults.ge || ''],
          en: [this.data.staticTexts.global.adults.en || ''],
          ru: [this.data.staticTexts.global.adults.ru || ''],
        }),
        children: this.fb.group({
          ge: [this.data.staticTexts.global.children.ge || ''],
          en: [this.data.staticTexts.global.children.en || ''],
          ru: [this.data.staticTexts.global.children.ru || ''],
        }),
        infants: this.fb.group({
          ge: [this.data.staticTexts.global.infants.ge || ''],
          en: [this.data.staticTexts.global.infants.en || ''],
          ru: [this.data.staticTexts.global.infants.ru || ''],
        }),
        search: this.fb.group({
          ge: [this.data.staticTexts.global.search.ge || ''],
          en: [this.data.staticTexts.global.search.en || ''],
          ru: [this.data.staticTexts.global.search.ru || ''],
        }),
        photos: this.fb.group({
          ge: [this.data.staticTexts.global.photos.ge || ''],
          en: [this.data.staticTexts.global.photos.en || ''],
          ru: [this.data.staticTexts.global.photos.ru || ''],
        }),
        mail: this.fb.group({
          ge: [this.data.staticTexts.global.mail.ge || ''],
          en: [this.data.staticTexts.global.mail.en || ''],
          ru: [this.data.staticTexts.global.mail.ru || ''],
        }),
        phone: this.fb.group({
          ge: [this.data.staticTexts.global.phone.ge || ''],
          en: [this.data.staticTexts.global.phone.en || ''],
          ru: [this.data.staticTexts.global.phone.ru || ''],
        }),
        address: this.fb.group({
          ge: [this.data.staticTexts.global.address.ge || ''],
          en: [this.data.staticTexts.global.address.en || ''],
          ru: [this.data.staticTexts.global.address.ru || ''],
        }),
        social: this.fb.group({
          ge: [this.data.staticTexts.global.social.ge || ''],
          en: [this.data.staticTexts.global.social.en || ''],
          ru: [this.data.staticTexts.global.social.ru || ''],
        }),
        name: this.fb.group({
          ge: [this.data.staticTexts.global.name.ge || ''],
          en: [this.data.staticTexts.global.name.en || ''],
          ru: [this.data.staticTexts.global.name.ru || ''],
        }),
        message: this.fb.group({
          ge: [this.data.staticTexts.global.message.ge || ''],
          en: [this.data.staticTexts.global.message.en || ''],
          ru: [this.data.staticTexts.global.message.ru || ''],
        }),
        send: this.fb.group({
          ge: [this.data.staticTexts.global.send.ge || ''],
          en: [this.data.staticTexts.global.send.en || ''],
          ru: [this.data.staticTexts.global.send.ru || ''],
        }),
        category: this.fb.group({
          ge: [this.data.staticTexts.global.category.ge || ''],
          en: [this.data.staticTexts.global.category.en || ''],
          ru: [this.data.staticTexts.global.category.ru || ''],
        }),
        startBooking: this.fb.group({
          ge: [this.data.staticTexts.global.startBooking.ge || ''],
          en: [this.data.staticTexts.global.startBooking.en || ''],
          ru: [this.data.staticTexts.global.startBooking.ru || ''],
        }),
        bookVisit: this.fb.group({
          ge: [this.data.staticTexts.global.bookVisit.ge || ''],
          en: [this.data.staticTexts.global.bookVisit.en || ''],
          ru: [this.data.staticTexts.global.bookVisit.ru || ''],
        }),
        callNow: this.fb.group({
          ge: [this.data.staticTexts.global.callNow.ge || ''],
          en: [this.data.staticTexts.global.callNow.en || ''],
          ru: [this.data.staticTexts.global.callNow.ru || ''],
        }),
      }),
    
      successMessages: this.fb.group({
        contactMessage: this.fb.group({
          ge: [this.data.staticTexts.successMessages.contactMessage.ge || ''],
          en: [this.data.staticTexts.successMessages.contactMessage.en || ''],
          ru: [this.data.staticTexts.successMessages.contactMessage.ru || ''],
        }),
        bookingMessage: this.fb.group({
          ge: [this.data.staticTexts.successMessages.bookingMessage.ge || ''],
          en: [this.data.staticTexts.successMessages.bookingMessage.en || ''],
          ru: [this.data.staticTexts.successMessages.bookingMessage.ru || ''],
        }),
        button: this.fb.group({
          ge: [this.data.staticTexts.successMessages.button.ge || ''],
          en: [this.data.staticTexts.successMessages.button.en || ''],
          ru: [this.data.staticTexts.successMessages.button.ru || ''],
        }),
      }),
      
    });
  }

  submitForm() {
    this.api.update({ staticTexts: this.textsForm.value } )
      .subscribe(() => {
        this.api.getOne();
      }, (data) => { console.log(data) });
    }
  
}
