import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextsRoutingModule } from './texts-routing.module';
import { TextsComponent } from './texts.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [TextsComponent],
  imports: [
    CommonModule,
    TextsRoutingModule,
    SharedModule,
  ],
  exports: [TextsComponent]
})
export class TextsModule { }
