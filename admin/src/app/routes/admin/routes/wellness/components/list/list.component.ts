import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Query } from 'app/shared/models/query';
import { Wellness } from 'app/shared/models/wellness';
import { PageEvent } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class ListComponent implements OnInit {
  @Input() items: any;
  @Input() numTotal: any;
  @Input() query: Query;

  @Output() queryChange = new EventEmitter<Query>();
  @Output() updateForm = new EventEmitter<any>();
  @Output() deleteForm = new EventEmitter<Wellness>();

  dataSource: Wellness[];
  pageEvent: PageEvent;
  pageLength: number;
  expandedElement: any;

  columnsToDisplay = ['name', 'options'];

  constructor() { }

  ngOnInit(): any {
    this.items.subscribe((data) => {
      this.dataSource = data;
    });

    this.numTotal.subscribe((data) => {
      this.pageLength = data;
    });
  }

  pagenatorEvent(pageData: any): any {
    this.queryChange.emit({
      page: pageData.pageIndex + 1,
      limit: pageData.pageSize,
    });
  }

  confirmDelete(event, element): any {
    // prevent table element header from other click actions
    event.stopPropagation();
    this.deleteForm.emit(element);
  }

  submitWellnessForm(data: any, id: any): any {
    this.updateForm.emit({ _id: id, ...data });
  }

}
