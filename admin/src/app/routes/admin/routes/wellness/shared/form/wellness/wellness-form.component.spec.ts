import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellnessFormComponent } from './wellness-form.component';

describe('WellnessFormComponent', () => {
  let component: WellnessFormComponent;
  let fixture: ComponentFixture<WellnessFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WellnessFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellnessFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
