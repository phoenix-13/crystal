import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Wellness } from 'app/shared/models/wellness';
import { FormComponent } from 'app/shared/components/form.component';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-wellness-form',
  templateUrl: './wellness-form.component.html',
  styleUrls: ['./wellness-form.component.scss']
})
export class WellnessFormComponent extends FormComponent implements OnInit {
  @Input() wellnessFormData: any;
  @Output() submitWellnessForm = new EventEmitter<Wellness>();
  @Input() canSubmit: boolean = true;

  form: FormGroup;
  submitted: boolean;

  constructor(private fb: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: this.fb.group({
        ge: [this.wellnessFormData.name.ge || ''],
        en: [this.wellnessFormData.name.en || ''],
        ru: [this.wellnessFormData.name.ru || ''],
      }),
      procedures: this.fb.array(this.wellnessFormData.procedures.map(item => this.createItem(item)))
    });
  }

  createItem(item): FormGroup {
    return this.fb.group({
      name: this.fb.group({
        ge: [item.name.ge || ''],
        en: [item.name.en || ''],
        ru: [item.name.ru || ''],
      }),
      price: this.fb.group({
        ge: [item.price.ge || ''],
        en: [item.price.en || ''],
        ru: [item.price.ru || ''],
      }),
      duration: this.fb.group({
        ge: [item.duration.ge || ''],
        en: [item.duration.en || ''],
        ru: [item.duration.ru || ''],
      }),
    });
  }

  get proceduresForms() {
    return this.form.get('procedures') as FormArray;
  }

  addForm() {
    const array = this.fb.group({
      name: this.fb.group({
        ge: [''],
        en: [''],
        ru: [''],
      }),
      price: [0],
      duration: [''],
    });

    this.proceduresForms.push(array);
  }

  deleteForm(i) {
    this.proceduresForms.removeAt(i);
  }

  submit() {
    this.submitWellnessForm.emit(this.form.value);
    this.submitted = true;
  }
}
