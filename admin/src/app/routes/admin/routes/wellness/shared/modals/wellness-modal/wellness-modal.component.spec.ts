import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WellnessModalComponent } from './wellness-modal.component';

describe('WellnessModalComponent', () => {
  let component: WellnessModalComponent;
  let fixture: ComponentFixture<WellnessModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WellnessModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WellnessModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
