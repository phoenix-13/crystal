import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormComponent } from 'app/shared/components/form.component';
import { WellnessFormComponent } from '../../form/wellness/wellness-form.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Wellness } from 'app/shared/models/wellness';
import * as _ from 'lodash';

@Component({
  selector: 'app-wellness-modal',
  templateUrl: './wellness-modal.component.html',
  styleUrls: ['./wellness-modal.component.scss']
})
export class WellnessModalComponent implements OnInit {
  formComponents: FormComponent[];

  @ViewChild('form', { static: false }) wellnessForm: WellnessFormComponent;

  constructor(
    private dialogRef: MatDialogRef<WellnessFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Wellness
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.formComponents = [
      this.wellnessForm
    ];
  }

  formsAreValid() {
    return this.formComponents.filter(component => component).every((formComponent: FormComponent) => formComponent.formIsValid());
  }

  onFinish() {
    if (this.formsAreValid()) {
      this.dialogRef.close(this.getFormData());
    } else {
      console.log('create error');
    }
  }

  getFormData(): any {
    let data = _.cloneDeep(_.merge(
      this.wellnessForm.getFormValue(),
    ));
    return data;
  }
}
