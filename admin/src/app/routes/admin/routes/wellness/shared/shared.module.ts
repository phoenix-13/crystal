import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WellnessFormComponent } from './form/wellness/wellness-form.component';
import { WellnessModalComponent } from './modals/wellness-modal/wellness-modal.component';
import { SharedModule as _SharedModule } from '../../shared/shared.module';
import { ConfirmDeleteModalComponent } from '../../shared/modals/confirm-delete-modal/confirm-delete-modal.component';

@NgModule({
  declarations: [WellnessFormComponent, WellnessModalComponent],
  imports: [
    CommonModule,
    _SharedModule,
  ],
  exports: [_SharedModule, WellnessFormComponent, WellnessModalComponent],
  entryComponents: [WellnessModalComponent, ConfirmDeleteModalComponent]
})
export class SharedModule { }
