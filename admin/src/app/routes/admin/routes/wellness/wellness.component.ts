import { Component, OnInit } from '@angular/core';
import { Query } from 'app/shared/models/query';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Wellness } from 'app/shared/models/wellness';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { WellnessApiService } from 'app/shared/http/wellness-api.service';
import { switchMap, share, map, filter } from 'rxjs/operators';
import { WellnessModalComponent } from './shared/modals/wellness-modal/wellness-modal.component';
import { ConfirmDeleteModalComponent } from '../shared/modals/confirm-delete-modal/confirm-delete-modal.component';

@Component({
  selector: 'app-wellness',
  templateUrl: './wellness.component.html',
  styleUrls: ['./wellness.component.scss']
})
export class WellnessComponent {
  query: Query;
  items$: Observable<Wellness[]>;
  numTotal$: Observable<number>;
  loadData$: Subject<Query>;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private api: WellnessApiService,
  ) {
    this.query = parseQueryParams(this.route.snapshot.queryParams);
    this.loadData$ = new BehaviorSubject(this.query);

    this.loadData$.subscribe((query: Query) => {
      this.router.navigate(['/admin/wellness'], {
        queryParams: query,
        queryParamsHandling: 'merge',
      });
    });

    const data$ = this.loadData$.pipe(
      switchMap(q => this.api.getByQuery(q)),
      share(),
    );

    this.items$ = data$.pipe(map(d => d.items));
    this.numTotal$ = data$.pipe(map(d => d.numTotal));
  }


  add() {
    const data: Wellness = {
      name: {},
      procedures: [],
    };
    this.dialog
      .open(WellnessModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(d => {
          return this.api.create(d);
        }),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  update(data: any) {
    this.api.update(data)
      .subscribe(() => {

        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }

  delete(data: Wellness) {
    this.dialog
      .open(ConfirmDeleteModalComponent, { data })
      .afterClosed()
      .pipe(
        filter(r => r),
        switchMap(() => this.api.delete(data._id)),
      )
      .subscribe(() => {
        this.loadData$.next(this.query);
      }, () => {
        this.loadData$.next(this.query);
      });
  }


  reloadParams(query: Query) {
    this.query = { ...this.query, ...query };
    this.loadData$.next(this.query);
  }

}

function parseQueryParams(params): Query {
  return {
    ...params,
    page: params.page ? Number(params.page) : 1,
    limit: params.limit ? Number(params.limit) : 10,
  };
}