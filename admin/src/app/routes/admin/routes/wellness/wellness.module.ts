import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WellnessRoutingModule } from './wellness-routing.module';
import { WellnessComponent } from './wellness.component';
import { ComponentsModule } from './components/components.module';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [WellnessComponent],
  imports: [
    CommonModule,
    WellnessRoutingModule,
    ComponentsModule,
    SharedModule,
  ],
  exports: [ComponentsModule, WellnessComponent]
})
export class WellnessModule { }
