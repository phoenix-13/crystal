import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { hasError } from '../../shared/utils/form.utils';
import { UserApiService } from '../../shared/http/user-api.service';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';
import { FuseSplashScreenService } from '../../../@fuse/services/splash-screen.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  hasError = hasError;
  submitted: boolean;
  serverError: boolean;
  loading: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userApiService: UserApiService,
    private authService: AuthService,
    private SplashScreen: FuseSplashScreenService,
  ) {}

  ngOnInit(): void {
    this.SplashScreen.hide();
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  submit() {
    this.submitted = true;
    if (!this.loginForm.valid || this.loading) return;
    this.loading = true;
    this.serverError = false;
    this.userApiService.signIn(this.loginForm.value)
      .subscribe(
        ({user, token}) => {
          this.authService.setToken(token);
          this.authService.changeUser(user);
          this.router.navigate(['/admin/info']);
        },
        (e) => {
          this.loading = false;
          this.serverError = true;
        },
        () => this.loading = false
      );  
  }
}
