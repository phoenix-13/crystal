import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { QueryResponse } from '../models/query-response';
import { ArticleCategory } from '../models/articleCategory';

const API_URL = environment.apiUrl;

@Injectable()
export class ArticleCategoryApiService {
  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<ArticleCategory>> {
    return this.http.get<any>(`${API_URL}/api/article-categories`, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${API_URL}/api/article-categories`, data, {
      responseType: 'text',
    });
  }

  update(data): Observable<any> {
    return this.http.post(`${API_URL}/api/article-categories/update`, data, {
      responseType: 'text',
    });
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${API_URL}/api/article-categories/${id}`, {
      responseType: 'text',
    });
  }

}
