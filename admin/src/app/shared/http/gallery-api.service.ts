import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { Gallery } from '../models/gallery';
import { environment } from 'environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
// {
//   providedIn: 'root'
// }
export class GalleryApiService {

  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Gallery>> {
    return this.http.get<any>(`${API_URL}/api/gallery`, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${API_URL}/api/gallery`, data, {
      responseType: 'text',
    });
  }

  update(data): Observable<any> {
    return this.http.post(`${API_URL}/api/gallery/update`, data, {
      responseType: 'text',
    });
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${API_URL}/api/gallery/${id}`, {
      responseType: 'text',
    });
  }
}
