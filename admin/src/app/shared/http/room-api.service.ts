import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Rooms } from '../models/rooms';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';


const API_URL = environment.apiUrl;

@Injectable()
export class RoomApiService {
  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Rooms>> {
    return this.http.get<any>(`${API_URL}/api/rooms`, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${API_URL}/api/rooms`, data, {
      responseType: 'text',
    });
  }

  update(data): Observable<any> {
    return this.http.post(`${API_URL}/api/rooms/update`, data, {
      responseType: 'text',
    });
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${API_URL}/api/rooms/${id}`, {
      responseType: 'text',
    });
  }
}
