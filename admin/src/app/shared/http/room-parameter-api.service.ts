import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { HttpClient } from '@angular/common/http';
import { RoomParameters } from '../models/room-parameters';

const API_URL = environment.apiUrl;

@Injectable()
export class RoomParameterApiService {

  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<RoomParameters>> {
    return this.http.get<any>(`${API_URL}/api/room-features`, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${API_URL}/api/room-features`, data, {
      responseType: 'text',
    });
  }

  update(data): Observable<any> {
    return this.http.post(`${API_URL}/api/room-features/update`, data, {
      responseType: 'text',
    });
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${API_URL}/api/room-features/${id}`, {
      responseType: 'text',
    });
  }

}
