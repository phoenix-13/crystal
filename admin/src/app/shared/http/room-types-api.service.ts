import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { HttpClient } from '@angular/common/http';
import { RoomTypes } from '../models/room-types';


const API_URL = environment.apiUrl;

@Injectable()
export class RoomTypesApiService {

  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<RoomTypes>> {
    return this.http.get<any>(`${API_URL}/api/room-types`, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${API_URL}/api/room-types`, data, {
      responseType: 'text',
    });
  }

  update(data): Observable<any> {
    return this.http.post(`${API_URL}/api/room-types/update`, data, {
      responseType: 'text',
    });
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${API_URL}/api/room-types/${id}`, {
      responseType: 'text',
    });
  }

  updatePositions(data): Observable<any> {
    return this.http.post(`${API_URL}/api/room-types/update/positions`, data, {
      responseType: 'text',
    });
  }

} 
