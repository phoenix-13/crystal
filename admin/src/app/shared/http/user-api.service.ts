import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';
import { User } from '../models/user';


const API_URL = environment.apiUrl;

@Injectable()
export class UserApiService {

  constructor(
    private http: HttpClient,
  ) {
  }

  public getMe(): Observable<User> {
    return this.http
      .get<User>(`${API_URL}/api/users/me`);
  }

  public isEmailUnique(email: string): Observable<any> {
    return this.http
      .get<any>(`${API_URL}/api/users/validate/email/${email}/unique`);
  }

  public getByQuery(params): Observable<any> {
    return this.http
      .get<any>(`${API_URL}/api/users`, { params });
  }

  public signIn(data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/sign-in`, data);
  }

  public create(data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users`, data, {
        responseType: 'text',
      });
  }

  public resendPassword(data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/password/resend`, data, {
        responseType: 'text',
      });
  }

  public activate(id, data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/${id}/activate`, data, {
        responseType: 'text',
      });
  }

  public update(id, data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/update/${id}`, data, {
        responseType: 'text',
      });
  }

  public updateName(id, data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/${id}/update/name`, data, {
        responseType: 'text',
      });
  }

  public updatePassword(id, data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/${id}/update/password`, data, {
        responseType: 'text',
      });
  }

  public updateGuarantee(id, data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/update-guarantee/${id}`, data, {
        responseType: 'text',
      });
  }

  public updateDriverLicense(data): Observable<any> {
    return this.http.post(`${API_URL}/api/users/update-driver-license`, data);
  }

  public updateSuperhostTag(id, data): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/update-superhost/${id}`, data, {
        responseType: 'text',
      });
  }

  public block(id: string): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/block/${id}`, {
        responseType: 'text',
      });
  }

  public unblock(id: string): Observable<any> {
    return this.http
      .post(`${API_URL}/api/users/unblock/${id}`, {
        responseType: 'text',
      });
  }

  public delete(id: string): Observable<any> {
    return this.http
      .delete(`${API_URL}/api/users/${id}`, {
        responseType: 'text',
      });
  }

}
