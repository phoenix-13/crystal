import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { Wellness } from '../models/wellness';


const API_URL = environment.apiUrl;

@Injectable()
export class WellnessApiService {
  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Wellness>> {
    return this.http.get<any>(`${API_URL}/api/wellness`, {
      params,
    });
  }

  create(data): Observable<any> {
    return this.http.post(`${API_URL}/api/wellness`, data, {
      responseType: 'text',
    });
  }

  update(data): Observable<any> {
    return this.http.post(`${API_URL}/api/wellness/update`, data, {
      responseType: 'text',
    });
  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${API_URL}/api/wellness/${id}`, {
      responseType: 'text',
    });
  }
}
