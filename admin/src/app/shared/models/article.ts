export interface Article {
  _id?: any;
  title?: any;
  description?: any;
  thumbnail?: any;
  content?: any;
  createdAt?: any;
  isFeatured?: boolean;
  articleCategory?: any;
  meta?: any;
}
