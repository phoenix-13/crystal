export interface Common {
  _id?: any;
  phone?: any;
  email?: any;
  address?: any;
  videoID?: any;
  title?: any;
  buttonText?: any;
  buttonLink?: any;
  content?: any;
  banners?: any;
} 