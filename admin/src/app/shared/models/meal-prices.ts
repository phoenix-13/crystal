export interface MealPrices {
  _id?: any;
  name?: any;
  startDate?: any;
  endDate?: any;
  singleMealPrice?: any;
  doubleMealPrice?: any;
  tripleMealPrice?: any;
}