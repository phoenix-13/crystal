import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'home',
        loadChildren: './routes/home/home.module#HomeModule',
      },
      {
        path: 'about',
        loadChildren: './routes/about/about.module#AboutModule',
      },
      {
        path: 'spa',
        loadChildren: './routes/spa/spa.module#SpaModule',
      },
      {
        path: 'gallery',
        loadChildren: './routes/gallery/gallery.module#GalleryModule',
      },
      {
        path: 'gallery/:galleryId',
        loadChildren: './routes/gallery/gallery.module#GalleryModule',
      },
      {
        path: 'rooms',
        loadChildren: './routes/rooms/rooms.module#RoomsModule',
      },
      {
        path: 'room',
        loadChildren: './routes/room/room.module#RoomModule',
      },
      {
        path: 'room/:roomId',
        loadChildren: './routes/room/room.module#RoomModule',
      },
      {
        path: 'news',
        loadChildren: './routes/news/news.module#NewsModule',
      },
      {
        path: 'article',
        loadChildren: './routes/article/article.module#ArticleModule',
      },
      {
        path: 'article/:articleId',
        loadChildren: './routes/article/article.module#ArticleModule',
      },
      {
        path: 'contact',
        loadChildren: './routes/contact/contact.module#ContactModule',
      },
      {
        path: '**',
        redirectTo: 'home'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
