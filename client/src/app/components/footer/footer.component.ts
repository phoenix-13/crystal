import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CommonApiService } from '../../shared/http/common-api.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  commonData: any;
  logo: any;

  constructor(
    private translate: TranslateService,
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {

    let currentLang = this.translate.currentLang;
    this.commonApiService.getOne().subscribe((data) => {
      this.commonData = data;

      this.translate.onLangChange.subscribe((data: any) => {
        currentLang = data.lang;
        this.logo = this.commonData.promo.footerLogo[currentLang];
      });
      this.logo = this.commonData.promo.footerLogo[currentLang];

    });
  }

}
