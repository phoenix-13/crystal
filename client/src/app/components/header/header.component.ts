import { Component, OnInit, OnChanges } from '@angular/core';
import { LangService } from '../../shared/services/lang.service';
import { Router } from '@angular/router';
import { trigger, transition, useAnimation, style } from '@angular/animations';
import { slideInUp, fadeInDown } from 'ng-animate';
import { CommonApiService } from 'src/app/shared/http/common-api.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('fadeInDown', [transition('* => *', [style({
      'opacity': 0
    }), useAnimation(fadeInDown, {
      params: { timing: 0.25 }
    })])
    ],
    )]
})
export class HeaderComponent implements OnInit {
  commonData: any;
  logo: any;

  langs: any = {
    'ge': 'GEO',
    'en': 'ENG',
    'ru': 'RUS',
  };

  isDisabled = false;
  isActive: any;
  fadeInDown: any;

  constructor(
    public langService: LangService,
    private router: Router, 
    private commonApiService: CommonApiService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    let currentLang = this.translate.currentLang;
    this.commonApiService.getOne().subscribe((data) => {
      this.commonData = data;

      this.translate.onLangChange.subscribe((data: any) => {
        currentLang = data.lang;
        this.logo = this.commonData.promo.headerLogo[currentLang];
      });
      this.logo = this.commonData.promo.headerLogo[currentLang];

    });

    if (window.innerWidth < 991) {
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
  }

  isHome() {
    return this.router.url.startsWith('/home');
  }
}
