import { Component, OnInit } from '@angular/core';
import { CommonApiService } from 'src/app/shared/http/common-api.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  common: any;
  constructor(private commonApiService: CommonApiService) { }

  ngOnInit() {
    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    });
  }

}
