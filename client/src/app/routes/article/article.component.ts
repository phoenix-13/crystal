import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleApiService } from '../../shared/http/article-api.service';
import * as moment from 'moment';
import { CommonApiService } from '../../shared/http/common-api.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent implements OnInit {

  newsData: any;
  createdDate: any;
  common: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ArticleApiService: ArticleApiService,
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {

    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    }) 

    let { articleId } = this.activatedRoute.snapshot.params;
    this.ArticleApiService.getById(articleId).subscribe((article) => {
      this.newsData = article;
    });

    setTimeout(() => {
      this.createdDate = moment(this.newsData.createdAt).format("DD MMMM YYYY");
    }, 500);
  }
}


