import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonApiService } from 'src/app/shared/http/common-api.service';
import { LoadingService } from 'src/app/shared/services/loading.service';
import { tap } from 'rxjs/operators';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { SuccessMessageComponent } from 'src/app/shared/components/modals/success-message/success-message.component';
import { MapStyle } from 'src/app/shared/constants/map-styles';

declare const google;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @ViewChild('mapElement', { static: true }) mapElement;

  map: any;
  common: any;
  form: FormGroup;
  submitted = false;
  location: any;

  constructor(
    private commonApiService: CommonApiService,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
      this.createMap(data.contact.location, data.contact.mapZoom);
    });

    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
    });
  }

  createMap(location, mapZoom) {
    let center = new google.maps.LatLng(location.lat, location.lng);
    let mapOptions = {
      zoom: mapZoom || 16,
      zoomControl: true,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: true,
      styles: MapStyle,
      center: center,
      draggable: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_TOP
      },
      mapTypeId: 'satellite'
    };
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    if (window.innerWidth > 768) {
      new google.maps.Marker({
        position: center,
        icon: '/assets/images/pin_logo.png',
        map: this.map,
      });
    } else {
      new google.maps.Marker({
        position: center,
        icon: '/assets/images/pin_logo_small.png',
        map: this.map,
      });
    }

  }

  submit() {
    this.submitted = true;
    if (this.form.valid) {
      this.loadingService.start();
      this.commonApiService.sendEmail(this.form.value)
        .pipe(tap(this.loadingService.getStopHandler()))
        .subscribe(() => {
          this.submitted = false;
          this.form.reset();
        });

      this.dialog.open(SuccessMessageComponent, {data: {name: 'contact'}});
    }
  }

}
