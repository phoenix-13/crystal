import { Component, OnInit } from '@angular/core';
import { GalleryApiService } from 'src/app/shared/http/gallery-api.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopupComponent } from './popup/popup.component';
import { CommonApiService } from '../../shared/http/common-api.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']

})
export class GalleryComponent implements OnInit {

  galleryData: any = { items: [], numtotal: 0 };

  selectedAlbum: any;
  subscription: Subscription;
  galleryId: string;
  popupOpen: boolean;
  opendImage: number;

  constructor(
    private galleryApiService: GalleryApiService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.galleryApiService.getByQuery({ all: true }).subscribe((data) => {
      this.galleryData = data;
      this.getAlbumById();
      this.listenRouteChange();
    });
  }

  getAlbumById() {
    this.galleryId = this.activatedRoute.snapshot.params.galleryId;
    if (!this.galleryId && this.galleryData.items.length > 0) {
      this.router.navigate([`/gallery/${this.galleryData.items[0]._id}`]);
    } else if (this.galleryId) {
      this.selectedAlbum = this.galleryId ? this.galleryData.items.find((item) => item._id === this.galleryId) : this.galleryData.items[0];
    }
  }


  listenRouteChange() {
    this.subscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.getAlbumById();
      }
    });
  }

  openImage(index: number) {
    this.popupOpen = true;
    let i = index + 1;
    this.opendImage = i;
  }

  openDialog(e: any, index: number): void {
    this.popupOpen = true;
    let i = index + 1;
    this.opendImage = i;
    let album = this.selectedAlbum;

    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      album: album,
      index: index
    };

    dialogConfig.panelClass = 'gallery_dialog';
    dialogConfig.backdropClass = 'gallery_dialog_bg';

    this.dialog.open(PopupComponent, dialogConfig);

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
