import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PopupComponent } from './popup/popup.component';
import { MatDialogModule } from '@angular/material';

@NgModule({
  declarations: [GalleryComponent, PopupComponent],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    SharedModule,
    MatDialogModule,
  ],
  exports: [SharedModule, MatDialogModule, GalleryComponent, PopupComponent],
  entryComponents: [PopupComponent]
})
export class GalleryModule { }
