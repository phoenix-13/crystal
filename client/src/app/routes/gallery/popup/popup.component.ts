import { Component, OnInit, Input, Inject, InjectionToken, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
})
@Injectable()
export class PopupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  carouselOptions: any = {
    items: 1,
    nav: true,
    navigation: true,
    dots: false,
    loop: true,
    margin: 0,
    startPosition: this.data.index,
    autoWidth: true,
    center: true,
    autoHeight: true,
    navContainer: '.popup_slider_nav',
    responsive: {},
  };

  ngOnInit() {

  }

  closeDialog() {
    this.dialogRef.close();
  }
}
