import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './gallery/gallery.component';
import { RoomsComponent } from './rooms/rooms.component';
import { PromoComponent } from './promo/promo.component';
import { SearchComponent } from './search/search.component';
import { ContactComponent } from './contact/contact.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    PromoComponent,
    GalleryComponent,
    RoomsComponent,
    SearchComponent,
    ContactComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    PromoComponent,
    GalleryComponent,
    RoomsComponent,
    SearchComponent,
    ContactComponent,
  ]
})
export class ComponentsModule { }
