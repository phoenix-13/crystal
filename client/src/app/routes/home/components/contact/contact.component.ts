import { Component, OnInit, Input } from '@angular/core';
import { CommonApiService } from 'src/app/shared/http/common-api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoadingService } from '../../../../shared/services/loading.service';
import { tap } from 'rxjs/operators';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { SuccessMessageComponent } from 'src/app/shared/components/modals/success-message/success-message.component';


@Component({
  selector: 'app-home-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})

export class ContactComponent implements OnInit {

  @Input() common: any;

  contactData: any;
  form: FormGroup;
  submitted = false;
  commonData: any;

  constructor(
    private commonApiService: CommonApiService,
    private fb: FormBuilder,
    private loadingService: LoadingService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.commonData = this.common.homeSections;
    this.contactData = this.common;

    this.form = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required],
    });
  }

  submit() {
    this.submitted = true;
    if (this.form.valid) {
      this.loadingService.start();
      this.commonApiService.sendEmail(this.form.value)
        .pipe(tap(this.loadingService.getStopHandler()))
        .subscribe(() => {
          this.submitted = false;
          this.form.reset();
        });

      const dialogConfig = new MatDialogConfig();

      dialogConfig.data = {
        name: 'contact'
      };

      this.dialog.open(SuccessMessageComponent, dialogConfig);
    }
  }

}

