import { Component, OnInit, Input } from '@angular/core';
import { GalleryApiService } from 'src/app/shared/http/gallery-api.service';
import { CommonApiService } from 'src/app/shared/http/common-api.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() common: any;

  galleryData: any = { items: [], numtotal: 0 };
  commonData: any;

  constructor(
    private galleryApiService: GalleryApiService,
  ) { }

  ngOnInit() {
    this.galleryApiService.getByQuery({ limit: 5 }).subscribe((data) => {
      this.galleryData = data;
    });

    this.commonData = this.common.homeSections;
  }


}
