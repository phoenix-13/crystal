import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.scss']
})
export class PromoComponent implements OnInit {

  @Input() promo: any;

  image: any;
  currentLang: any;

  constructor(
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.currentLang = this.translate.currentLang;
		this.translate.onLangChange.subscribe((data: any) => {
      this.currentLang = data.lang;
      this.image = this.promo.promoImage[this.currentLang];
		});
    this.image = this.promo.promoImage[this.currentLang];
  }

}
