import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { RoomApiService } from 'src/app/shared/http/room-api.service';
import { CommonApiService } from 'src/app/shared/http/common-api.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  @Input() common: any;

  roomsData: any = { items: [], numTotal: 0 };
  commonData: any;

  constructor(
    private RoomsApiService: RoomApiService,
  ) { }

  ngOnInit() {
    this.RoomsApiService.getByQuery({ all: true }).subscribe((data) => {
      this.roomsData = data;
    });

    this.commonData = this.common.homeSections;
  }


}
