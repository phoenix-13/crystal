import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { fadeInLeft, fadeInDown } from 'ng-animate';
import * as moment from 'moment';
import { trigger, transition, useAnimation, style } from '@angular/animations';
import { Router } from '@angular/router';
import { CommonApiService } from '../../../../shared/http/common-api.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD MMMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
  animations: [
    trigger('fadeInLeft', [
      transition('* => *', [
        style({ 'opacity': 0}), 
        useAnimation(fadeInLeft, { params: { timing: 0.4, delay: 0.3, a: '-70px', }})
      ])
    ]),
    trigger('fadeInDown', [
      transition('* => *', [
        style({'opacity': 0 }), 
        useAnimation(fadeInDown, { params: { timing: 0.25 }})
      ])
    ]),
  ],
})
export class SearchComponent implements OnInit {
  isVisible = false;
  isDisabled = false;
  inView = false;

  form: FormGroup;
  minStartDate = moment();
  minEndDate = moment().add(2, 'd');

  dateError = false;
  newEndDate: any;
  newStartDate: any;
  fadeInDown: any;
  common: any;
  
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private commonApiService: CommonApiService,
  ) { }


  ngOnInit() {

    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    });

    this.form = this.fb.group({
      startDate: [moment().add(1, 'd') || '', Validators.required],
      endDate: [moment().add(2, 'd') || '', Validators.required],
      adults: [2, Validators.required],
      children: [0, Validators.required],
      infants: [0, Validators.required],
    }, { validator: this.dateDiffValidator.bind(this) });

    this.form.get('startDate').valueChanges.subscribe((data) => {
      this.newStartDate = data;
      this.minEndDate = moment(data).add(1, 'd');
      if (this.newStartDate.format("MM-DD-YYYY") >= this.form.get('endDate').value.format("MM-DD-YYYY")) {
        this.form.patchValue({
          endDate: moment(this.newStartDate).add(1, 'd')
        });
      }
    });
    this.form.get('endDate').valueChanges.subscribe((data) => {
      this.newEndDate = data;
      if (this.newStartDate && this.newStartDate.format("MM-DD-YYYY") >= this.newEndDate.format("MM-DD-YYYY")) {
        this.form.patchValue({
          endDate: moment(this.newStartDate).add(1, 'd')
        });
      }
    });

    if (window.innerWidth > 991) {
      setTimeout(() => {
        this.inView = true;
      }, 100);
      this.isDisabled = false;
    } else {
      this.inView = true;
      this.isDisabled = true;
    }

  }

  submit() {
    if (this.form.valid) {
      this.router.navigate(['rooms'], { queryParams: this.form.value })
    }
  }

  dateDiffValidator(form: FormGroup): { [key: string]: boolean } {
    let startDateVal = moment(form.get('startDate').value).format("MM-DD-YYYY");
    let endDateVal = moment(form.get('endDate').value).format("MM-DD-YYYY");

    if (moment(endDateVal).isAfter(startDateVal)) {
      this.dateError = false;
      return null;
    } else {
      this.dateError = true;
      return { 'dateDiffValidator': true };
    }
  }
}
