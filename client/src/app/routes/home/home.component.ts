import { Component, OnInit } from '@angular/core';
import { CommonApiService } from '../../shared/http/common-api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  common: any;

  constructor(
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {

    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    });

  }

}
