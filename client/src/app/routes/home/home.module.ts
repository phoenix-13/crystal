import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { ComponentsModule } from './components/components.module';
import { SuccessMessageComponent } from 'src/app/shared/components/modals/success-message/success-message.component';


@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ComponentsModule
  ],
  exports: [HomeComponent],
  entryComponents: [SuccessMessageComponent]
})
export class HomeModule { }
