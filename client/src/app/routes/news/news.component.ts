import { Component, OnInit } from '@angular/core';
import { ArticleApiService } from 'src/app/shared/http/article-api.service';
import { CommonApiService } from '../../shared/http/common-api.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  articlesData: any = { items: [], numTotal: 0 };

  constructor(
    private ArticleApiService: ArticleApiService,
  ) { }

  ngOnInit() {
    this.ArticleApiService.getByQuery({ all: true }).subscribe((data) => {
      this.articlesData = data;
    });
  }
  
}
