import { Component, OnInit } from '@angular/core';
import { RoomApiService } from 'src/app/shared/http/room-api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as Moment from 'moment';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS, MatDialog } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ActivatedRoute, Params } from '@angular/router';
import { SuccessMessageComponent } from 'src/app/shared/components/modals/success-message/success-message.component';
import { CommonApiService } from '../../shared/http/common-api.service';
import { LoadingService } from '../../shared/services/loading.service';
import { tap } from 'rxjs/operators';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD MMMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class RoomComponent implements OnInit {
  totalPrice: number;
  adultsValue: number;
  childValue: number;
  defaultPrice: number;
  priceForExtraPerson: number;
  personNumberError: boolean;
  numberOfAdditionalGuests: number;
  numberOfGuests: number;
  minStartDate = moment().add(1, 'd');
  minEndDate: any;
  difference: any;
  totalPeopleDefault: number;
  totalAllowedPeople: number;
  priceRangeDates: any[] = [];
  mealPrice: number;
  roomsData: any = { items: [], numTotal: 0 };
  activeFeature: boolean;
  room: any;
  bookingFeatures: any;
  form: FormGroup;
  dateError: boolean;
  newEndDate: any;
  newStartDate: any;
  startDate: any = {};
  endDate: any;
  adults: string;
  children: string;
  infants: string;
  queryParams: Params;
  common: any = {};


  stars: number[];


  constructor(
    private RoomsApiService: RoomApiService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private commonApiService: CommonApiService,
    private loadingService: LoadingService,
  ) { }

  ngOnInit() {
    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
      this.mealPrice = this.common.defaultMealPriceRanges.singleMealPrice;
      this.form.get('features').setValue(this.common.staticTexts.room.breakfast);
    });

    this.activatedRoute.queryParams.subscribe(params => {
      this.startDate = params['startDate'] || moment().add(1, 'd');
      this.endDate = params['endDate'] || moment().add(2, 'd');
      this.adults = params['adults'] || 2;
      this.children = params['children'] || 0;
      this.infants = params['infants'] || 0;
      this.queryParams = params;

      this.adultsValue = Number(this.adults);
      this.childValue = Number(this.children);

    });

    let { roomId } = this.activatedRoute.snapshot.params;
    this.RoomsApiService.getById(roomId).subscribe((room) => {
      this.room = room;
      this.defaultPrice = this.room.defaultPrice;
      this.priceForExtraPerson = this.room.priceForAdditionalPerson;
      this.numberOfAdditionalGuests = this.room.numberOfAdditionalGuests;
      this.numberOfGuests = this.room.numberOfGuests;
      this.priceRangeDates = this.room.priceRangeDates;

      this.stars = Array(this.room.stars).fill(1);

      this.calculatePrice();
    });

    this.bookingFeatures = [];
    this.form = this.fb.group({
      startDate: [this.startDate ? moment(this.startDate) : moment().add(1, 'd'), Validators.required],
      endDate: [this.endDate ? moment(this.endDate) : moment().add(2, 'd'), Validators.required],
      details: [''],
      features: [''],
      adults: [this.adults || 2, Validators.required],
      children: [this.children || 0, Validators.required],
      infants: [this.infants || 0, Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
    }, { validator: this.dateDiffValidator.bind(this) });


    this.minEndDate = moment(this.startDate).add(1, 'd');

    this.form.get('startDate').valueChanges.subscribe((data) => {
      this.newStartDate = data;
      this.startDate = data;
      if (this.newStartDate >= this.form.get('endDate').value) {
        this.form.patchValue({
          endDate: moment(this.newStartDate).add(1, 'd')
        });
      }
      this.minEndDate = data;
      this.calculatePrice();
    });

    this.form.get('endDate').valueChanges.subscribe((data) => {
      this.newEndDate = data;
      this.endDate = data;
      if (this.newStartDate && this.newStartDate >= this.newEndDate) {
        this.form.patchValue({
          endDate: moment(this.newStartDate).add(1, 'd')
        });
      }
      this.calculatePrice();
    });

    this.form.get('adults').valueChanges.subscribe(data => {
      this.adultsValue = data;
      this.calculatePrice();
    });

    this.form.get('children').valueChanges.subscribe(data => {
      this.childValue = data;
      this.calculatePrice();
    });

  }

  calculatePrice() {
    // format to calculate only days, not hours.
    var start = this.startDate.format('MM DD YYYY');
    var end = this.endDate.format('MM DD YYYY');

    this.mealPrice = this.mealPrice || 0;
    this.difference = moment(end).diff(moment(start), 'days'); // number of total selected days

    this.totalPeopleDefault = this.adultsValue + this.childValue;
    this.totalAllowedPeople = this.numberOfGuests + this.numberOfAdditionalGuests;

    // check if total number of people is not greater than allowed in this room
    if (this.totalPeopleDefault > this.totalAllowedPeople) {
      this.personNumberError = true;
    } else {
      this.personNumberError = false;
      this.totalPrice = this.calculateRoomPrice(this.startDate, this.endDate) + (this.mealPrice * this.difference);
    }
  }

  calculateRoomPrice(startDate: any, endDate: any): number {
    let defaultSpecialPrice = 0;
    let defaultSpecialAdditionalPrice = 0;
    let defaultNormalPrice = 0;
    let defaultAdditionalPrice = 0;
    let roomPrice;
    let specialDay = 0;
    let normalDay = 0;

    // Calculatin Price depending on special price ranges
    for (let i = 0; i < this.difference; i++) {
      let day = moment(startDate).add(i, 'd');

      for (let j = 0; j < this.priceRangeDates.length; j++) {
        let rangeStartDate = moment(this.priceRangeDates[j].startDate);
        let rangeEndDate = moment(this.priceRangeDates[j].endDate);
        let rangeDefPrice = this.priceRangeDates[j].defaultPrice;
        let rangeAdditionalPrice = this.priceRangeDates[j].priceForAdditionalPerson;
        let specialDays = moment.range(rangeStartDate, rangeEndDate);

        if (moment.range(startDate, endDate).overlaps(specialDays)) {
          if (specialDays.contains(day)) {
            specialDay++;
            defaultSpecialPrice += rangeDefPrice;
            defaultSpecialAdditionalPrice += rangeAdditionalPrice;
          }
        }
      }
    }

    // calculate number of normal days
    normalDay = this.difference - specialDay;

    // calculate prices in normal days
    defaultNormalPrice = this.defaultPrice * normalDay;
    defaultAdditionalPrice = this.priceForExtraPerson * normalDay;

    // check if number of people is less or equal than default number of guests and calculate prices
    if (this.totalPeopleDefault <= this.numberOfGuests) {
      roomPrice = defaultSpecialPrice + defaultNormalPrice;
    }

    // check if number of people is  equal to maximum number of guests and calculate prices
    if (this.totalPeopleDefault === (this.numberOfGuests + this.numberOfAdditionalGuests)) {
      roomPrice = defaultSpecialPrice + defaultAdditionalPrice + defaultNormalPrice + defaultSpecialAdditionalPrice;
    }

    return roomPrice;
  }

  submit() {
    if (this.form.valid) {
      this.loadingService.start();
      this.commonApiService.sendInquiry(this.form.value)
        .pipe(tap(this.loadingService.getStopHandler()))
        .subscribe(() => {
          // this.form.reset();
        });

      this.dialog.open(SuccessMessageComponent, { data: { name: 'booking' } });
    }
  }

  dateDiffValidator(form: FormGroup): { [key: string]: boolean } {
    let startDateVal = moment(form.get('startDate').value).format("MM-DD-YYYY");
    let endDateVal = moment(form.get('endDate').value).format("MM-DD-YYYY");

    if (moment(endDateVal).isAfter(startDateVal)) {
      this.dateError = false;
      return null;
    } else {
      this.dateError = true;
      return { 'dateDiffValidator': true };
    }
  }


  onFeatureSelect(element: any, price: number) {
    let item = document.getElementsByClassName('form_feature_item');

    for (let i = 0; i < item.length; i++) {
      // Remove the class 'active' if it exists
      item[i].classList.remove('checked');
    }
    element.target.classList.add('checked');

    this.form.get('features').setValue(element.target.innerText);

    this.mealPrice = price;
    this.calculatePrice();
  }
}


