import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoomRoutingModule } from './room-routing.module';
import { RoomComponent } from './room.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [RoomComponent],
  imports: [
    CommonModule,
    RoomRoutingModule,
    SharedModule
  ],
  exports: [RoomComponent]
})
export class RoomModule { }
