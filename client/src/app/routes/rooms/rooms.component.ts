import { Component, OnInit } from '@angular/core';
import { RoomApiService } from 'src/app/shared/http/room-api.service';
import { ActivatedRoute, Params, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { CommonApiService } from '../../shared/http/common-api.service';


@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  roomsData: any = [];

  queryParams: Params;
  eventt: any;
  subscription: Subscription;
  common: any;

  constructor(
    private RoomsApiService: RoomApiService,
    private route: ActivatedRoute,
    private router: Router,
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.queryParams = params;
    });

    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    });

    this.searchRoomData();
    this.listenRouteChange();
  }

  searchRoomData() {
    this.RoomsApiService.searchRooms(this.queryParams).subscribe((data) => {
      this.roomsData = data;

      this.roomsData.map((item) => {
        item.starsArray = Array(item.stars).fill(1);
        return item;
      });

      console.log('this.roomsData', this.roomsData);
    });
  }

  listenRouteChange() {
    this.subscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.searchRoomData();
      }
    });
  }

}
