import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';
import { WellnessApiService } from 'src/app/shared/http/wellness-api.service';
import { CommonApiService } from 'src/app/shared/http/common-api.service';

@Component({
  selector: 'app-spa',
  templateUrl: './spa.component.html',
  styleUrls: ['./spa.component.scss']
})
export class SpaComponent implements OnInit {
  pageName = 'Wellness';
  imageName = 'spa_bg.png';

  isPaused = true;
  wellnessData: any;
  data: any[];
  commonData: any;

  @ViewChild('video', { static: false }) video: ElementRef;

  constructor(
    private wellnessApiService: WellnessApiService,
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {
    setTimeout(() => {
      $('.menu_item').click(function () {
        if ($(this).children('.sub_menu').hasClass('open')) {
          $('.sub_menu').removeClass('open');
          $('.item_name').removeClass('open');
        } else {
          $('.sub_menu').removeClass('open');
          $(this).children('.sub_menu').addClass('open');

          $('.item_name').removeClass('open');
          $(this).children('.item_name').addClass('open');
        }
      });
    }, 500);

    this.wellnessApiService.getByQuery({ all: true }).subscribe((data) => {
      this.wellnessData = data;
      this.data = this.wellnessData.items;
    });

    this.commonApiService.getOne().subscribe((data) => {
      this.commonData = data;
    });
  }

  pauseVideo() {
    this.video.nativeElement.pause();
    if (this.isPaused !== true) {
      this.isPaused = true;
    }
  }

  playVideo() {
    if (this.video.nativeElement.paused) {
      this.video.nativeElement.play();
      this.isPaused = false;
    } else {
      this.video.nativeElement.pause();
      this.isPaused = true;
    }
  }
}
