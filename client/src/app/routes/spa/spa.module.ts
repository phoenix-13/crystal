import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpaRoutingModule } from './spa-routing.module';
import { SpaComponent } from './spa.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [SpaComponent],
  imports: [
    CommonModule,
    SpaRoutingModule,
    SharedModule
  ],
  exports: [SpaComponent]
})
export class SpaModule { }
