import { Component, OnInit, Input } from '@angular/core';
import { CommonApiService } from '../../http/common-api.service';

@Component({
  selector: 'app-book-visit',
  templateUrl: './book-visit.component.html',
  styleUrls: ['./book-visit.component.scss']
})
export class BookVisitComponent implements OnInit {
  @Input() phoneNumber: any;
  @Input() contactText: any;

  common: any;

  constructor(
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {
    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    });
  }
}
