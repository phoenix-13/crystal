import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Options } from 'ng5-slider';
import { RoomApiService } from '../../http/room-api.service';
import { StarRatingComponent } from 'ng-starrating';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RoomTypesApiService } from '../../http/room-types-api.service';
import * as _ from "lodash";
import { CommonApiService } from '../../http/common-api.service';


@Component({
  selector: 'app-filters-global',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  roomTypes: any = { items: [], numTotal: 0 };
  form: FormGroup;
  openFilter: any;
  lowValue: any;
  minPriceValue: number;
  maxPriceValue: number;
  priceSliderOptions: Options = {
    floor: 0,
    ceil: 1000,
    step: 5,
    hideLimitLabels: true,
    hidePointerLabels: false,
  };
  queryParams: Params;

  common: any;


  constructor(
    private fb: FormBuilder,
    private RoomTypesApiService: RoomTypesApiService,
    private router: Router,
    private route: ActivatedRoute,
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {

    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;

      this.priceSliderOptions.floor = this.common.filterLimits.from;
      this.priceSliderOptions.ceil = this.common.filterLimits.to;


      this.route.queryParams.subscribe(params => {
        this.queryParams = params;
        this.minPriceValue = params['priceFrom'] || this.common.filterLimits.from;
        this.maxPriceValue = params['priceTo'] || this.common.filterLimits.to;

        this.form = this.fb.group({
          filter: this.fb.group({
            priceFrom: [this.minPriceValue],
            priceTo: [this.maxPriceValue],
            starValue: [this.queryParams.starValue],
            type: [[]],
          })
        });
        
      });

    });

    this.RoomTypesApiService.getRoomTypes({ all: true }).subscribe((data) => {
      this.roomTypes = data;
    });

    
  }

  priceRangeChange({ value, highValue }) {
    this.form.get('filter').get('priceFrom').setValue(value, { emitEvent: false });
    this.form.get('filter').get('priceTo').setValue(highValue, { emitEvent: false });
  }

  priceRangeChanged({ value, highValue }) {
    this.form.get('filter').get('priceFrom').setValue(value, { emitEvent: false });
    this.form.get('filter').get('priceTo').setValue(highValue);
  }

  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    // if ($event.newValue < 3) {
    //   this.form.get('filter').get('starValue').setValue(3);
    // } else if ($event.newValue > 4) {
    //   this.form.get('filter').get('starValue').setValue(4);
    // } else {
    //   this.form.get('filter').get('starValue').setValue($event.newValue);
    // }
    this.form.get('filter').get('starValue').setValue($event.newValue);
  }

  onTypeSelect(type: any) {
    let index = this.form.get('filter').get('type').value.indexOf(type._id);
    let typeId = _.find(this.queryParams.type, function(val) { return val === type._id; });

    if (index > -1 || typeId) {
      _.remove(this.queryParams.type, function(value) { return value === type._id;});
      this.form.get('filter').get('type').value.splice(index, 1);
      type.isActive = false;
    } else {
      this.form.get('filter').get('type').value.push(type._id);
      type.isActive = true;
    }
  }

  submit() {
    if (this.form.get('filter').get('starValue').value <= 3) {
      this.form.get('filter').get('starValue').setValue(3);
    } else if (this.form.get('filter').get('starValue').value >= 4) {
      this.form.get('filter').get('starValue').setValue(4);
    }
    let finalTypes = this.queryParams.type ? _.merge(this.queryParams.type, this.form.value.filter.type) : this.form.value.filter.type;
    this.form.value.filter.type = finalTypes;

    this.queryParams = { ...this.queryParams, ...this.form.value.filter };
    this.router.navigate(['rooms'], { queryParams: this.queryParams });
  }

  resetForm() {
    this.form.get('filter').reset();
    this.router.navigate(['rooms'], { queryParams: {} });
  }

}
