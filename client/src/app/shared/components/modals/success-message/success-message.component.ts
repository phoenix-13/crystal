import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CommonApiService } from '../../../http/common-api.service';

@Component({
  selector: 'app-success-message',
  templateUrl: './success-message.component.html',
  styleUrls: ['./success-message.component.scss']
})
export class SuccessMessageComponent implements OnInit {
  contactForm: boolean;
  common: any;

  constructor(
    private dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonApiService: CommonApiService,
  ) { }

  ngOnInit() {

    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
    });

    if (this.data.name === 'contact') {
      this.contactForm = true;
    } else {
      this.contactForm = false;
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
