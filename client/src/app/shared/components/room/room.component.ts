import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Rooms } from '../../models/rooms';

@Component({
  selector: 'app-room-item',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  @Input() roomData: Rooms;

  fourStar: boolean = false;

  stars: number[];

  constructor() { }

  ngOnInit() {

    this.stars = Array(this.roomData.stars).fill(1);
    console.log(this.stars);

  }

}
