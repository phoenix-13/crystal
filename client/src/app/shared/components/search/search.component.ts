import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CommonApiService } from '../../http/common-api.service';

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD MMMM',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-search-global',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class SearchComponent implements OnInit {

  // @Input() title: string;
  // @Input() image: string;



  @Input() page: string;

  form: FormGroup;
  minStartDate = moment();
  minEndDate = moment().add(1, 'd');
  openSearch: any;
  dateError = false;
  newEndDate: any;
  newStartDate: any;
  common: any;

  startDate: string;
  endDate: string;
  adults: string;
  children: string;
  infants: string;

  title: string;
  image: string;

  queryParams: Params;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private commonApiService: CommonApiService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.commonApiService.getOne().subscribe((data) => {
      this.common = data;
      this.title = this.common.banners[this.page].title;
      this.image = this.common.banners[this.page].image;
    });

    this.route.queryParams.subscribe(params => {
      this.startDate = params['startDate'];
      this.endDate = params['endDate'];
      this.adults = params['adults'];
      this.children = params['children'];
      this.infants = params['infants'];
      this.queryParams = params;
    });

    this.form = this.fb.group({
      startDate: [this.startDate ? moment(this.startDate) : moment().add(1, 'd'), Validators.required],
      endDate: [this.endDate ? moment(this.endDate) : moment().add(2, 'd'), Validators.required],
      adults: [this.adults || 3, Validators.required],
      children: [this.children || 0, Validators.required],
      infants: [this.infants || 0, Validators.required],
    }, { validator: this.dateDiffValidator.bind(this) });


    this.form.get('startDate').valueChanges.subscribe((data) => {
      this.newStartDate = data;
      this.minEndDate = moment(data).add(1, 'd');
      if (this.newStartDate.format("MM-DD-YYYY") >= this.form.get('endDate').value.format("MM-DD-YYYY")) {
        this.form.patchValue({
          endDate: moment(this.newStartDate).add(1, 'd')
        });
      }
    });
    this.form.get('endDate').valueChanges.subscribe((data) => {
      this.newEndDate = data;
      if (this.newStartDate && this.newStartDate.format("MM-DD-YYYY") >= this.newEndDate.format("MM-DD-YYYY")) {
        this.form.patchValue({
          endDate: moment(this.newStartDate).add(1, 'd')
        });
      }
    });
  }

  submit() {
    if (this.form.valid) {
      this.queryParams = { ...this.queryParams, ...this.form.value }
      console.log(this.queryParams);
      this.router.navigate(['rooms'], { queryParams: this.queryParams });
      this.openSearch = false;
    }
  }

  dateDiffValidator(form: FormGroup): { [key: string]: boolean } {
    let startDateVal = moment(form.get('startDate').value).format("MM-DD-YYYY");
    let endDateVal = moment(form.get('endDate').value).format("MM-DD-YYYY");

    if (moment(endDateVal).isAfter(startDateVal)) {
      this.dateError = false;
      return null;
    } else {
      this.dateError = true;
      return { 'dateDiffValidator': true };
    }
  }
}
