export const en = {

  HOME: {
    SEARCH: {
      START_BOOKING: `Start Booking Now`,
    },
  },

  ROOM: {
    STARTS_FROM: `Starts From`,
    NIGHT: `Night`,
    DESCRIPTION: `Description`,
    BOOK_NOW: `Book Now`,
  },

  ROOMS: {
    FROM: `from`,
  },

  FILTERS: {
    MORE_FILTERS: `More Filters`,
    PRICE_RANGE: `Price Range`,
    STARS: `Stars`,
    ROOM_TYPES: `Room Types`,
  },

  GLOBAL: {
    START_DATE: `Start Date`,
    END_DATE: `End Date`,
    ADULTS: `Adults`,
    CHILDREN: `Children`,
    INFANTS: `Infants`,
    SEARCH: `Search`,
    PHOTOS: `Photos`,
    MAIL: `E Mail`,
    PHONE: `Phone`,
    SOCIAL: `Social`,
    NAME: `Your Name`,
    MESSAGE: `Message`,
    SEND: `Send`,
    CATEGORY: `Category`,

    CLEAR: 'Clear Searches',

    BREAKFAST: 'Breakfast',
    DOUBLE: 'Two meals a day',
    TRIPLE: 'Three meals a day',
  },

  ABOUT_US: {
    BOOK_VISIT: `Want To book visit?`,
    CALL_NOW: `Call Now`,
  },
  SUCCESS_MESSAGES: {
    CONTACT_MESSAGE: `Your message has been sent successfully`,
    BOOKING_MESSAGE: `Your order has been sent successfully`,
    BUTTON: `Okay`,
  },

  'T': '{{en}}',
};

export const ge = {


  HOME: {
    SEARCH: {
      START_BOOKING: `Start Booking Now`,
    },
  },

  ROOM: {
    STARTS_FROM: `Starts From`,
    NIGHT: `Night`,
    DESCRIPTION: `Description`,
    BOOK_NOW: `Book Now`,
  },

  ROOMS: {
    FROM: `from`,
  },

  FILTERS: {
    MORE_FILTERS: `More Filters`,
    PRICE_RANGE: `Price Range`,
    STARS: `Stars`,
    ROOM_TYPES: `Room Types`,
  },

  GLOBAL: {
    START_DATE: `Start Date`,
    END_DATE: `End Date`,
    ADULTS: `Adults`,
    CHILDREN: `Children`,
    INFANTS: `Infants`,
    SEARCH: `Search`,
    PHOTOS: `Photos`,
    MAIL: `E Mail`,
    PHONE: `Phone`,
    SOCIAL: `Social`,
    NAME: `Your Name`,
    MESSAGE: `Message`,
    SEND: `Send`,
    CATEGORY: `Category`,

    CLEAR: 'Clear Searches',

    BREAKFAST: 'Breakfast',
    DOUBLE: 'Two meals a day',
    TRIPLE: 'Three meals a day',
  },

  ABOUT_US: {
    BOOK_VISIT: `Want To book visit?`,
    CALL_NOW: `Call Now`,
  },

  'T': '{{ge}}',
};

export const ru = {


  HOME: {
    SEARCH: {
      START_BOOKING: `Start Booking Now`,
    },
  },

  ROOM: {
    STARTS_FROM: `Starts From`,
    NIGHT: `Night`,
    DESCRIPTION: `Description`,
    BOOK_NOW: `Book Now`,
  },

  ROOMS: {
    FROM: `from`,
  },

  FILTERS: {
    MORE_FILTERS: `More Filters`,
    PRICE_RANGE: `Price Range`,
    STARS: `Stars`,
    ROOM_TYPES: `Room Types`,
  },

  GLOBAL: {
    START_DATE: `Start Date`,
    END_DATE: `End Date`,
    ADULTS: `Adults`,
    CHILDREN: `Children`,
    INFANTS: `Infants`,
    SEARCH: `Search`,
    PHOTOS: `Photos`,
    MAIL: `E Mail`,
    PHONE: `Phone`,
    SOCIAL: `Social`,
    NAME: `Your Name`,
    MESSAGE: `Message`,
    SEND: `Send`,
    CATEGORY: `Category`,

    CLEAR: 'Clear Searches',

    BREAKFAST: 'Breakfast',
    DOUBLE: 'Two meals a day',
    TRIPLE: 'Three meals a day',
  },

  ABOUT_US: {
    BOOK_VISIT: `Want To book visit?`,
    CALL_NOW: `Call Now`,
  },

  'T': '{{ru}}',
}; 
