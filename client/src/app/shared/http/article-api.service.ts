import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { QueryResponse } from '../models/query-response';
import { Article } from '../models/article';

const API_URL = environment.apiUrl;

@Injectable()
export class ArticleApiService {
  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Article>> {
    return this.http.get<any>(`${API_URL}/api/articles`, {
      params,
    });
  }

  getById(articleId: string): Observable<any> {
    return this.http.get(`${API_URL}/api/articles/one/${articleId}`);
  }
}
