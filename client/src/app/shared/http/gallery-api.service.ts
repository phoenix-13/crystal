import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { Gallery } from '../models/gallery';
import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class GalleryApiService {

  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Gallery>> {
    return this.http.get<any>(`${API_URL}/api/gallery`, {
      params,
    });
  }
}
