import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Rooms } from '../models/rooms';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class RoomApiService {
  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Rooms>> {
    return this.http.get<any>(`${API_URL}/api/rooms`, {
      params,
    });
  }

  getById(roomId: string): Observable<any> {
    return this.http.get(`${API_URL}/api/rooms/one/${roomId}`);
  }

  getRoomTypes(params): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/room-types`, {
      params,
    });
  }

  searchRooms(data): Observable<QueryResponse<Rooms>> {
    return this.http.post<any>(`${API_URL}/api/rooms/search-rooms`, data);
  }
}
