import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { HttpClient } from '@angular/common/http';
import { RoomParameters } from '../models/room-parameters';
import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class RoomParameterApiService {

  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<RoomParameters>> {
    return this.http.get<any>(`${API_URL}/api/room-features`, {
      params,
    });
  }
}
