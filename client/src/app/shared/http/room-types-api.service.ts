import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const API_URL = environment.apiUrl;

@Injectable()
export class RoomTypesApiService {
  constructor(private http: HttpClient) { }

  getRoomTypes(params): Observable<any> {
    return this.http.get<any>(`${API_URL}/api/room-types`, {
      params,
    });
  }

}
