import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { QueryResponse } from '../models/query-response';
import { Wellness } from '../models/wellness';


const API_URL = environment.apiUrl;

@Injectable()
export class WellnessApiService {
  constructor(private http: HttpClient) { }

  getByQuery(params): Observable<QueryResponse<Wellness>> {
    return this.http.get<any>(`${API_URL}/api/wellness`, {
      params,
    });
  }
}
