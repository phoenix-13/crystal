export interface Gallery {
  _id?: any;
  title?: any;
  description?: any;
  images?: any;
  date?: any;
  thumbnail?: any;
}