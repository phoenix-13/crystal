export interface QueryResponse<T> {
  contact: any;
  items?: T[];
  numTotal?: number;
}