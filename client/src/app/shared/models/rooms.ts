export interface Rooms {
  _id?: any;
  name?: any;
  stars?: any;
  thumbnail?: any;
  images?: any[];
  description?: any;
  features?: any[];
  defaultPrice?: number;
  priceForAdditionalPerson?: number;
  priceRangeDates?: any[];
  meta?: any;
  type?: any;
}
