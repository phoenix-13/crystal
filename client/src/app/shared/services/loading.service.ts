import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class LoadingService {

  $isSpinning: Subject<boolean>;

  constructor(
  ) {
    this.$isSpinning = new Subject<boolean>();
  }

  start() {
    this.$isSpinning.next(true);
  }

  stop() {
    this.$isSpinning.next(false);
  }

  getStopHandler() {
    return {
      next: () => {
        this.$isSpinning.next(false);
      },
      error: () => {
        this.$isSpinning.next(false);
      }
    };
  }
}
