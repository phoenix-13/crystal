import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LangService } from './services/lang.service';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';
import { RoomComponent } from './components/room/room.component';
import { RoomApiService } from './http/room-api.service';
import { ArticleApiService } from './http/article-api.service';
import { CommonApiService } from './http/common-api.service';
import { FileApiService } from './http/files-api.service';
import { GalleryApiService } from './http/gallery-api.service';
import { RoomParameterApiService } from './http/room-parameter-api.service';
import { RoomTypesApiService } from './http/room-types-api.service';
import { UserApiService } from './http/user-api.service';
import { LoadingService } from './services/loading.service';
import { Ng5SliderModule } from 'ng5-slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule, } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { OwlModule } from 'ngx-owl-carousel';
import { SearchComponent } from './components/search/search.component';
import { RatingModule } from 'ng-starrating';
import { FiltersComponent } from './components/filters/filters.component';
import { RouterModule } from '@angular/router';
import { ResourceUrlPipe } from './pipes/resource-url.pipe';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { BookVisitComponent } from './components/book-visit/book-visit.component';
import { WellnessApiService } from './http/wellness-api.service';
import { SuccessMessageComponent } from './components/modals/success-message/success-message.component';


@NgModule({
  declarations: [
    RoomComponent,
    SearchComponent,
    FiltersComponent,
    ResourceUrlPipe,
    SafeHtmlPipe,
    BookVisitComponent,
    SuccessMessageComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    Ng5SliderModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatMomentDateModule,
    OwlModule,
    RatingModule,
    RouterModule,

  ],
  exports: [
    TranslateModule,
    RoomComponent,
    Ng5SliderModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatMomentDateModule,
    OwlModule,
    SearchComponent,
    RatingModule,
    FiltersComponent,
    RouterModule,
    ResourceUrlPipe,
    SafeHtmlPipe,
    BookVisitComponent,
    SuccessMessageComponent,

  ],
  entryComponents: [SuccessMessageComponent]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        LangService,
        TranslateService,
        CookieService,
        RoomApiService,
        ArticleApiService,
        CommonApiService,
        FileApiService,
        GalleryApiService,
        RoomParameterApiService,
        RoomTypesApiService,
        UserApiService,
        LoadingService,
        WellnessApiService,
      ]
    }
  }
}
