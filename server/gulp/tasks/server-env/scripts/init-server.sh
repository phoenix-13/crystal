NUM_ARGS=2
IP=$1
PORT=$2
PASSWORD=testpassword
USER=gdg
SSH_PORT=22067

if [ $# -ne $NUM_ARGS ] ; then
  echo "Error! Wrong number of arguments ($# instead of ${NUM_ARGS})"
  exit 128
fi

# init root
ssh-keyscan -H $IP >> ~/.ssh/known_hosts
ssh root@$IP -p $SSH_PORT 'bash -s' < ./gulp/tasks/server-env/scripts/root/init-lang-and-time.sh
ssh root@$IP -p $SSH_PORT 'bash -s' < ./gulp/tasks/server-env/scripts/root/add-user.sh $USER $PASSWORD

# init user
sshpass -p $PASSWORD ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@$IP
rsync -avr -e "ssh -p $SSH_PORT" ./gulp/tasks/server-env/files/ $USER@$IP:
ssh $USER@$IP -p $SSH_PORT 'bash -s' < ./gulp/tasks/server-env/scripts/user/init-env.sh $PORT
