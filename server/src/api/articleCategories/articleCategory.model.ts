import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';

const ArticleCategorySchema = new Schema({
  title: multilingualSchema,
});

export default model('ArticleCategory', ArticleCategorySchema);