import { Router, Request, Response, NextFunction } from 'express';
import * as articleCategoryDao from './articleCategory.dao';
import * as articleCategoryParser  from './articleCategory.parser';
import * as auth from '../../auth';


const articleCategoryRouter = Router();

articleCategoryRouter.get('/', articleCategoryParser.parseGetByQuery, getByQuery);

articleCategoryRouter.post('/', auth.isAdmin, articleCategoryParser.parseCreate, create);

articleCategoryRouter.post('/update', auth.isAdmin, articleCategoryParser.parseUpdate, update);

articleCategoryRouter.delete('/:id', auth.isAdmin, destroy);

export default articleCategoryRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const articleCategoriesData = await articleCategoryDao.getByQuery(query);
    res.json(articleCategoriesData);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await articleCategoryDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await articleCategoryDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await articleCategoryDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}