import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';
import metaTagsSchema from '../../schemas/metaTags.schema';
import imageSchema from '../../schemas/image.schema';
const { ObjectId } = Schema.Types;

const ArticleSchema = new Schema({
  title: multilingualSchema,
  description: multilingualSchema,
  thumbnail: imageSchema,
  content: multilingualSchema,
  articleCategory: { type: ObjectId, ref: 'ArticleCategory' },
  createdAt: Date,
  isFeatured: Boolean,
  meta: metaTagsSchema,
});

export default model('Article', ArticleSchema);