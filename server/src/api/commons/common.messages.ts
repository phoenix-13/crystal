'use strict';

import * as mailer from '../../helpers/mailer';
import { contacts } from '../../constants/common';


export function sendClientMessage(payload: any) {
  const content = `
    <p> Name: ${payload.name} </p>
    <p>Email: ${payload.email}</p>
    <p>Message: ${payload.message}</p>
  `;

  const subject = `Contact Request`;
  mailer.sendHtml({ email: contacts.MAIL_TO, subject, content });
}

export function sendClientInquiryMessage(payload: any) {
  const content = `
    <p> Start Date: ${payload.startDate} </p>
    <p> End Date: ${payload.endDate} </p>
    <p> Contact Info (Email): ${payload.email} </p>
    <p> Contact Info (Phone): ${payload.phone} </p>
    <p> Details: ${payload.details} </p>
    <p> Preffered Meal Type: ${payload.features} </p>
    <p> Number of Adults: ${payload.adults} </p>
    <p> Number of children: ${payload.children} </p>
    <p> Number of infants: ${payload.infants} </p>
  `;

  const subject = `Booking Request`;
  mailer.sendHtml({ email: contacts.MAIL_TO, subject, content });
}
