import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';
import imageSchema from '../../schemas/image.schema';

const soclialLinksSchema = {
  icon: String,
  url: String,
};

const contactSchema = {
  phone: String,
  email: String,
  address: multilingualSchema,
  socialLinks: [soclialLinksSchema],
  facebookUrl: String,
  twitterUrl: String,
  linkedinUrl: String,
  instagramUrl: String,
  location: {
    lat: Number,
    lng: Number,
  },
  mapZoom: Number,
};

const promoSchema = {
  promoImage: {
    ge: imageSchema,
    en: imageSchema,
    ru: imageSchema,
  },
  title: multilingualSchema,
  subtitle: multilingualSchema,
  buttonText: multilingualSchema,
  buttonLink: String,
  headerLogo: {
    ge: imageSchema,
    en: imageSchema,
    ru: imageSchema,
  },
  footerLogo: {
    ge: imageSchema,
    en: imageSchema,
    ru: imageSchema,
  },

  headerColor: String,
  langColor: String,
};

const aboutSchema = {
  image: imageSchema,
  title: multilingualSchema,
  subtitle: multilingualSchema,
  content: multilingualSchema,
  contactText: multilingualSchema,
  contactPhone: String,
};

const bannerItemSchema = {
  title: multilingualSchema,
  image: imageSchema,
};

const bannerSchema = {
  about: bannerItemSchema,
  wellness: bannerItemSchema,
  rooms: bannerItemSchema,
  room: bannerItemSchema,
  news: bannerItemSchema,
  article: bannerItemSchema,
  gallery: bannerItemSchema,
  contact: bannerItemSchema,
};

const priceRangeShema = {
  startDate: Date,
  endDate: Date,
  singleMealPrice: Number,
  doubleMealPrice: Number,
  tripleMealPrice: Number,
};

const homeSectionsSchema = {
  rooms: {
    title: multilingualSchema,
    description: multilingualSchema,
    button: multilingualSchema,
  },
  gallery: {
    title: multilingualSchema,
    description: multilingualSchema,
  },
  contact: {
    title: multilingualSchema,
    description: multilingualSchema,
  }
};

const defaultMealPriceRangeSchema = {
  singleMealPrice: Number,
  doubleMealPrice: Number,
  tripleMealPrice: Number,
};

const wellnessSchema = {
  contactText: multilingualSchema,
  contactPhone: String,
  image1: imageSchema,
  image2: imageSchema,
  image3: imageSchema,
  image4: imageSchema,
  image5: imageSchema,
};

const staticTextsSchema = {
  header: {
    about: multilingualSchema,
    wellness: multilingualSchema,
    rooms: multilingualSchema,
    news: multilingualSchema,
    gallery: multilingualSchema,
    contact: multilingualSchema,
  },

  room: {
    startsFrom_prefix: multilingualSchema,
    startsFrom_suffix: multilingualSchema,
    night: multilingualSchema,
    description: multilingualSchema,
    bookNow: multilingualSchema,
    from: multilingualSchema,

    emailField: multilingualSchema,
    phoneField: multilingualSchema,
    bookingMessage: multilingualSchema,
    breakfast: multilingualSchema,
    doubleMeal: multilingualSchema,
    tripleMeal: multilingualSchema,
    bookingButton: multilingualSchema,
  },

  filters: {
    moreFilters: multilingualSchema,
    priceRange: multilingualSchema,
    stars: multilingualSchema,
    roomTypes: multilingualSchema,
    search: multilingualSchema,
    clear: multilingualSchema,
    resultsNotFound: multilingualSchema,
  },

  global: {
    startDate: multilingualSchema,
    endDate: multilingualSchema,
    adults: multilingualSchema,
    children: multilingualSchema,
    infants: multilingualSchema,
    search: multilingualSchema,
    photos: multilingualSchema,
    mail: multilingualSchema,
    phone: multilingualSchema,
    address: multilingualSchema,
    social: multilingualSchema,
    name: multilingualSchema,
    message: multilingualSchema,
    send: multilingualSchema,
    category: multilingualSchema,
    startBooking: multilingualSchema,
    bookVisit: multilingualSchema,
    callNow: multilingualSchema,
  },

  successMessages: {
    contactMessage: multilingualSchema,
    bookingMessage: multilingualSchema,
    button: multilingualSchema,
  },
};

const filterLimitsSchema = {
  from: Number,
  to: Number,
};

const CommonSchema = new Schema({
  contact: contactSchema,
  promo: promoSchema,
  aboutUs: aboutSchema,
  wellness: wellnessSchema,
  banners: bannerSchema,
  priceRangeDates: [priceRangeShema],
  defaultMealPriceRanges: defaultMealPriceRangeSchema,
  homeSections: homeSectionsSchema,
  filterLimits: filterLimitsSchema,
  staticTexts: staticTextsSchema,
});

export default model('Common', CommonSchema);