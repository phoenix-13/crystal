import * as _ from 'lodash';
import { Request, Response, NextFunction } from 'express';
import { parseOffsetAndLimit } from '../../helpers/parser-utils';

// =============== GET ===============

export function parseGetByQuery(req: Request, res: Response, next: NextFunction) {
  // @ts-ignore
  const { query } = req;
  // @ts-ignore
  req.query = {
    // @ts-ignore
    ...parseOffsetAndLimit(query),
    find: {
      // @ts-ignore
      ...parseId(query),
    },
    // @ts-ignore
    ...parseSearch(query),
  };
  next();
}

function parseId({ _id }: { _id: any }) {
  return _id ? { _id } : {};
}

function parseSearch({ keyword }: { keyword: string }) {
  return keyword ? {
    or: [
      { hostFee: { $regex: keyword, $options: 'i' } },
    ],
  } : {};
}

export function parseSendEmail(req: Request, res: Response, next: NextFunction) {
  req.body = _.pick(req.body, ['name', 'email', 'message']);
  next();
}

export function parseSendInquiry(req: Request, res: Response, next: NextFunction) {
  req.body = _.pick(req.body, ['startDate', 'endDate', 'details', 'email', 'phone', 'features', 'adults', 'children', 'infants']);
  next();
}

// =============== POST ===============

export function parseCreate(req: Request, res: Response, next: NextFunction) {
  req.body = parseBaseProps(req.body),
    next();
}

export function parseUpdate(req: Request, res: Response, next: NextFunction) {
  req.body = parseCommon(req.body);
  next();
}

function parseBaseProps(body: any) {
  return _.pick(body, [
    'pricing',
  ]);
}


function parseCommon(body: any) {
  const parsedBody: any = {};

  if (body.contact) parsedBody.contact = body.contact;
  if (body.promo) parsedBody.promo = body.promo;
  if (body.aboutUs) parsedBody.aboutUs = body.aboutUs;
  if (body.wellness) parsedBody.wellness = body.wellness;
  if (body.banners) parsedBody.banners = body.banners;
  if (body.priceRangeDates) parsedBody.priceRangeDates = body.priceRangeDates;
  if (body.homeSections) parsedBody.homeSections = body.homeSections;
  if (body.filterLimits) parsedBody.filterLimits = body.filterLimits;
  if (body.defaultMealPriceRanges) parsedBody.defaultMealPriceRanges = body.defaultMealPriceRanges;
  if (body.staticTexts) parsedBody.staticTexts = body.staticTexts;

  return parsedBody;
}