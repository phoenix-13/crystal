import { Router, Request, Response, NextFunction } from 'express';
import * as commonDao from './common.dao';
import * as commonParser  from './common.parser';
import * as auth from '../../auth';
import { sendClientMessage, sendClientInquiryMessage } from './common.messages';
import { langs } from '../../constants/common';

const commonRouter = Router();

commonRouter.get('/one', getOne);
commonRouter.post('/update', auth.isAdmin, commonParser.parseUpdate, update);
commonRouter.post('/send-email', commonParser.parseSendEmail, sendEmail);
commonRouter.post('/send-inquiry', commonParser.parseSendInquiry, sendInquiry);


export default commonRouter;

// =============== GET ===============

async function getOne(req: Request, res: Response, next: NextFunction) {
  try {
    const data = await commonDao.getOne();
    res.json(data);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await commonDao.update(payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function sendEmail(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await sendClientMessage(payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function sendInquiry(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await sendClientInquiryMessage(payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}