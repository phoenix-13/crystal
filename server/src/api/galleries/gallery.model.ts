import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';
import imageSchema from '../../schemas/image.schema';
import metaTagsSchema from '../../schemas/metaTags.schema';

const GallerySchema = new Schema({
  title: multilingualSchema,
  thumbnail: imageSchema,
  description: multilingualSchema,
  date: Date,
  images: [imageSchema],
  meta: metaTagsSchema,
});

export default model('Gallery', GallerySchema);