import { Router, Request, Response, NextFunction } from 'express';
import * as galleryDao from './gallery.dao';
import * as galleryParser  from './gallery.parser';
import * as auth from '../../auth';


const galleryRouter = Router();

galleryRouter.get('/', galleryParser.parseGetByQuery, getByQuery);
galleryRouter.post('/', auth.isAdmin, galleryParser.parseCreate, create);
galleryRouter.post('/update', auth.isAdmin, galleryParser.parseUpdate, update);
galleryRouter.delete('/:id', auth.isAdmin, destroy);

export default galleryRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const galleriesData = await galleryDao.getByQuery(query);
    res.json(galleriesData);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await galleryDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await galleryDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await galleryDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}