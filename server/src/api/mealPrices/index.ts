import { Router, Request, Response, NextFunction } from 'express';
import * as mealPriceDao from './mealPrice.dao';
import * as mealPriceParser  from './mealPrice.parser';
import * as auth from '../../auth';


const mealPriceRouter = Router();

mealPriceRouter.get('/', mealPriceParser.parseGetByQuery, getByQuery);
mealPriceRouter.post('/', auth.isAdmin, mealPriceParser.parseCreate, create);
mealPriceRouter.post('/update', auth.isAdmin, mealPriceParser.parseUpdate, update);
mealPriceRouter.delete('/:id', auth.isAdmin, destroy);

export default mealPriceRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const mealPricesData = await mealPriceDao.getByQuery(query);
    res.json(mealPricesData);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await mealPriceDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await mealPriceDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await mealPriceDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}