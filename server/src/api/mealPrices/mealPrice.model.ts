import { Schema, model } from 'mongoose';

const priceRangeShema = {
  startDate: Date,
  endDate: Date,
  singleMealPrice: Number,
  doubleMealPrice: Number,
  tripleMealPrice: Number,
};

const MealPriceSchema = new Schema({
  priceRangeDates: [priceRangeShema],
});

export default model('MealPrice', MealPriceSchema);