import { Schema, model } from 'mongoose';
import metaTagsSchema from '../../schemas/metaTags.schema';

const MetaSchema = new Schema({
  home: metaTagsSchema,
  contacts: metaTagsSchema,
  rooms: metaTagsSchema,
  articles: metaTagsSchema,
  gallery: metaTagsSchema,
  aboutUs: metaTagsSchema,
  wellness: metaTagsSchema,
});

export default model('Meta', MetaSchema);