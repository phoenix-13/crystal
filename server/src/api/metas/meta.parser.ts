import * as _ from 'lodash';
import { Request, Response, NextFunction } from 'express';
import { parseOffsetAndLimit } from '../../helpers/parser-utils';

// =============== GET ===============

export function parseGetByQuery(req: Request, res: Response, next: NextFunction) {
  const { query } = req;
  // @ts-ignore
  req.query = {
    // @ts-ignore
    ...parseOffsetAndLimit(query),
    find: {
      // @ts-ignore
      ...parseId(query),
    },
    // @ts-ignore
    ...parseSearch(query),
  };
  next();
}

function parseId({ _id }: { _id: any }) {
  return _id ? { _id } : {};
}

function parseSearch({ keyword }: { keyword: string }) {
  return keyword ? {
    or: [
      { name: { $regex: keyword, $options: 'i' } },
    ],
  } : {};
}

// =============== POST ===============

export function parseUpdate(req: Request, res: Response, next: NextFunction) {
  req.body = parseMeta(req.body);
  next();
}

function parseMeta(body: any) {
  const parsedBody: any = {};

  if (body.home) parsedBody.home = body.home;
  if (body.contacts) parsedBody.contacts = body.contacts;
  if (body.rooms) parsedBody.rooms = body.rooms;
  if (body.articles) parsedBody.articles = body.articles;
  if (body.gallery) parsedBody.gallery = body.gallery;
  if (body.aboutUs) parsedBody.aboutUs = body.aboutUs;
  if (body.wellness) parsedBody.wellness = body.wellness;

  return parsedBody;
}
