import { Router, Request, Response, NextFunction } from 'express';
import * as roomFeatureDao from './roomFeature.dao';
import * as roomFeatureParser  from './roomFeature.parser';
import * as auth from '../../auth';


const roomFeatureRouter = Router();

roomFeatureRouter.get('/', roomFeatureParser.parseGetByQuery, getByQuery);
roomFeatureRouter.post('/', auth.isAdmin, roomFeatureParser.parseCreate, create);
roomFeatureRouter.post('/update', auth.isAdmin, roomFeatureParser.parseUpdate, update);
roomFeatureRouter.delete('/:id', auth.isAdmin, destroy);

export default roomFeatureRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const roomFeaturesData = await roomFeatureDao.getByQuery(query);
    res.json(roomFeaturesData);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await roomFeatureDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await roomFeatureDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await roomFeatureDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}