import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';
import imageSchema from '../../schemas/image.schema';


const RoomFeatureSchema = new Schema({
  name: multilingualSchema,
  icon: imageSchema,
});

export default model('RoomFeature', RoomFeatureSchema);