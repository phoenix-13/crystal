import { Router, Request, Response, NextFunction } from 'express';
import * as roomTypeDao from './roomType.dao';
import * as roomTypeParser  from './roomType.parser';
import * as auth from '../../auth';


const roomTypeRouter = Router();

roomTypeRouter.get('/', roomTypeParser.parseGetByQuery, getByQuery);

roomTypeRouter.post('/', auth.isAdmin, roomTypeParser.parseCreate, create);
roomTypeRouter.post('/update', auth.isAdmin, roomTypeParser.parseUpdate, update);
roomTypeRouter.post('/update/positions', auth.isAdmin, roomTypeParser.parseUpdatePositions, updatePositions);
roomTypeRouter.delete('/:id', auth.isAdmin, destroy);

export default roomTypeRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const roomTypesData = await roomTypeDao.getByQuery(query);
    res.json(roomTypesData);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await roomTypeDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await roomTypeDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}


async function updatePositions(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await payload.items.map((item: any) => {
      roomTypeDao.update(item._id, { position: item.position });
    });
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await roomTypeDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}