import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';

const RoomTypeSchema = new Schema({
  name: multilingualSchema,
  position: Number,
});

export default model('RoomType', RoomTypeSchema);