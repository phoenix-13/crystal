import { Router, Request, Response, NextFunction } from 'express';
import * as roomDao from './room.dao';
import * as roomParser  from './room.parser';
import * as auth from '../../auth';
import { ObjectId } from 'bson';
const moment = require('moment');
const _ = require('lodash');

const roomRouter = Router();

roomRouter.get('/', roomParser.parseGetByQuery, getByQuery);
roomRouter.get('/one/:roomId', getById);

roomRouter.post('/', auth.isAdmin, roomParser.parseCreate, create);
roomRouter.post('/update', auth.isAdmin, roomParser.parseUpdate, update);

roomRouter.post('/search-rooms', roomParser.parseRoomDate, searchRooms);

roomRouter.delete('/:id', auth.isAdmin, destroy);

export default roomRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const roomsData = await roomDao.getByQuery(query);
    res.json(roomsData);
  } catch (e) {
    next(e);
  }
}

async function getById(req: Request, res: Response, next: NextFunction) {
  try {
    const { roomId } = req.params;
    const room = await roomDao.getById(roomId);
    res.json(room);
  } catch (e) {
    next(e);
  }
}

async function searchRooms(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    const guests = (payload.children && payload.adults) ? parseInt(payload.children) + parseInt(payload.adults) : 1;
    let infantFriendly = {};
    if (parseInt(payload.infants) > 0) {
      infantFriendly = {infantFriendly: true};
    }
    const stars = (payload.starValue) ? { stars: parseInt(payload.starValue) } : {};
    const roomType = (Array.isArray(payload.type)) ? _.map(payload.type, function(value: any) { return value = new ObjectId(value); }) : [new ObjectId(payload.type)];
    const type = (payload.type && (Array.isArray(payload.type) && payload.type.length)) ? { type: {$in: roomType} } : {};
    const query = {
      guests,
      find: {
        ...infantFriendly,
        ...stars,
        ...type,
      },
    };
    let roomsData = await roomDao.getBySearchQuery(query);
    if (payload.priceFrom && payload.priceTo) {
      roomsData = getFilterDataByPriceRange(roomsData, payload, guests);
    }
    res.json(roomsData);
  } catch (e) {
    next(e);
  }
}

function getPriceByDate(priceRangeDates: any, startDate: any) {
  const startDateVal = moment(startDate);
  const priceValue = _.find(priceRangeDates, function(val: any) {
    return startDate >= val.startDate && startDateVal < val.endDate;
  });
  return priceValue;
}

function getDates(startDate: any, stopDate: any) {
  const dateArray = [];
  while (startDate < stopDate) {
    dateArray.push( moment(startDate).format('YYYY-MM-DD') );
    startDate = moment(startDate).add(1, 'days');
  }
  return dateArray;
}

function getFilterDataByPriceRange(roomsData: any, payload: any, guests: number) {
  if (payload.startDate && payload.endDate) {
    payload.startDate = moment(payload.startDate);
    payload.endDate = moment(payload.endDate);
  }
  const totalDuration = (payload.startDate && payload.endDate) ? payload.endDate.diff(payload.startDate, 'days') : {};
  const dateArray = (payload.startDate && payload.endDate) ? getDates(payload.startDate, payload.endDate) : {};
  _.map(roomsData, function(value: any) {
    if (!payload.startDate && !payload.endDate) {
      value.dailyPrice = (guests > value.numberOfGuests) ? (value.defaultPrice + (guests - value.numberOfGuests) * value.priceForAdditionalPerson) : value.defaultPrice;
      value.totalPrice = value.dailyPrice;
    } else {
      const priceArray: any[] = [];
      _.map(dateArray, function(val: any) {
        priceArray.push(getPriceByDate(value.priceRangeDates, val) ? getPriceByDate(value.priceRangeDates, val) : {defaultPrice: value.defaultPrice, priceForAdditionalPerson: value.priceForAdditionalPerson});
      });
      const avaragePrice = Math.round(_.sumBy(priceArray, function(o: any) { return o.defaultPrice; }) / totalDuration);
      const avaragePriceForAdditionalPerson = Math.round(_.sumBy(priceArray, function(o: any) { return o.priceForAdditionalPerson; }) / totalDuration);
      value.dailyPrice = (guests > value.numberOfGuests) ? (avaragePrice + (guests - value.numberOfGuests) * avaragePriceForAdditionalPerson) : avaragePrice;
      value.totalPrice = value.dailyPrice * totalDuration;
    }
  });
  roomsData = _.filter(roomsData, function(room: any) { return room.dailyPrice >= payload.priceFrom && room.dailyPrice < payload.priceTo; });
  return roomsData;
}


// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await roomDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await roomDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await roomDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}