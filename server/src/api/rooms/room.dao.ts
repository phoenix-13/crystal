import Model from './room.model';
import Promise, { any } from 'bluebird';
import { assertFound } from '../../helpers/db-result-handler';
const _ = require('lodash');

// =============== Getters ===============

export function getByQuery({ find = {}, or = [{}], sort = { stars: -1, _id: -1 }, offset = 0, limit = 10 }) {
  return Promise.all([
    Model.find(find).lean().or(or).sort(sort).skip(offset).limit(limit),
    Model.find(find).lean().or(or).countDocuments()
  ]).spread((items: any[], numTotal: number) => ({ items, numTotal }));
}

export function getById(id: any): any {
  return Model.findOne({ _id: id }).lean().populate('features type')
    .then(assertFound(`Room (id ${id}) was not found`));
}

export  function getBySearchQuery({ find, guests, offset, limit }: any) {
  // console.log(guests, 'find');
  return Model.aggregate([
    { $addFields: { totalGuest: { $add: [ '$numberOfGuests', '$numberOfAdditionalGuests' ]} } },
    { $match: {
      totalGuest: {$gte: guests},
      ...find,
      }
    },
    { $sort: { stars: -1, totalGuest: 1, numberOfGuests: -1 } },
    // { $skip: offset },
    { $limit: 100 },
  ]).then(assertFound(`Rooms () was not found`));
}

// =============== Setters ===============

export function create(data: any) {
  return Model.create(data);
}

export function insertMany(data: any) {
  return Model.insertMany(data);
}

export function update(id: any, data: any) {
  return Model.findOneAndUpdate({ _id: id }, { $set: data })
    .then(assertFound(`Could not update room (id ${id})`));
}

export function destroy(id: any) {
  return Model.findOneAndRemove({ _id: id })
    .then(assertFound(`Could not destroy room (id ${id})`));
}

export function destroyAll() {
  return Model.deleteMany({});
}

