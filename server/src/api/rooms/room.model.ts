import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';
import imageSchema from '../../schemas/image.schema';
import metaTagsSchema from '../../schemas/metaTags.schema';
const { ObjectId } = Schema.Types;

const priceRangeShema = {
  startDate: Date,
  endDate: Date,
  defaultPrice: Number,
  priceForAdditionalPerson: Number,
};

const RoomSchema = new Schema({
  name: multilingualSchema,
  stars: Number,
  defaultPrice: Number,
  thumbnail: imageSchema,
  priceForAdditionalPerson: Number,
  images: [imageSchema],
  description: multilingualSchema,
  features: [{ type: ObjectId, ref: 'RoomFeature' }],
  priceRangeDates: [priceRangeShema],
  meta: metaTagsSchema,
  type: { type: ObjectId, ref: 'RoomType' },
  numberOfGuests: Number,
  numberOfAdditionalGuests: Number,
  infantFriendly: Boolean,
});

export default model('Room', RoomSchema);