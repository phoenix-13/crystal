import { Router, Request, Response, NextFunction } from 'express';
import * as wellnessDao from './wellness.dao';
import * as wellnessParser  from './wellness.parser';
import * as auth from '../../auth';


const wellnessRouter = Router();

wellnessRouter.get('/', wellnessParser.parseGetByQuery, getByQuery);

wellnessRouter.post('/', auth.isAdmin, wellnessParser.parseCreate, create);

wellnessRouter.post('/update', auth.isAdmin, wellnessParser.parseUpdate, update);

wellnessRouter.delete('/:id', auth.isAdmin, destroy);

export default wellnessRouter;

// =============== GET ===============

async function getByQuery(req: Request, res: Response, next: NextFunction) {
  try {
    const query = req.query;
    const wellnessesData = await wellnessDao.getByQuery(query);
    res.json(wellnessesData);
  } catch (e) {
    next(e);
  }
}

// =============== POST ===============

async function create(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await wellnessDao.create(payload);
    res.sendStatus(201);
  } catch (e) {
    next(e);
  }
}

async function update(req: Request, res: Response, next: NextFunction) {
  try {
    const payload = req.body;
    await wellnessDao.update(payload._id, payload);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function destroy(req: Request, res: Response, next: NextFunction) {
  try {
    const { id } = req.params;
    await wellnessDao.destroy(id);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}