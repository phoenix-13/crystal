import { Schema, model } from 'mongoose';
import multilingualSchema from '../../schemas/multilingual.schema';

const procedureSchema = {
  name: multilingualSchema,
  price: multilingualSchema,
  duration: multilingualSchema,
};

const WellnessSchema = new Schema({
  name: multilingualSchema,
  procedures: [procedureSchema],
});

export default model('Wellness', WellnessSchema);