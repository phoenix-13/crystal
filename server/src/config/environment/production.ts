export default {
  port: 4002,

  url: {
    scheme: 'http',
    host: 'crystal.com',
    api: 'crystal.com',
  },

  mongo: {
    uri: `mongodb://localhost:27017/crystal`,
    options: {
      useNewUrlParser: true,
      useCreateIndex: true,
    }
  },

  // seedDB: true,
};
