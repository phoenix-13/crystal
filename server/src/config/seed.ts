import logger from '../helpers/logger';
import config from '../config/environment';
import { roles } from '../constants/user';
import * as _ from 'lodash';

import * as UserDao from '../api/users/user.dao';
import * as ArticleDao from '../api/articles/article.dao';
import * as RoomDao from '../api/rooms/room.dao';
import * as GalleryDao from '../api/galleries/gallery.dao';
import * as RoomFeatureDao from '../api/roomFeatures/roomFeature.dao';
import * as CommonDao from '../api/commons/common.dao';
import * as MetaDao from '../api/metas/meta.dao';
import * as MealPriceDao from '../api/mealPrices/mealPrice.dao';
import * as roomTypeDao from '../api/roomTypes/roomType.dao';
import * as wellnessDao from '../api/wellnesses/wellness.dao';

import * as UserStub from '../stubs/user.stub';
import * as ArticleStub from '../stubs/article.stub';
import * as RoomStub from '../stubs/room.stub';
import * as GalleryStub from '../stubs/gallery.stub';
import * as RoomFeatureStub from '../stubs/roomFeature.stub';
import * as CommonStub from '../stubs/common.stub';
import * as MetaStub from '../stubs/meta.stub';
import * as mealPriceStub from '../stubs/mealPrice.stub';
import * as roomTypeStub from '../stubs/roomType.stub';
import * as wellnessStub from '../stubs/wellness.stub';


export async function seedDB() {
  const { seedDB, env } = config;
  if (!seedDB) return;
  if (env === 'development') {
    await clearDBDevelopment();
    await seedDBDevelopment();
  }
  if (env === 'production') {
    await clearDBProduction();
    await seedDBProduction();
  }
}

export async function seedDBDevelopment() {
  await UserDao.insertMany(getAdmin());
  await MetaDao.insertMany(MetaStub.getMeta());
  await CommonDao.insertMany(CommonStub.getCommon());
  await ArticleDao.insertMany(ArticleStub.getMany(11));
  await RoomDao.insertMany(RoomStub.getMany(11));
  await GalleryDao.insertMany(GalleryStub.getMany(11));
  await RoomFeatureDao.insertMany(RoomFeatureStub.getMany(11));
  await MealPriceDao.insertMany(mealPriceStub.getMany(11));
  await roomTypeDao.insertMany(roomTypeStub.getMany(11));
  await wellnessDao.insertMany(wellnessStub.getMany(11));

  logger.info('Seed DB development completed');
}

export async function seedDBProduction() {
  await UserDao.insertMany(getAdmin());
  await MetaDao.insertMany(MetaStub.getMeta());
  await CommonDao.insertMany(CommonStub.getCommon());
  await ArticleDao.insertMany(ArticleStub.getMany(11));
  await RoomDao.insertMany(RoomStub.getMany(11));
  await GalleryDao.insertMany(GalleryStub.getMany(11));
  await RoomFeatureDao.insertMany(RoomFeatureStub.getMany(11));
  await MealPriceDao.insertMany(mealPriceStub.getMany(11));
  await roomTypeDao.insertMany(roomTypeStub.getMany(11));
  await wellnessDao.insertMany(wellnessStub.getMany(11));

  logger.info('Seed DB production completed');
}

export async function clearDBDevelopment() {
  await UserDao.destroyAll();
  await MetaDao.destroyAll();
  await CommonDao.destroyAll();
  await ArticleDao.destroyAll();
  await RoomDao.destroyAll();
  await GalleryDao.destroyAll();
  await MealPriceDao.destroyAll();
  await RoomFeatureDao.destroyAll();
  await roomTypeDao.destroyAll();
  await wellnessDao.destroyAll();
}

export async function clearDBProduction() {
  await UserDao.destroyAll();
  await MetaDao.destroyAll();
  await CommonDao.destroyAll();
  await ArticleDao.destroyAll();
  await RoomDao.destroyAll();
  await GalleryDao.destroyAll();
  await MealPriceDao.destroyAll();
  await RoomFeatureDao.destroyAll();
  await roomTypeDao.destroyAll();
  await wellnessDao.destroyAll();
}

function getAdmin() {
  return [
    UserStub.getSingle({
      email: 'admin@crystal.com',
      name: 'Admin',
      role: roles.ADMIN,
      isActivated: true,
    })
  ];
}