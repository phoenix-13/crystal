'use strict';

export const contacts = {
  MAIL_TO: 'reservation@hotelcrystal.ge',
};

export const langs = {
  EN: 'en',
  GE: 'ge',
  RU: 'ru',
};