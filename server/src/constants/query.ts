export const MAX_LIMIT = Number.MAX_SAFE_INTEGER;

export const CIRCLE_MAX_DISTANCE = 50 * 1000;

export const SQUARE_MAX_DISTANCE = 200 * 1000;
