import * as path from 'path';
import * as winston from 'winston';
import config from '../config/environment';

const filename = path.join(config.paths.log, `${config.env}.log`);

if (config.env === 'production') {
  winston.add(new winston.transports.File({filename}));
} else {
  winston.add(new winston.transports.Console());
}

export default winston;
