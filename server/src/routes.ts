import express from 'express';
import { Express, Response, Request, NextFunction } from 'express';
import path from 'path';
import config from './config/environment';
import logger from './helpers/logger';
import * as auth from './auth';
import * as locator from './helpers/locator';
import { AppError } from './errors';

import fileRouter from './api/files';
import userRouter from './api/users';
import commonRouter from './api/commons';
import metaRouter from './api/metas';
import roomRouter from './api/rooms';
import roomFeatureRouter from './api/roomFeatures';
import articleRouter from './api/articles';
import galleryRouter from './api/galleries';
import { getMetaTags } from './helpers/metaTagsHelper';
import roomTypeRouter from './api/roomTypes';
import wellnessRouter from './api/wellnesses';
import articleCategoryRouter from './api/articleCategories';

export function initRoutes(app: Express) {
  app.use(express.static(path.join(config.paths.uploads)));

  app.get('/admin', renderAdminHtml);
  app.get('/', setMetaTags, renderClientHtml);
  app.use(express.static(path.join(config.root, '../client/dist')));
  app.use(express.static(path.join(config.root, '../admin/dist')));
  app.set('views', path.join(config.root, '../client/dist'));
  app.set('views', path.join(config.root, '../admin/dist'));

  app.use(auth.setUser);
  app.use(locator.setLocation);

  app.use('/api/users', userRouter);
  app.use('/api/files', fileRouter);
  app.use('/api/commons', commonRouter);
  app.use('/api/metas', metaRouter);
  app.use('/api/rooms', roomRouter);
  app.use('/api/room-features', roomFeatureRouter);
  app.use('/api/articles', articleRouter);
  app.use('/api/gallery', galleryRouter);
  app.use('/api/room-types', roomTypeRouter);
  app.use('/api/wellness', wellnessRouter);
  app.use('/api/article-categories', articleCategoryRouter);


  app.get('/admin/*', renderAdminHtml);
  app.get('/*', setMetaTags, renderClientHtml);

  app.use(handleError);
}

function renderClientHtml(req: any, res: Response) {
  res.render(path.join(config.root, '../client/dist/index.html'), req.metaTags);
}

function renderAdminHtml(req: Request, res: Response) {
  res.render(path.join(config.root, '../admin/dist/index.html'));
}

async function setMetaTags(req: any, res: Response, next: NextFunction) {
  try {
    const { originalUrl: url } = req;
    req.metaTags = await getMetaTags(url);
    next();
  } catch (e) {
    next(e);
  }
}

function handleError(err: Error, req: Request, res: Response, next: NextFunction) {
  try {
    const status = Object(err).httpStatusCode || 500;
    const data = Object(err).data;
    if (data) {
      res.json(data);
    } else {
      res.sendStatus(status);
    }
    if (err instanceof AppError) {
      logger.error(err.message);
    } else {
      logger.error(err);
    }
  } catch (error) {
    logger.error(error);
  }
}
