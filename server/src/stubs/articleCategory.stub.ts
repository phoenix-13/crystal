import * as _ from 'lodash';
import { cloneStub }  from '../helpers/stub-helpers';

const ArticleCategoryStub = {
  name: 'name',
};

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(ArticleCategoryStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
    name: `name_${i}`,
  }));
}