import * as _ from 'lodash';
import { cloneStub } from '../helpers/stub-helpers';

const CommonStub = {
  contact: getContactObject(),
  promo: getPromoObject(),
  aboutUs: getAboutObject(),
  banners: getBannersObject(),
  priceRangeDates: [getPriceRange(), getPriceRange()],
  homeSections: getHomeSectionsObject(),
  defaultMealPriceRanges: getDefaultMealPriceRangesObject(),
  wellness: getWellnessObject(),

  filterLimits: getFilterLimitsObject(),
  staticTexts: getStaticTextsObject(),

};

function getFilterLimitsObject(): any {
  return {
    from: 10,
    to: 1000,
  };
}

function getStaticTextsObject(): any {
  return {
    header: {
      about: {ge: 'about', en: 'about', ru: 'about'},
      wellness: {ge: 'wellness', en: 'wellness', ru: 'wellness'},
      rooms: {ge: 'rooms', en: 'rooms', ru: 'rooms'},
      news: {ge: 'news', en: 'news', ru: 'news'},
      gallery: {ge: 'gallery', en: 'gallery', ru: 'gallery'},
      contact: {ge: 'contact', en: 'contact', ru: 'contact'},
    },

    room: {
      startsFrom_prefix: {ge: 'from', en: 'from', ru: 'from'},
      startsFrom_suffix: {ge: '', en: '', ru: ''},
      night: {ge: 'night', en: 'night', ru: 'night'},
      description: {ge: 'description', en: 'description', ru: 'description'},
      bookNow: {ge: 'bookNow', en: 'bookNow', ru: 'bookNow'},
      from: {ge: 'from', en: 'from', ru: 'from'},

      emailField: {ge: 'emailField', en: 'emailField', ru: 'emailField'},
      phoneField: {ge: 'phoneField', en: 'phoneField', ru: 'phoneField'},
      bookingMessage: {ge: 'bookingMessage', en: 'bookingMessage', ru: 'bookingMessage'},
      breakfast: {ge: 'breakfast', en: 'breakfast', ru: 'breakfast'},
      doubleMeal: {ge: 'doubleMeal', en: 'doubleMeal', ru: 'doubleMeal'},
      tripleMeal: {ge: 'tripleMeal', en: 'tripleMeal', ru: 'tripleMeal'},
      bookingButton: {ge: 'bookingButton', en: 'bookingButton', ru: 'bookingButton'},
    },

    filters: {
      moreFilters: {ge: 'moreFilters', en: 'moreFilters', ru: 'moreFilters'},
      priceRange: {ge: 'priceRange', en: 'priceRange', ru: 'priceRange'},
      stars: {ge: 'stars', en: 'stars', ru: 'stars'},
      roomTypes: {ge: 'roomTypes', en: 'roomTypes', ru: 'roomTypes'},
      search: {ge: 'search', en: 'search', ru: 'search'},
      clear: {ge: 'clear', en: 'clear', ru: 'clear'},
      resultsNotFound: {ge: 'resultsNotFound', en: 'resultsNotFound', ru: 'resultsNotFound'},
    },

    global: {
      startDate: {ge: 'startDate', en: 'startDate', ru: 'startDate'},
      endDate: {ge: 'endDate', en: 'endDate', ru: 'endDate'},
      adults: {ge: 'adults', en: 'adults', ru: 'adults'},
      children: {ge: 'children', en: 'children', ru: 'children'},
      infants: {ge: 'infants', en: 'infants', ru: 'infants'},
      search: {ge: 'search', en: 'search', ru: 'search'},
      photos: {ge: 'photos', en: 'photos', ru: 'photos'},
      mail: {ge: 'mail', en: 'mail', ru: 'mail'},
      phone: {ge: 'phone', en: 'phone', ru: 'phone'},
      address: {ge: 'address', en: 'address', ru: 'address'},
      social: {ge: 'social', en: 'social', ru: 'social'},
      name: {ge: 'name', en: 'name', ru: 'name'},
      message: {ge: 'message', en: 'message', ru: 'message'},
      send: {ge: 'send', en: 'send', ru: 'send'},
      category: {ge: 'category', en: 'category', ru: 'category'},
      startBooking: {ge: 'startBooking', en: 'startBooking', ru: 'startBooking'},
      bookVisit: {ge: 'bookVisit', en: 'bookVisit', ru: 'bookVisit'},
      callNow: {ge: 'callNow', en: 'callNow', ru: 'callNow'},
    },

    successMessages: {
      contactMessage: {ge: 'contactMessage', en: 'contactMessage', ru: 'contactMessage'},
      bookingMessage: {ge: 'bookingMessage', en: 'bookingMessage', ru: 'bookingMessage'},
      button: {ge: 'button', en: 'button', ru: 'button'},
    },
  };
}

function getWellnessObject(): any {
  return {
    contactText: {ge: 'contactText', en: 'contactText', ru: 'contactText'},
    contactPhone: '+45435345435345',
    image1: { url: 'https://tinyurl.com/y4v2uzaq'},
    image2: { url: 'https://tinyurl.com/y4v2uzaq'},
    image3: { url: 'https://tinyurl.com/y4v2uzaq'},
    image4: { url: 'https://tinyurl.com/y4v2uzaq'},
    image5: { url: 'https://tinyurl.com/y4v2uzaq'},
  };
}

function getDefaultMealPriceRangesObject(): any {
  return {
    singleMealPrice: 10,
    doubleMealPrice: 15,
    tripleMealPrice: 20,
  };
}

function getHomeSectionsObject(): any {
  return {
    rooms: {
      title: {ge: 'Our Rooms', en: 'Our Rooms', ru: 'Our Rooms'},
      description: {
        ge: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, cumque facilis distinctio quam tempora magni eveniet ',
        en: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, cumque facilis distinctio quam tempora magni eveniet ',
        ru: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, cumque facilis distinctio quam tempora magni eveniet '
      },
      button: {ge: 'Browse Rooms', en: 'Browse Rooms', ru: 'Browse Rooms'},
    },
    gallery: {
      title: {ge: 'Gallery', en: 'Gallery', ru: 'Gallery'},
      description: {
        ge: 'Collections From Our Hotel, Rooms and e.t.c',
        en: 'Collections From Our Hotel, Rooms and e.t.c',
        ru: 'Collections From Our Hotel, Rooms and e.t.c',
      },
    },
    contact: {
      title: {ge: 'Contact Us', en: 'Contact Us', ru: 'Contact Us'},
      description: {
        ge: 'All info about our hotel locations / electronic mails / phone number',
        en: 'All info about our hotel locations / electronic mails / phone number',
        ru: 'All info about our hotel locations / electronic mails / phone number'
      },
    },
  };
}

function getContactObject(): any {
  return {
    phone: '995 555 123456',
    email: 'hello@gmail.com',
    address: { en: 'address en', ge: 'address ge', ru: 'address ru' },
    facebookUrl: 'facebookUrl',
    twitterUrl: 'twitterUrl',
    linkedinUrl: 'linkedinUrl',
    instagramUrl: 'instagramUrl',
    location: {
      lat: 41.7042747,
      lng: 44.7848676
    }
  };
}

function getPromoObject(): any {
  return {
    promoImage: {
      ge: { url: 'http://185.229.111.122/assets/images/gallery_top_banner.png'},
      en: { url: 'http://185.229.111.122/assets/images/gallery_top_banner.png'},
      ru: { url: 'http://185.229.111.122/assets/images/gallery_top_banner.png'},
    },

    headerLogo: {
      ge: { url: 'http://185.229.111.122/0d55185c5938.png'},
      en: { url: 'http://185.229.111.122/0d55185c5938.png'},
      ru: { url: 'http://185.229.111.122/0d55185c5938.png'},
    },
    footerLogo: {
      ge: { url: 'http://185.229.111.122/0d55185c5938.png'},
      en: { url: 'http://185.229.111.122/0d55185c5938.png'},
      ru: { url: 'http://185.229.111.122/0d55185c5938.png'},
    },

  };
}

function getBannerItemObject(title: String): any {
  return {
    title: { en: `${title} en`, ge: `${title} ge`, ru: `${title} ru` },
    image: { url: 'http://157.230.96.128/assets/images/gallery_top_banner.png'},
  };
}

function getBannersObject(): any {
  return {
    about: getBannerItemObject('about'),
    wellness: getBannerItemObject('wellness'),
    rooms: getBannerItemObject('rooms'),
    room: getBannerItemObject('room'),
    news: getBannerItemObject('news'),
    article: getBannerItemObject('article'),
    gallery: getBannerItemObject('gallery'),
    contact: getBannerItemObject('contact'),
  };
}

function getAboutObject(): any {
  return {
    image: { url: 'http://157.230.96.128/assets/images/gallery_top_banner.png'},
    title: { en: 'title en', ge: 'title ge', ru: 'title ru' },
    subtitle: { en: 'subtitle en', ge: 'subtitle ge', ru: 'subtitle ru' },
    content: { en: 'content en', ge: 'content ge', ru: 'content ru' },
    contactPhone: '+995555259325',
    contactText: { en: 'contactText en', ge: 'contactText ge', ru: 'contactText ru' },
  };
}

function getPriceRange(): any {
  return {
    startDate: new Date(),
    endDate: new Date(),
    singleMealPrice: 20,
    doubleMealPrice: 30,
    tripleMealPrice: 40,
  };
}

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(CommonStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
  }));
}

export function getCommon() {
  return {
    contact: getContactObject(),
    promo: getPromoObject(),
    aboutUs: getAboutObject(),
    banners: getBannersObject(),
    priceRangeDates: [getPriceRange(), getPriceRange()],
    homeSections: getHomeSectionsObject(),
    defaultMealPriceRanges: getDefaultMealPriceRangesObject(),
    wellness: getWellnessObject(),

    filterLimits: getFilterLimitsObject(),
    staticTexts: getStaticTextsObject(),
  };
}