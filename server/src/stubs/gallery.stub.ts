import * as _ from 'lodash';
import { cloneStub }  from '../helpers/stub-helpers';

const GalleryStub = {
  title: { en: 'title en', ge: 'title ge', ru: 'title ru' },
  thumbnail: getImage(),
  description: { en: 'description en', ge: 'description ge', ru: 'description ru' },
  date: new Date(),
  images: [getImage(), getImage(), getImage(), getImage(), getImage()],
  meta: getMeta(),
};

function getMeta(): any {
  return {
    title: { en: 'meta title en', ge: 'meta title ge', ru: 'meta title ru' },
    description: { en: 'meta description en', ge: 'meta description ge', ru: 'meta description ru' },
    keywords: [ 'keyword1', 'keyword2', 'keyword3' ],
    image: { url: 'https://www.residencestyle.com/wp-content/uploads/2019/03/Living-Room.jpg' },
  };
}

function getImage(): any {
  return { url: 'https://www.residencestyle.com/wp-content/uploads/2019/03/Living-Room.jpg' };
}

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(GalleryStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
    title: { en: `title en ${i}`, ge: `title ge ${i}`, ru: `title ru ${i}` },
  }));
}