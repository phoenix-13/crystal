import * as _ from 'lodash';
import { cloneStub }  from '../helpers/stub-helpers';

const MealPriceStub = {
  priceRangeDates: [getPriceRange(), getPriceRange()],
};

function getPriceRange(): any {
  return {
    startDate: new Date(),
    endDate: new Date(),
    singleMealPrice: 20,
    doubleMealPrice: 30,
    tripleMealPrice: 40,
  };
}

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(MealPriceStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
  }));
}