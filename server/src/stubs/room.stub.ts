import * as _ from 'lodash';
import { cloneStub } from '../helpers/stub-helpers';
const moment = require('moment');

const RoomStub = {
  name: { en: 'roomType en', ge: 'roomType ge', ru: 'roomType ru' },
  stars: 4,
  defaultPrice: 100,
  thumbnail: { url: 'https://2486634c787a971a3554-d983ce57e4c84901daded0f67d5a004f.ssl.cf1.rackcdn.com/mylohotel/media/doubledoublenew-571fb97b036cc.jpg' },
  priceForAdditionalPerson: 40,
  images: [ {url: 'https://t-ec.bstatic.com/images/hotel/max1024x768/178/178850685.jpg'},
            {url: 'https://www.hotelnikkosf.com/resourcefiles/roomssmallimages/hotel-nikko-san-francisco-deluxe-room-th.jpg'},
            {url: 'https://bentleyhotelnyc.com/wp-content/uploads/sites/3/2016/10/bentley-hotel-nyc-superior-king-room.jpg'},
          ],
  description: { en: 'description en', ge: 'description ge', ru: 'description ru' },
  priceRangeDates: [getPriceRange(1), getPriceRange(21), getPriceRange(41)],
  meta: getMeta(),

  numberOfGuests: 4,
  numberOfAdditionalGuests: 1,
  infantFriendly: true,
};

function getMeta(): any {
  return {
    title: { en: 'meta title en', ge: 'meta title ge', ru: 'meta title ru' },
    description: { en: 'meta description en', ge: 'meta description ge', ru: 'meta description ru' },
    keywords: ['keyword1', 'keyword2', 'keyword3'],
    image: { url: 'https://t-ec.bstatic.com/images/hotel/max1024x768/178/178850685.jpg' },
  };
}

function getPriceRange(day: number): any {
  return {
    startDate: moment().add(day, 'days'),
    endDate: moment().add(day + 20, 'days'),
    defaultPrice: 210,
    priceForAdditionalPerson: 1,
  };
}

function getImage(): any {
  return { url: 'https://d3bv2hg4q0qyg2.cloudfront.net/2016/04/18/write.jpg' };
}

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(RoomStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
    roomType: { en: `Room Type en ${i}`, ge: `Room Type ge ${i}`, ru: `Room Type ru ${i}` },
  }));
}