import * as _ from 'lodash';
import { cloneStub } from '../helpers/stub-helpers';

const RoomFeatureStub = {
  name: { en: 'name en', ge: 'name ge', ru: 'name ru' },
  icon: { url: 'http://www.clker.com/cliparts/T/v/4/h/T/v/feature-md.png' },
};

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(RoomFeatureStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
    name: { en: `name en ${i}`, ge: `name ge ${i}`, ru: `name ru ${i}` },
  }));
}