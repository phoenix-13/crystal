import * as _ from 'lodash';
import { cloneStub }  from '../helpers/stub-helpers';

const RoomTypeStub = {
  name: { en: 'name en', ge: 'name ge', ru: 'name ru' },
};

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(RoomTypeStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
    name: { en: `name en ${i}`, ge: `name ge ${i}`, ru: `name ru ${i}` },
  }));
}