import * as _ from 'lodash';
import { cloneStub }  from '../helpers/stub-helpers';

const WellnessStub = {
  name: { en: 'name en', ge: 'name ge', ru: 'name ru' },
  procedures: [getProcedure(), getProcedure(), getProcedure(), getProcedure()],
};


function getProcedure(): any {
  return {
    name: { en: 'procedure name en', ge: 'procedure name ge', ru: 'procedure name ru' },
    price: { en: '10 gel en', ge: '10 gel ge', ru: '10 gel ru' },
    duration: { en: '10 min en', ge: '10 min ge', ru: '10 min ru' },
  };
}

export function getSingle(fields?: any): any {
  return {
    ...cloneStub(WellnessStub),
    ...fields
  };
}

export function getMany(count: number, fields?: any) {
  return _.range(count).map((i: number) => ({
    ...getSingle(),
    ...fields,
    name: { en: `name en ${i}`, ge: `name ge ${i}`, ru: `name ru ${i}` },
  }));
}

